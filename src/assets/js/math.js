

// 计算精度
/*
 * 判断obj是否为一个整数
 */
function isInteger (obj) {
    return Math.floor(obj) === obj
}

/*
 * 将一个浮点数转成整数，返回整数和倍数。如 3.14 >> 314，倍数是 100
 * @param floatNum {number} 小数
 * @return {object}
 *   {times:100, num: 314}
 */
function toInteger(floatNum) {
    var ret = { times: 1, num: 0 }
    if (isInteger(floatNum)) {
        ret.num = floatNum
        return ret
    }
    var strfi = floatNum + ''
    var dotPos = strfi.indexOf('.')
    var len = strfi.substr(dotPos + 1).length
    var times = Math.pow(10, len)
    var intNum = parseInt(floatNum * times + 0.5, 10)
    ret.times = times
    ret.num = intNum
    return ret
}

/*
 * 核心方法，实现加减乘除运算，确保不丢失精度
 * 思路：把小数放大为整数（乘），进行算术运算，再缩小为小数（除）
 *
 * @param a {number} 运算数1
 * @param b {number} 运算数2
 * @param op {string} 运算类型，有加减乘除（add/subtract/multiply/divide）
 *
 */
function operation(a, b, op) {
    var o1 = toInteger(a)
    var o2 = toInteger(b)
    var n1 = o1.num
    var n2 = o2.num
    var t1 = o1.times
    var t2 = o2.times
    var max = t1 > t2 ? t1 : t2
    var result = null
    switch (op) {
        case 'add':
            if (t1 === t2) { // 两个小数位数相同
                result = n1 + n2
            } else if (t1 > t2) { // o1 小数位 大于 o2
                result = n1 + n2 * (t1 / t2)
            } else { // o1 小数位 小于 o2
                result = n1 * (t2 / t1) + n2
            }
            return result / max
        case 'subtract':
            if (t1 === t2) {
                result = n1 - n2
            } else if (t1 > t2) {
                result = n1 - n2 * (t1 / t2)
            } else {
                result = n1 * (t2 / t1) - n2
            }
            return result / max
        case 'multiply':
            result = (n1 * n2) / (t1 * t2)
            return result
        case 'divide':
            result = (n1 / n2) * (t2 / t1)
            return result
    }
}

// 加减乘除
const add = (a, b) => {
    return operation(a, b, 'add')
}
const subtract = (a, b) => {
    return operation(a, b, 'subtract')
}
const multiply = (a, b) => {
    return operation(a, b, 'multiply')
}
const divide = (a, b) => {
    return operation(a, b, 'divide')
}
const formatPrice = (a, b) => {
    return Math.round(a * 100) / 100
}

// console.log(add(0.1, 0.2)) // 0.3
// console.log(add(0.155, 0.254)) // 0.3
// console.log(0.155 + 0.254)

// console.log(subtract(1.0, 0.9)) // 0.1
// console.log(multiply(19.9, 100)) // 1990
// console.log(divide(6.6, 0.2)) // 33

// console.log(1.535.toFixed(2))
// console.log(1.635.toFixed(2))

// console.log(add(1.5, 0.035).toFixed(2)) // 0.3
// console.log('formatPrice === ', formatPrice(1.5377777777777777005))

const math = {
    add,
    subtract,
    multiply,
    divide,

}

export default math