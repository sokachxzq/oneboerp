
const phoneExp = /^1(3|4|5|6|7|8|9)\d{9}$/; // 手机
const passwordExp = /^[a-zA-Z0-9_]{6,16}$/; // 密码
const weChatExp = /^[a-zA-Z]([-_a-zA-Z0-9]{5,19})+$/; // 微信
const qqExp = /^[1-9][0-9]{4,9}$/gim; // qq
const faxExp = /^(\d{3,4}-)?\d{7,8}$/; // 传真
const emailExp = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/; // 邮箱
const idCardExp = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/; // 身份证
const invoiceExp = /^[0-9a-zA-Z\(\)\（\）\u4e00-\u9fa5]{0,50}$/; // 发票抬头
const TaxpayerExp = /^[A-Z0-9]{15}$|^[A-Z0-9]{17}$|^[A-Z0-9]{18}$|^[A-Z0-9]{20}$/; // 纳税人识别号
const bankCradExp = /^([1-9]{1})(\d{15}|\d{18})$/; // 银行卡
const webUrlExp = /^([hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/; // 网址
const moneyExp = /((^[1-9]\d*)|^0)(\.\d{0,2}){0,1}$/; // 金额
const numberExp = /^\+?[0-9][0-9]*$/; // 整数0~9
const numberOneExp = /^\+?[1-9][0-9]*$/; // 整数1~9
const expressionExp = /(\ud83c[\udf00-\udfff])|(\ud83d[\udc00-\ude4f])|(\ud83d[\ude80-\udeff])/g; // 表情符


const exp = {
    phoneExp,
    passwordExp,
    weChatExp,
    qqExp,
    faxExp,
    emailExp,
    idCardExp,
    invoiceExp,
    TaxpayerExp,
    bankCradExp,
    webUrlExp,
    moneyExp,
    numberExp,
    numberOneExp,
    expressionExp,



}

export default exp