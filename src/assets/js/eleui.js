import { Message, MessageBox } from 'element-ui';

const eleui = {
    message(message, type, obj) {
        var obj = obj || {}
        var message = message || obj.message || '请求失败'
        var type = type || obj.type || 'error'
        return Message({
            message: obj.message || message,
            type: obj.type || type,
            iconClass: obj.iconClass || '',
            duration: obj.duration || 2000,
            showClose: obj.showClose || true,
            onClose: obj.fail || function () {
                
            }
        })
    },
    confirm(obj) {
        var message = obj.message || '确定删除吗？'
        var title = obj.title || '温馨提示'
        return MessageBox.confirm(message, title, {
            type: obj.type || 'info',
            iconClass: obj.iconClass || '',
            showCancelButton: obj.showCancelButton || true,
            showConfirmButton: obj.showConfirmButton || true,
            cancelButtonText: obj.cancelButtonText || '取消',
            confirmButtonText: obj.confirmButtonText || '确定',
        }).then(obj.success || function() {
            
        }).catch(obj.fail || function() {
            
        })
    },

    // 规格表格表单数据 ========================
    validateInput(rule, value, callback) {
        if (value === '' || value === null || value === undefined) {
            return callback(new Error('此项必填'))
        } else {
            callback()
        }
    },
    // 验证价格
    validatePrices(rule, value, callback) {
        if (rule.required === false && (value === '' || value === null || value === undefined)) {
            callback()
        }
        const isPrice = /(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/;
        if (isPrice.test(value)) {
            callback()
        } else {
            callback(new Error(rule.message || '请输入正确的价格，例：10, 10.00'))
        }
    },
    // 验证数字
    validateNumber(rule, value, callback) {
        if (rule.required === false && (value === '' || value === null || value === undefined)) {
            callback()
        }
        const isNumber = /^\+?[0-9][0-9]*$/;
        if (isNumber.test(value)) {
            callback()
        } else {
            callback(new Error(rule.message || '请输入正确的数字，>=0，例：10'))
        }
    },
    validateNumber_(rule, value, callback) {
        if (rule.required === false && (value === '' || value === null || value === undefined)) {
            callback()
        }
        const isNumber = /^\+?[1-9][0-9]*$/;
        if (isNumber.test(value)) {
            callback()
        } else {
            callback(new Error(rule.message || '请输入正确的数字，>=1，例：10'))
        }
    },
    validatePhone(rule, value, callback) {
        if (rule.required === false && (value === '' || value === null || value === undefined)) {
            callback()
        }
        const isPhone = /^1(3|4|5|6|7|8|9)\d{9}$/;
        if (isPhone.test(value)) {
            callback()
        } else {
            callback(new Error(rule.message || '请输入正确的手机号码'))
        }
    },
    validatePassword(rule, value, callback) {
        if (rule.required === false && (value === '' || value === null || value === undefined)) {
            callback()
        }
        var exp = /^[a-zA-Z0-9_]{6,16}$/
        if (exp.test(value)) {
            callback()
        } else {
            callback(new Error(rule.message || '请输入正确的密码'))
        }
    },

}

export default eleui

