var obj = {
	"110000": {
		"areaId": 2,
		"children": {
			"110100": {
				"areaId": 2,
				"children": {
					"110101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110101,
						"regionName": "东城区"
					},
					"110102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110102,
						"regionName": "西城区"
					},
					"110105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110105,
						"regionName": "朝阳区"
					},
					"110106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110106,
						"regionName": "丰台区"
					},
					"110107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110107,
						"regionName": "石景山区"
					},
					"110108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110108,
						"regionName": "海淀区"
					},
					"110109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110109,
						"regionName": "门头沟区"
					},
					"110111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110111,
						"regionName": "房山区"
					},
					"110112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110112,
						"regionName": "通州区"
					},
					"110113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110113,
						"regionName": "顺义区"
					},
					"110114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110114,
						"regionName": "昌平区"
					},
					"110115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110115,
						"regionName": "大兴区"
					},
					"110116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110116,
						"regionName": "怀柔区"
					},
					"110117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110117,
						"regionName": "平谷区"
					},
					"110118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110118,
						"regionName": "密云区"
					},
					"110119": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 110119,
						"regionName": "延庆区"
					}
				},
				"regionGrade": 2,
				"regionId": 110100,
				"regionName": "北京市"
			}
		},
		"regionGrade": 1,
		"regionId": 110000,
		"regionName": "北京市"
	},
	"120000": {
		"areaId": 2,
		"children": {
			"120100": {
				"areaId": 2,
				"children": {
					"120101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120101,
						"regionName": "和平区"
					},
					"120102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120102,
						"regionName": "河东区"
					},
					"120103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120103,
						"regionName": "河西区"
					},
					"120104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120104,
						"regionName": "南开区"
					},
					"120105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120105,
						"regionName": "河北区"
					},
					"120106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120106,
						"regionName": "红桥区"
					},
					"120110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120110,
						"regionName": "东丽区"
					},
					"120111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120111,
						"regionName": "西青区"
					},
					"120112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120112,
						"regionName": "津南区"
					},
					"120113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120113,
						"regionName": "北辰区"
					},
					"120114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120114,
						"regionName": "武清区"
					},
					"120115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120115,
						"regionName": "宝坻区"
					},
					"120116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120116,
						"regionName": "滨海新区"
					},
					"120117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120117,
						"regionName": "宁河区"
					},
					"120118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120118,
						"regionName": "静海区"
					},
					"120119": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 120119,
						"regionName": "蓟州区"
					}
				},
				"regionGrade": 2,
				"regionId": 120100,
				"regionName": "天津市"
			}
		},
		"regionGrade": 1,
		"regionId": 120000,
		"regionName": "天津市"
	},
	"130000": {
		"areaId": 2,
		"children": {
			"130100": {
				"areaId": 0,
				"children": {
					"130102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130102,
						"regionName": "长安区"
					},
					"130104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130104,
						"regionName": "桥西区"
					},
					"130105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130105,
						"regionName": "新华区"
					},
					"130107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130107,
						"regionName": "井陉矿区"
					},
					"130108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130108,
						"regionName": "裕华区"
					},
					"130109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130109,
						"regionName": "藁城区"
					},
					"130110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130110,
						"regionName": "鹿泉区"
					},
					"130111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130111,
						"regionName": "栾城区"
					},
					"130121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130121,
						"regionName": "井陉县"
					},
					"130123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130123,
						"regionName": "正定县"
					},
					"130125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130125,
						"regionName": "行唐县"
					},
					"130126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130126,
						"regionName": "灵寿县"
					},
					"130127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130127,
						"regionName": "高邑县"
					},
					"130128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130128,
						"regionName": "深泽县"
					},
					"130129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130129,
						"regionName": "赞皇县"
					},
					"130130": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130130,
						"regionName": "无极县"
					},
					"130131": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130131,
						"regionName": "平山县"
					},
					"130132": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130132,
						"regionName": "元氏县"
					},
					"130133": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130133,
						"regionName": "赵县"
					},
					"130181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130181,
						"regionName": "辛集市"
					},
					"130183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130183,
						"regionName": "晋州市"
					},
					"130184": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130184,
						"regionName": "新乐市"
					}
				},
				"regionGrade": 2,
				"regionId": 130100,
				"regionName": "石家庄市"
			},
			"130200": {
				"areaId": 0,
				"children": {
					"130202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130202,
						"regionName": "路南区"
					},
					"130203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130203,
						"regionName": "路北区"
					},
					"130204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130204,
						"regionName": "古冶区"
					},
					"130205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130205,
						"regionName": "开平区"
					},
					"130207": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130207,
						"regionName": "丰南区"
					},
					"130208": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130208,
						"regionName": "丰润区"
					},
					"130209": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130209,
						"regionName": "曹妃甸区"
					},
					"130223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130223,
						"regionName": "滦县"
					},
					"130224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130224,
						"regionName": "滦南县"
					},
					"130225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130225,
						"regionName": "乐亭县"
					},
					"130227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130227,
						"regionName": "迁西县"
					},
					"130229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130229,
						"regionName": "玉田县"
					},
					"130281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130281,
						"regionName": "遵化市"
					},
					"130283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130283,
						"regionName": "迁安市"
					}
				},
				"regionGrade": 2,
				"regionId": 130200,
				"regionName": "唐山市"
			},
			"130300": {
				"areaId": 0,
				"children": {
					"130302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130302,
						"regionName": "海港区"
					},
					"130303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130303,
						"regionName": "山海关区"
					},
					"130304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130304,
						"regionName": "北戴河区"
					},
					"130306": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130306,
						"regionName": "抚宁区"
					},
					"130321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130321,
						"regionName": "青龙满族自治县"
					},
					"130322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130322,
						"regionName": "昌黎县"
					},
					"130324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130324,
						"regionName": "卢龙县"
					}
				},
				"regionGrade": 2,
				"regionId": 130300,
				"regionName": "秦皇岛市"
			},
			"130400": {
				"areaId": 0,
				"children": {
					"130402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130402,
						"regionName": "邯山区"
					},
					"130403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130403,
						"regionName": "丛台区"
					},
					"130404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130404,
						"regionName": "复兴区"
					},
					"130406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130406,
						"regionName": "峰峰矿区"
					},
					"130407": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130407,
						"regionName": "肥乡区"
					},
					"130408": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130408,
						"regionName": "永年区"
					},
					"130423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130423,
						"regionName": "临漳县"
					},
					"130424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130424,
						"regionName": "成安县"
					},
					"130425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130425,
						"regionName": "大名县"
					},
					"130426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130426,
						"regionName": "涉县"
					},
					"130427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130427,
						"regionName": "磁县"
					},
					"130430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130430,
						"regionName": "邱县"
					},
					"130431": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130431,
						"regionName": "鸡泽县"
					},
					"130432": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130432,
						"regionName": "广平县"
					},
					"130433": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130433,
						"regionName": "馆陶县"
					},
					"130434": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130434,
						"regionName": "魏县"
					},
					"130435": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130435,
						"regionName": "曲周县"
					},
					"130481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130481,
						"regionName": "武安市"
					}
				},
				"regionGrade": 2,
				"regionId": 130400,
				"regionName": "邯郸市"
			},
			"130500": {
				"areaId": 0,
				"children": {
					"130502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130502,
						"regionName": "桥东区"
					},
					"130503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130503,
						"regionName": "桥西区"
					},
					"130521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130521,
						"regionName": "邢台县"
					},
					"130522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130522,
						"regionName": "临城县"
					},
					"130523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130523,
						"regionName": "内丘县"
					},
					"130524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130524,
						"regionName": "柏乡县"
					},
					"130525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130525,
						"regionName": "隆尧县"
					},
					"130526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130526,
						"regionName": "任县"
					},
					"130527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130527,
						"regionName": "南和县"
					},
					"130528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130528,
						"regionName": "宁晋县"
					},
					"130529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130529,
						"regionName": "巨鹿县"
					},
					"130530": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130530,
						"regionName": "新河县"
					},
					"130531": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130531,
						"regionName": "广宗县"
					},
					"130532": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130532,
						"regionName": "平乡县"
					},
					"130533": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130533,
						"regionName": "威县"
					},
					"130534": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130534,
						"regionName": "清河县"
					},
					"130535": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130535,
						"regionName": "临西县"
					},
					"130581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130581,
						"regionName": "南宫市"
					},
					"130582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130582,
						"regionName": "沙河市"
					}
				},
				"regionGrade": 2,
				"regionId": 130500,
				"regionName": "邢台市"
			},
			"130600": {
				"areaId": 0,
				"children": {
					"130602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130602,
						"regionName": "竞秀区"
					},
					"130606": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130606,
						"regionName": "莲池区"
					},
					"130607": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130607,
						"regionName": "满城区"
					},
					"130608": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130608,
						"regionName": "清苑区"
					},
					"130609": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130609,
						"regionName": "徐水区"
					},
					"130623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130623,
						"regionName": "涞水县"
					},
					"130624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130624,
						"regionName": "阜平县"
					},
					"130626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130626,
						"regionName": "定兴县"
					},
					"130627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130627,
						"regionName": "唐县"
					},
					"130628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130628,
						"regionName": "高阳县"
					},
					"130629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130629,
						"regionName": "容城县"
					},
					"130630": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130630,
						"regionName": "涞源县"
					},
					"130631": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130631,
						"regionName": "望都县"
					},
					"130632": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130632,
						"regionName": "安新县"
					},
					"130633": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130633,
						"regionName": "易县"
					},
					"130634": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130634,
						"regionName": "曲阳县"
					},
					"130635": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130635,
						"regionName": "蠡县"
					},
					"130636": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130636,
						"regionName": "顺平县"
					},
					"130637": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130637,
						"regionName": "博野县"
					},
					"130638": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130638,
						"regionName": "雄县"
					},
					"130681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130681,
						"regionName": "涿州市"
					},
					"130682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130682,
						"regionName": "定州市"
					},
					"130683": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130683,
						"regionName": "安国市"
					},
					"130684": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130684,
						"regionName": "高碑店市"
					}
				},
				"regionGrade": 2,
				"regionId": 130600,
				"regionName": "保定市"
			},
			"130700": {
				"areaId": 0,
				"children": {
					"130702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130702,
						"regionName": "桥东区"
					},
					"130703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130703,
						"regionName": "桥西区"
					},
					"130705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130705,
						"regionName": "宣化区"
					},
					"130706": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130706,
						"regionName": "下花园区"
					},
					"130708": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130708,
						"regionName": "万全区"
					},
					"130709": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130709,
						"regionName": "崇礼区"
					},
					"130722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130722,
						"regionName": "张北县"
					},
					"130723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130723,
						"regionName": "康保县"
					},
					"130724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130724,
						"regionName": "沽源县"
					},
					"130725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130725,
						"regionName": "尚义县"
					},
					"130726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130726,
						"regionName": "蔚县"
					},
					"130727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130727,
						"regionName": "阳原县"
					},
					"130728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130728,
						"regionName": "怀安县"
					},
					"130730": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130730,
						"regionName": "怀来县"
					},
					"130731": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130731,
						"regionName": "涿鹿县"
					},
					"130732": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130732,
						"regionName": "赤城县"
					}
				},
				"regionGrade": 2,
				"regionId": 130700,
				"regionName": "张家口市"
			},
			"130800": {
				"areaId": 0,
				"children": {
					"130802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130802,
						"regionName": "双桥区"
					},
					"130803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130803,
						"regionName": "双滦区"
					},
					"130804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130804,
						"regionName": "鹰手营子矿区"
					},
					"130821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130821,
						"regionName": "承德县"
					},
					"130822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130822,
						"regionName": "兴隆县"
					},
					"130824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130824,
						"regionName": "滦平县"
					},
					"130825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130825,
						"regionName": "隆化县"
					},
					"130826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130826,
						"regionName": "丰宁满族自治县"
					},
					"130827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130827,
						"regionName": "宽城满族自治县"
					},
					"130828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130828,
						"regionName": "围场满族蒙古族自治县"
					},
					"130881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130881,
						"regionName": "平泉市"
					}
				},
				"regionGrade": 2,
				"regionId": 130800,
				"regionName": "承德市"
			},
			"130900": {
				"areaId": 0,
				"children": {
					"130902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130902,
						"regionName": "新华区"
					},
					"130903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130903,
						"regionName": "运河区"
					},
					"130921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130921,
						"regionName": "沧县"
					},
					"130922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130922,
						"regionName": "青县"
					},
					"130923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130923,
						"regionName": "东光县"
					},
					"130924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130924,
						"regionName": "海兴县"
					},
					"130925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130925,
						"regionName": "盐山县"
					},
					"130926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130926,
						"regionName": "肃宁县"
					},
					"130927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130927,
						"regionName": "南皮县"
					},
					"130928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130928,
						"regionName": "吴桥县"
					},
					"130929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130929,
						"regionName": "献县"
					},
					"130930": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130930,
						"regionName": "孟村回族自治县"
					},
					"130981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130981,
						"regionName": "泊头市"
					},
					"130982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130982,
						"regionName": "任丘市"
					},
					"130983": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130983,
						"regionName": "黄骅市"
					},
					"130984": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 130984,
						"regionName": "河间市"
					}
				},
				"regionGrade": 2,
				"regionId": 130900,
				"regionName": "沧州市"
			},
			"131000": {
				"areaId": 0,
				"children": {
					"131002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131002,
						"regionName": "安次区"
					},
					"131003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131003,
						"regionName": "广阳区"
					},
					"131022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131022,
						"regionName": "固安县"
					},
					"131023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131023,
						"regionName": "永清县"
					},
					"131024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131024,
						"regionName": "香河县"
					},
					"131025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131025,
						"regionName": "大城县"
					},
					"131026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131026,
						"regionName": "文安县"
					},
					"131028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131028,
						"regionName": "大厂回族自治县"
					},
					"131081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131081,
						"regionName": "霸州市"
					},
					"131082": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131082,
						"regionName": "三河市"
					}
				},
				"regionGrade": 2,
				"regionId": 131000,
				"regionName": "廊坊市"
			},
			"131100": {
				"areaId": 0,
				"children": {
					"131102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131102,
						"regionName": "桃城区"
					},
					"131103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131103,
						"regionName": "冀州区"
					},
					"131121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131121,
						"regionName": "枣强县"
					},
					"131122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131122,
						"regionName": "武邑县"
					},
					"131123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131123,
						"regionName": "武强县"
					},
					"131124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131124,
						"regionName": "饶阳县"
					},
					"131125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131125,
						"regionName": "安平县"
					},
					"131126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131126,
						"regionName": "故城县"
					},
					"131127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131127,
						"regionName": "景县"
					},
					"131128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131128,
						"regionName": "阜城县"
					},
					"131182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 131182,
						"regionName": "深州市"
					}
				},
				"regionGrade": 2,
				"regionId": 131100,
				"regionName": "衡水市"
			}
		},
		"regionGrade": 1,
		"regionId": 130000,
		"regionName": "河北省"
	},
	"140000": {
		"areaId": 2,
		"children": {
			"140100": {
				"areaId": 0,
				"children": {
					"140105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140105,
						"regionName": "小店区"
					},
					"140106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140106,
						"regionName": "迎泽区"
					},
					"140107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140107,
						"regionName": "杏花岭区"
					},
					"140108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140108,
						"regionName": "尖草坪区"
					},
					"140109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140109,
						"regionName": "万柏林区"
					},
					"140110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140110,
						"regionName": "晋源区"
					},
					"140121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140121,
						"regionName": "清徐县"
					},
					"140122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140122,
						"regionName": "阳曲县"
					},
					"140123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140123,
						"regionName": "娄烦县"
					},
					"140181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140181,
						"regionName": "古交市"
					}
				},
				"regionGrade": 2,
				"regionId": 140100,
				"regionName": "太原市"
			},
			"140200": {
				"areaId": 0,
				"children": {
					"140202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140202,
						"regionName": "城区"
					},
					"140203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140203,
						"regionName": "矿区"
					},
					"140211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140211,
						"regionName": "南郊区"
					},
					"140212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140212,
						"regionName": "新荣区"
					},
					"140221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140221,
						"regionName": "阳高县"
					},
					"140222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140222,
						"regionName": "天镇县"
					},
					"140223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140223,
						"regionName": "广灵县"
					},
					"140224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140224,
						"regionName": "灵丘县"
					},
					"140225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140225,
						"regionName": "浑源县"
					},
					"140226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140226,
						"regionName": "左云县"
					},
					"140227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140227,
						"regionName": "大同县"
					}
				},
				"regionGrade": 2,
				"regionId": 140200,
				"regionName": "大同市"
			},
			"140300": {
				"areaId": 0,
				"children": {
					"140302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140302,
						"regionName": "城区"
					},
					"140303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140303,
						"regionName": "矿区"
					},
					"140311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140311,
						"regionName": "郊区"
					},
					"140321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140321,
						"regionName": "平定县"
					},
					"140322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140322,
						"regionName": "盂县"
					}
				},
				"regionGrade": 2,
				"regionId": 140300,
				"regionName": "阳泉市"
			},
			"140400": {
				"areaId": 0,
				"children": {
					"140402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140402,
						"regionName": "城区"
					},
					"140411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140411,
						"regionName": "郊区"
					},
					"140421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140421,
						"regionName": "长治县"
					},
					"140423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140423,
						"regionName": "襄垣县"
					},
					"140424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140424,
						"regionName": "屯留县"
					},
					"140425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140425,
						"regionName": "平顺县"
					},
					"140426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140426,
						"regionName": "黎城县"
					},
					"140427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140427,
						"regionName": "壶关县"
					},
					"140428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140428,
						"regionName": "长子县"
					},
					"140429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140429,
						"regionName": "武乡县"
					},
					"140430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140430,
						"regionName": "沁县"
					},
					"140431": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140431,
						"regionName": "沁源县"
					},
					"140481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140481,
						"regionName": "潞城市"
					}
				},
				"regionGrade": 2,
				"regionId": 140400,
				"regionName": "长治市"
			},
			"140500": {
				"areaId": 0,
				"children": {
					"140502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140502,
						"regionName": "城区"
					},
					"140521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140521,
						"regionName": "沁水县"
					},
					"140522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140522,
						"regionName": "阳城县"
					},
					"140524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140524,
						"regionName": "陵川县"
					},
					"140525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140525,
						"regionName": "泽州县"
					},
					"140581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140581,
						"regionName": "高平市"
					}
				},
				"regionGrade": 2,
				"regionId": 140500,
				"regionName": "晋城市"
			},
			"140600": {
				"areaId": 0,
				"children": {
					"140602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140602,
						"regionName": "朔城区"
					},
					"140603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140603,
						"regionName": "平鲁区"
					},
					"140621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140621,
						"regionName": "山阴县"
					},
					"140622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140622,
						"regionName": "应县"
					},
					"140623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140623,
						"regionName": "右玉县"
					},
					"140624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140624,
						"regionName": "怀仁县"
					}
				},
				"regionGrade": 2,
				"regionId": 140600,
				"regionName": "朔州市"
			},
			"140700": {
				"areaId": 0,
				"children": {
					"140702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140702,
						"regionName": "榆次区"
					},
					"140721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140721,
						"regionName": "榆社县"
					},
					"140722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140722,
						"regionName": "左权县"
					},
					"140723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140723,
						"regionName": "和顺县"
					},
					"140724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140724,
						"regionName": "昔阳县"
					},
					"140725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140725,
						"regionName": "寿阳县"
					},
					"140726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140726,
						"regionName": "太谷县"
					},
					"140727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140727,
						"regionName": "祁县"
					},
					"140728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140728,
						"regionName": "平遥县"
					},
					"140729": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140729,
						"regionName": "灵石县"
					},
					"140781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140781,
						"regionName": "介休市"
					}
				},
				"regionGrade": 2,
				"regionId": 140700,
				"regionName": "晋中市"
			},
			"140800": {
				"areaId": 0,
				"children": {
					"140802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140802,
						"regionName": "盐湖区"
					},
					"140821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140821,
						"regionName": "临猗县"
					},
					"140822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140822,
						"regionName": "万荣县"
					},
					"140823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140823,
						"regionName": "闻喜县"
					},
					"140824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140824,
						"regionName": "稷山县"
					},
					"140825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140825,
						"regionName": "新绛县"
					},
					"140826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140826,
						"regionName": "绛县"
					},
					"140827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140827,
						"regionName": "垣曲县"
					},
					"140828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140828,
						"regionName": "夏县"
					},
					"140829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140829,
						"regionName": "平陆县"
					},
					"140830": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140830,
						"regionName": "芮城县"
					},
					"140881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140881,
						"regionName": "永济市"
					},
					"140882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140882,
						"regionName": "河津市"
					}
				},
				"regionGrade": 2,
				"regionId": 140800,
				"regionName": "运城市"
			},
			"140900": {
				"areaId": 0,
				"children": {
					"140902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140902,
						"regionName": "忻府区"
					},
					"140921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140921,
						"regionName": "定襄县"
					},
					"140922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140922,
						"regionName": "五台县"
					},
					"140923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140923,
						"regionName": "代县"
					},
					"140924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140924,
						"regionName": "繁峙县"
					},
					"140925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140925,
						"regionName": "宁武县"
					},
					"140926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140926,
						"regionName": "静乐县"
					},
					"140927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140927,
						"regionName": "神池县"
					},
					"140928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140928,
						"regionName": "五寨县"
					},
					"140929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140929,
						"regionName": "岢岚县"
					},
					"140930": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140930,
						"regionName": "河曲县"
					},
					"140931": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140931,
						"regionName": "保德县"
					},
					"140932": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140932,
						"regionName": "偏关县"
					},
					"140981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 140981,
						"regionName": "原平市"
					}
				},
				"regionGrade": 2,
				"regionId": 140900,
				"regionName": "忻州市"
			},
			"141000": {
				"areaId": 0,
				"children": {
					"141002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141002,
						"regionName": "尧都区"
					},
					"141021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141021,
						"regionName": "曲沃县"
					},
					"141022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141022,
						"regionName": "翼城县"
					},
					"141023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141023,
						"regionName": "襄汾县"
					},
					"141024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141024,
						"regionName": "洪洞县"
					},
					"141025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141025,
						"regionName": "古县"
					},
					"141026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141026,
						"regionName": "安泽县"
					},
					"141027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141027,
						"regionName": "浮山县"
					},
					"141028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141028,
						"regionName": "吉县"
					},
					"141029": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141029,
						"regionName": "乡宁县"
					},
					"141030": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141030,
						"regionName": "大宁县"
					},
					"141031": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141031,
						"regionName": "隰县"
					},
					"141032": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141032,
						"regionName": "永和县"
					},
					"141033": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141033,
						"regionName": "蒲县"
					},
					"141034": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141034,
						"regionName": "汾西县"
					},
					"141081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141081,
						"regionName": "侯马市"
					},
					"141082": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141082,
						"regionName": "霍州市"
					}
				},
				"regionGrade": 2,
				"regionId": 141000,
				"regionName": "临汾市"
			},
			"141100": {
				"areaId": 0,
				"children": {
					"141102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141102,
						"regionName": "离石区"
					},
					"141121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141121,
						"regionName": "文水县"
					},
					"141122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141122,
						"regionName": "交城县"
					},
					"141123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141123,
						"regionName": "兴县"
					},
					"141124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141124,
						"regionName": "临县"
					},
					"141125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141125,
						"regionName": "柳林县"
					},
					"141126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141126,
						"regionName": "石楼县"
					},
					"141127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141127,
						"regionName": "岚县"
					},
					"141128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141128,
						"regionName": "方山县"
					},
					"141129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141129,
						"regionName": "中阳县"
					},
					"141130": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141130,
						"regionName": "交口县"
					},
					"141181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141181,
						"regionName": "孝义市"
					},
					"141182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 141182,
						"regionName": "汾阳市"
					}
				},
				"regionGrade": 2,
				"regionId": 141100,
				"regionName": "吕梁市"
			}
		},
		"regionGrade": 1,
		"regionId": 140000,
		"regionName": "山西省"
	},
	"150000": {
		"areaId": 2,
		"children": {
			"150100": {
				"areaId": 0,
				"children": {
					"150102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150102,
						"regionName": "新城区"
					},
					"150103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150103,
						"regionName": "回民区"
					},
					"150104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150104,
						"regionName": "玉泉区"
					},
					"150105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150105,
						"regionName": "赛罕区"
					},
					"150121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150121,
						"regionName": "土默特左旗"
					},
					"150122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150122,
						"regionName": "托克托县"
					},
					"150123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150123,
						"regionName": "和林格尔县"
					},
					"150124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150124,
						"regionName": "清水河县"
					},
					"150125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150125,
						"regionName": "武川县"
					}
				},
				"regionGrade": 2,
				"regionId": 150100,
				"regionName": "呼和浩特市"
			},
			"150200": {
				"areaId": 0,
				"children": {
					"150202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150202,
						"regionName": "东河区"
					},
					"150203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150203,
						"regionName": "昆都仑区"
					},
					"150204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150204,
						"regionName": "青山区"
					},
					"150205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150205,
						"regionName": "石拐区"
					},
					"150206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150206,
						"regionName": "白云鄂博矿区"
					},
					"150207": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150207,
						"regionName": "九原区"
					},
					"150221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150221,
						"regionName": "土默特右旗"
					},
					"150222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150222,
						"regionName": "固阳县"
					},
					"150223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150223,
						"regionName": "达尔罕茂明安联合旗"
					}
				},
				"regionGrade": 2,
				"regionId": 150200,
				"regionName": "包头市"
			},
			"150300": {
				"areaId": 0,
				"children": {
					"150302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150302,
						"regionName": "海勃湾区"
					},
					"150303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150303,
						"regionName": "海南区"
					},
					"150304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150304,
						"regionName": "乌达区"
					}
				},
				"regionGrade": 2,
				"regionId": 150300,
				"regionName": "乌海市"
			},
			"150400": {
				"areaId": 0,
				"children": {
					"150402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150402,
						"regionName": "红山区"
					},
					"150403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150403,
						"regionName": "元宝山区"
					},
					"150404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150404,
						"regionName": "松山区"
					},
					"150421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150421,
						"regionName": "阿鲁科尔沁旗"
					},
					"150422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150422,
						"regionName": "巴林左旗"
					},
					"150423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150423,
						"regionName": "巴林右旗"
					},
					"150424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150424,
						"regionName": "林西县"
					},
					"150425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150425,
						"regionName": "克什克腾旗"
					},
					"150426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150426,
						"regionName": "翁牛特旗"
					},
					"150428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150428,
						"regionName": "喀喇沁旗"
					},
					"150429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150429,
						"regionName": "宁城县"
					},
					"150430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150430,
						"regionName": "敖汉旗"
					}
				},
				"regionGrade": 2,
				"regionId": 150400,
				"regionName": "赤峰市"
			},
			"150500": {
				"areaId": 0,
				"children": {
					"150502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150502,
						"regionName": "科尔沁区"
					},
					"150521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150521,
						"regionName": "科尔沁左翼中旗"
					},
					"150522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150522,
						"regionName": "科尔沁左翼后旗"
					},
					"150523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150523,
						"regionName": "开鲁县"
					},
					"150524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150524,
						"regionName": "库伦旗"
					},
					"150525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150525,
						"regionName": "奈曼旗"
					},
					"150526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150526,
						"regionName": "扎鲁特旗"
					},
					"150581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150581,
						"regionName": "霍林郭勒市"
					}
				},
				"regionGrade": 2,
				"regionId": 150500,
				"regionName": "通辽市"
			},
			"150600": {
				"areaId": 0,
				"children": {
					"150602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150602,
						"regionName": "东胜区"
					},
					"150603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150603,
						"regionName": "康巴什区"
					},
					"150621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150621,
						"regionName": "达拉特旗"
					},
					"150622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150622,
						"regionName": "准格尔旗"
					},
					"150623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150623,
						"regionName": "鄂托克前旗"
					},
					"150624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150624,
						"regionName": "鄂托克旗"
					},
					"150625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150625,
						"regionName": "杭锦旗"
					},
					"150626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150626,
						"regionName": "乌审旗"
					},
					"150627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150627,
						"regionName": "伊金霍洛旗"
					}
				},
				"regionGrade": 2,
				"regionId": 150600,
				"regionName": "鄂尔多斯市"
			},
			"150700": {
				"areaId": 0,
				"children": {
					"150702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150702,
						"regionName": "海拉尔区"
					},
					"150703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150703,
						"regionName": "扎赉诺尔区"
					},
					"150721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150721,
						"regionName": "阿荣旗"
					},
					"150722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150722,
						"regionName": "莫力达瓦达斡尔族自治旗"
					},
					"150723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150723,
						"regionName": "鄂伦春自治旗"
					},
					"150724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150724,
						"regionName": "鄂温克族自治旗"
					},
					"150725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150725,
						"regionName": "陈巴尔虎旗"
					},
					"150726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150726,
						"regionName": "新巴尔虎左旗"
					},
					"150727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150727,
						"regionName": "新巴尔虎右旗"
					},
					"150781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150781,
						"regionName": "满洲里市"
					},
					"150782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150782,
						"regionName": "牙克石市"
					},
					"150783": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150783,
						"regionName": "扎兰屯市"
					},
					"150784": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150784,
						"regionName": "额尔古纳市"
					},
					"150785": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150785,
						"regionName": "根河市"
					}
				},
				"regionGrade": 2,
				"regionId": 150700,
				"regionName": "呼伦贝尔市"
			},
			"150800": {
				"areaId": 0,
				"children": {
					"150802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150802,
						"regionName": "临河区"
					},
					"150821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150821,
						"regionName": "五原县"
					},
					"150822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150822,
						"regionName": "磴口县"
					},
					"150823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150823,
						"regionName": "乌拉特前旗"
					},
					"150824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150824,
						"regionName": "乌拉特中旗"
					},
					"150825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150825,
						"regionName": "乌拉特后旗"
					},
					"150826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150826,
						"regionName": "杭锦后旗"
					}
				},
				"regionGrade": 2,
				"regionId": 150800,
				"regionName": "巴彦淖尔市"
			},
			"150900": {
				"areaId": 0,
				"children": {
					"150902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150902,
						"regionName": "集宁区"
					},
					"150921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150921,
						"regionName": "卓资县"
					},
					"150922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150922,
						"regionName": "化德县"
					},
					"150923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150923,
						"regionName": "商都县"
					},
					"150924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150924,
						"regionName": "兴和县"
					},
					"150925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150925,
						"regionName": "凉城县"
					},
					"150926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150926,
						"regionName": "察哈尔右翼前旗"
					},
					"150927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150927,
						"regionName": "察哈尔右翼中旗"
					},
					"150928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150928,
						"regionName": "察哈尔右翼后旗"
					},
					"150929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150929,
						"regionName": "四子王旗"
					},
					"150981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 150981,
						"regionName": "丰镇市"
					}
				},
				"regionGrade": 2,
				"regionId": 150900,
				"regionName": "乌兰察布市"
			},
			"152200": {
				"areaId": 0,
				"children": {
					"152201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152201,
						"regionName": "乌兰浩特市"
					},
					"152202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152202,
						"regionName": "阿尔山市"
					},
					"152221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152221,
						"regionName": "科尔沁右翼前旗"
					},
					"152222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152222,
						"regionName": "科尔沁右翼中旗"
					},
					"152223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152223,
						"regionName": "扎赉特旗"
					},
					"152224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152224,
						"regionName": "突泉县"
					}
				},
				"regionGrade": 2,
				"regionId": 152200,
				"regionName": "兴安盟"
			},
			"152500": {
				"areaId": 0,
				"children": {
					"152501": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152501,
						"regionName": "二连浩特市"
					},
					"152502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152502,
						"regionName": "锡林浩特市"
					},
					"152522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152522,
						"regionName": "阿巴嘎旗"
					},
					"152523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152523,
						"regionName": "苏尼特左旗"
					},
					"152524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152524,
						"regionName": "苏尼特右旗"
					},
					"152525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152525,
						"regionName": "东乌珠穆沁旗"
					},
					"152526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152526,
						"regionName": "西乌珠穆沁旗"
					},
					"152527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152527,
						"regionName": "太仆寺旗"
					},
					"152528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152528,
						"regionName": "镶黄旗"
					},
					"152529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152529,
						"regionName": "正镶白旗"
					},
					"152530": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152530,
						"regionName": "正蓝旗"
					},
					"152531": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152531,
						"regionName": "多伦县"
					}
				},
				"regionGrade": 2,
				"regionId": 152500,
				"regionName": "锡林郭勒盟"
			},
			"152900": {
				"areaId": 0,
				"children": {
					"152921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152921,
						"regionName": "阿拉善左旗"
					},
					"152922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152922,
						"regionName": "阿拉善右旗"
					},
					"152923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 152923,
						"regionName": "额济纳旗"
					}
				},
				"regionGrade": 2,
				"regionId": 152900,
				"regionName": "阿拉善盟"
			}
		},
		"regionGrade": 1,
		"regionId": 150000,
		"regionName": "内蒙古自治区"
	},
	"210000": {
		"areaId": 5,
		"children": {
			"210100": {
				"areaId": 0,
				"children": {
					"210102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210102,
						"regionName": "和平区"
					},
					"210103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210103,
						"regionName": "沈河区"
					},
					"210104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210104,
						"regionName": "大东区"
					},
					"210105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210105,
						"regionName": "皇姑区"
					},
					"210106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210106,
						"regionName": "铁西区"
					},
					"210111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210111,
						"regionName": "苏家屯区"
					},
					"210112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210112,
						"regionName": "浑南区"
					},
					"210113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210113,
						"regionName": "沈北新区"
					},
					"210114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210114,
						"regionName": "于洪区"
					},
					"210115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210115,
						"regionName": "辽中区"
					},
					"210123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210123,
						"regionName": "康平县"
					},
					"210124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210124,
						"regionName": "法库县"
					},
					"210181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210181,
						"regionName": "新民市"
					}
				},
				"regionGrade": 2,
				"regionId": 210100,
				"regionName": "沈阳市"
			},
			"210200": {
				"areaId": 0,
				"children": {
					"210202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210202,
						"regionName": "中山区"
					},
					"210203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210203,
						"regionName": "西岗区"
					},
					"210204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210204,
						"regionName": "沙河口区"
					},
					"210211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210211,
						"regionName": "甘井子区"
					},
					"210212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210212,
						"regionName": "旅顺口区"
					},
					"210213": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210213,
						"regionName": "金州区"
					},
					"210214": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210214,
						"regionName": "普兰店区"
					},
					"210224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210224,
						"regionName": "长海县"
					},
					"210281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210281,
						"regionName": "瓦房店市"
					},
					"210283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210283,
						"regionName": "庄河市"
					}
				},
				"regionGrade": 2,
				"regionId": 210200,
				"regionName": "大连市"
			},
			"210300": {
				"areaId": 0,
				"children": {
					"210302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210302,
						"regionName": "铁东区"
					},
					"210303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210303,
						"regionName": "铁西区"
					},
					"210304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210304,
						"regionName": "立山区"
					},
					"210311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210311,
						"regionName": "千山区"
					},
					"210321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210321,
						"regionName": "台安县"
					},
					"210323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210323,
						"regionName": "岫岩满族自治县"
					},
					"210381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210381,
						"regionName": "海城市"
					}
				},
				"regionGrade": 2,
				"regionId": 210300,
				"regionName": "鞍山市"
			},
			"210400": {
				"areaId": 0,
				"children": {
					"210402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210402,
						"regionName": "新抚区"
					},
					"210403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210403,
						"regionName": "东洲区"
					},
					"210404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210404,
						"regionName": "望花区"
					},
					"210411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210411,
						"regionName": "顺城区"
					},
					"210421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210421,
						"regionName": "抚顺县"
					},
					"210422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210422,
						"regionName": "新宾满族自治县"
					},
					"210423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210423,
						"regionName": "清原满族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 210400,
				"regionName": "抚顺市"
			},
			"210500": {
				"areaId": 0,
				"children": {
					"210502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210502,
						"regionName": "平山区"
					},
					"210503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210503,
						"regionName": "溪湖区"
					},
					"210504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210504,
						"regionName": "明山区"
					},
					"210505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210505,
						"regionName": "南芬区"
					},
					"210521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210521,
						"regionName": "本溪满族自治县"
					},
					"210522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210522,
						"regionName": "桓仁满族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 210500,
				"regionName": "本溪市"
			},
			"210600": {
				"areaId": 0,
				"children": {
					"210602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210602,
						"regionName": "元宝区"
					},
					"210603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210603,
						"regionName": "振兴区"
					},
					"210604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210604,
						"regionName": "振安区"
					},
					"210624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210624,
						"regionName": "宽甸满族自治县"
					},
					"210681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210681,
						"regionName": "东港市"
					},
					"210682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210682,
						"regionName": "凤城市"
					}
				},
				"regionGrade": 2,
				"regionId": 210600,
				"regionName": "丹东市"
			},
			"210700": {
				"areaId": 0,
				"children": {
					"210702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210702,
						"regionName": "古塔区"
					},
					"210703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210703,
						"regionName": "凌河区"
					},
					"210711": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210711,
						"regionName": "太和区"
					},
					"210726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210726,
						"regionName": "黑山县"
					},
					"210727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210727,
						"regionName": "义县"
					},
					"210781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210781,
						"regionName": "凌海市"
					},
					"210782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210782,
						"regionName": "北镇市"
					}
				},
				"regionGrade": 2,
				"regionId": 210700,
				"regionName": "锦州市"
			},
			"210800": {
				"areaId": 0,
				"children": {
					"210802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210802,
						"regionName": "站前区"
					},
					"210803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210803,
						"regionName": "西市区"
					},
					"210804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210804,
						"regionName": "鲅鱼圈区"
					},
					"210811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210811,
						"regionName": "老边区"
					},
					"210881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210881,
						"regionName": "盖州市"
					},
					"210882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210882,
						"regionName": "大石桥市"
					}
				},
				"regionGrade": 2,
				"regionId": 210800,
				"regionName": "营口市"
			},
			"210900": {
				"areaId": 0,
				"children": {
					"210902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210902,
						"regionName": "海州区"
					},
					"210903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210903,
						"regionName": "新邱区"
					},
					"210904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210904,
						"regionName": "太平区"
					},
					"210905": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210905,
						"regionName": "清河门区"
					},
					"210911": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210911,
						"regionName": "细河区"
					},
					"210921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210921,
						"regionName": "阜新蒙古族自治县"
					},
					"210922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 210922,
						"regionName": "彰武县"
					}
				},
				"regionGrade": 2,
				"regionId": 210900,
				"regionName": "阜新市"
			},
			"211000": {
				"areaId": 0,
				"children": {
					"211002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211002,
						"regionName": "白塔区"
					},
					"211003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211003,
						"regionName": "文圣区"
					},
					"211004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211004,
						"regionName": "宏伟区"
					},
					"211005": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211005,
						"regionName": "弓长岭区"
					},
					"211011": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211011,
						"regionName": "太子河区"
					},
					"211021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211021,
						"regionName": "辽阳县"
					},
					"211081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211081,
						"regionName": "灯塔市"
					}
				},
				"regionGrade": 2,
				"regionId": 211000,
				"regionName": "辽阳市"
			},
			"211100": {
				"areaId": 0,
				"children": {
					"211102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211102,
						"regionName": "双台子区"
					},
					"211103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211103,
						"regionName": "兴隆台区"
					},
					"211104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211104,
						"regionName": "大洼区"
					},
					"211122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211122,
						"regionName": "盘山县"
					}
				},
				"regionGrade": 2,
				"regionId": 211100,
				"regionName": "盘锦市"
			},
			"211200": {
				"areaId": 0,
				"children": {
					"211202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211202,
						"regionName": "银州区"
					},
					"211204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211204,
						"regionName": "清河区"
					},
					"211221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211221,
						"regionName": "铁岭县"
					},
					"211223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211223,
						"regionName": "西丰县"
					},
					"211224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211224,
						"regionName": "昌图县"
					},
					"211281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211281,
						"regionName": "调兵山市"
					},
					"211282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211282,
						"regionName": "开原市"
					}
				},
				"regionGrade": 2,
				"regionId": 211200,
				"regionName": "铁岭市"
			},
			"211300": {
				"areaId": 0,
				"children": {
					"211302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211302,
						"regionName": "双塔区"
					},
					"211303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211303,
						"regionName": "龙城区"
					},
					"211321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211321,
						"regionName": "朝阳县"
					},
					"211322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211322,
						"regionName": "建平县"
					},
					"211324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211324,
						"regionName": "喀喇沁左翼蒙古族自治县"
					},
					"211381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211381,
						"regionName": "北票市"
					},
					"211382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211382,
						"regionName": "凌源市"
					}
				},
				"regionGrade": 2,
				"regionId": 211300,
				"regionName": "朝阳市"
			},
			"211400": {
				"areaId": 0,
				"children": {
					"211402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211402,
						"regionName": "连山区"
					},
					"211403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211403,
						"regionName": "龙港区"
					},
					"211404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211404,
						"regionName": "南票区"
					},
					"211421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211421,
						"regionName": "绥中县"
					},
					"211422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211422,
						"regionName": "建昌县"
					},
					"211481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 211481,
						"regionName": "兴城市"
					}
				},
				"regionGrade": 2,
				"regionId": 211400,
				"regionName": "葫芦岛市"
			}
		},
		"regionGrade": 1,
		"regionId": 210000,
		"regionName": "辽宁省"
	},
	"220000": {
		"areaId": 5,
		"children": {
			"220100": {
				"areaId": 0,
				"children": {
					"220102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220102,
						"regionName": "南关区"
					},
					"220103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220103,
						"regionName": "宽城区"
					},
					"220104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220104,
						"regionName": "朝阳区"
					},
					"220105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220105,
						"regionName": "二道区"
					},
					"220106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220106,
						"regionName": "绿园区"
					},
					"220112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220112,
						"regionName": "双阳区"
					},
					"220113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220113,
						"regionName": "九台区"
					},
					"220122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220122,
						"regionName": "农安县"
					},
					"220182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220182,
						"regionName": "榆树市"
					},
					"220183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220183,
						"regionName": "德惠市"
					},
					"220184": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220184,
						"regionName": "净月经济开发区"
					},
					"220185": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220185,
						"regionName": "高新技术产业开发区"
					},
					"220186": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220186,
						"regionName": "经济技术开发区"
					}
				},
				"regionGrade": 2,
				"regionId": 220100,
				"regionName": "长春市"
			},
			"220200": {
				"areaId": 0,
				"children": {
					"220202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220202,
						"regionName": "昌邑区"
					},
					"220203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220203,
						"regionName": "龙潭区"
					},
					"220204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220204,
						"regionName": "船营区"
					},
					"220211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220211,
						"regionName": "丰满区"
					},
					"220221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220221,
						"regionName": "永吉县"
					},
					"220281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220281,
						"regionName": "蛟河市"
					},
					"220282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220282,
						"regionName": "桦甸市"
					},
					"220283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220283,
						"regionName": "舒兰市"
					},
					"220284": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220284,
						"regionName": "磐石市"
					}
				},
				"regionGrade": 2,
				"regionId": 220200,
				"regionName": "吉林市"
			},
			"220300": {
				"areaId": 0,
				"children": {
					"220302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220302,
						"regionName": "铁西区"
					},
					"220303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220303,
						"regionName": "铁东区"
					},
					"220322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220322,
						"regionName": "梨树县"
					},
					"220323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220323,
						"regionName": "伊通满族自治县"
					},
					"220381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220381,
						"regionName": "公主岭市"
					},
					"220382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220382,
						"regionName": "双辽市"
					}
				},
				"regionGrade": 2,
				"regionId": 220300,
				"regionName": "四平市"
			},
			"220400": {
				"areaId": 0,
				"children": {
					"220402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220402,
						"regionName": "龙山区"
					},
					"220403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220403,
						"regionName": "西安区"
					},
					"220421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220421,
						"regionName": "东丰县"
					},
					"220422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220422,
						"regionName": "东辽县"
					}
				},
				"regionGrade": 2,
				"regionId": 220400,
				"regionName": "辽源市"
			},
			"220500": {
				"areaId": 0,
				"children": {
					"220502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220502,
						"regionName": "东昌区"
					},
					"220503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220503,
						"regionName": "二道江区"
					},
					"220521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220521,
						"regionName": "通化县"
					},
					"220523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220523,
						"regionName": "辉南县"
					},
					"220524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220524,
						"regionName": "柳河县"
					},
					"220581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220581,
						"regionName": "梅河口市"
					},
					"220582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220582,
						"regionName": "集安市"
					}
				},
				"regionGrade": 2,
				"regionId": 220500,
				"regionName": "通化市"
			},
			"220600": {
				"areaId": 0,
				"children": {
					"220602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220602,
						"regionName": "浑江区"
					},
					"220605": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220605,
						"regionName": "江源区"
					},
					"220621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220621,
						"regionName": "抚松县"
					},
					"220622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220622,
						"regionName": "靖宇县"
					},
					"220623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220623,
						"regionName": "长白朝鲜族自治县"
					},
					"220681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220681,
						"regionName": "临江市"
					}
				},
				"regionGrade": 2,
				"regionId": 220600,
				"regionName": "白山市"
			},
			"220700": {
				"areaId": 0,
				"children": {
					"220702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220702,
						"regionName": "宁江区"
					},
					"220721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220721,
						"regionName": "前郭尔罗斯蒙古族自治县"
					},
					"220722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220722,
						"regionName": "长岭县"
					},
					"220723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220723,
						"regionName": "乾安县"
					},
					"220781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220781,
						"regionName": "扶余市"
					}
				},
				"regionGrade": 2,
				"regionId": 220700,
				"regionName": "松原市"
			},
			"220800": {
				"areaId": 0,
				"children": {
					"220802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220802,
						"regionName": "洮北区"
					},
					"220821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220821,
						"regionName": "镇赉县"
					},
					"220822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220822,
						"regionName": "通榆县"
					},
					"220881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220881,
						"regionName": "洮南市"
					},
					"220882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 220882,
						"regionName": "大安市"
					}
				},
				"regionGrade": 2,
				"regionId": 220800,
				"regionName": "白城市"
			},
			"222400": {
				"areaId": 0,
				"children": {
					"222401": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222401,
						"regionName": "延吉市"
					},
					"222402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222402,
						"regionName": "图们市"
					},
					"222403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222403,
						"regionName": "敦化市"
					},
					"222404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222404,
						"regionName": "珲春市"
					},
					"222405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222405,
						"regionName": "龙井市"
					},
					"222406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222406,
						"regionName": "和龙市"
					},
					"222424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222424,
						"regionName": "汪清县"
					},
					"222426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 222426,
						"regionName": "安图县"
					}
				},
				"regionGrade": 2,
				"regionId": 222400,
				"regionName": "延边朝鲜族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 220000,
		"regionName": "吉林省"
	},
	"230000": {
		"areaId": 5,
		"children": {
			"230100": {
				"areaId": 0,
				"children": {
					"230102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230102,
						"regionName": "道里区"
					},
					"230103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230103,
						"regionName": "南岗区"
					},
					"230104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230104,
						"regionName": "道外区"
					},
					"230108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230108,
						"regionName": "平房区"
					},
					"230109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230109,
						"regionName": "松北区"
					},
					"230110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230110,
						"regionName": "香坊区"
					},
					"230111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230111,
						"regionName": "呼兰区"
					},
					"230112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230112,
						"regionName": "阿城区"
					},
					"230113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230113,
						"regionName": "双城区"
					},
					"230123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230123,
						"regionName": "依兰县"
					},
					"230124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230124,
						"regionName": "方正县"
					},
					"230125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230125,
						"regionName": "宾县"
					},
					"230126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230126,
						"regionName": "巴彦县"
					},
					"230127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230127,
						"regionName": "木兰县"
					},
					"230128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230128,
						"regionName": "通河县"
					},
					"230129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230129,
						"regionName": "延寿县"
					},
					"230183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230183,
						"regionName": "尚志市"
					},
					"230184": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230184,
						"regionName": "五常市"
					}
				},
				"regionGrade": 2,
				"regionId": 230100,
				"regionName": "哈尔滨市"
			},
			"230200": {
				"areaId": 0,
				"children": {
					"230202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230202,
						"regionName": "龙沙区"
					},
					"230203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230203,
						"regionName": "建华区"
					},
					"230204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230204,
						"regionName": "铁锋区"
					},
					"230205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230205,
						"regionName": "昂昂溪区"
					},
					"230206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230206,
						"regionName": "富拉尔基区"
					},
					"230207": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230207,
						"regionName": "碾子山区"
					},
					"230208": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230208,
						"regionName": "梅里斯达斡尔族区"
					},
					"230221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230221,
						"regionName": "龙江县"
					},
					"230223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230223,
						"regionName": "依安县"
					},
					"230224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230224,
						"regionName": "泰来县"
					},
					"230225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230225,
						"regionName": "甘南县"
					},
					"230227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230227,
						"regionName": "富裕县"
					},
					"230229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230229,
						"regionName": "克山县"
					},
					"230230": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230230,
						"regionName": "克东县"
					},
					"230231": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230231,
						"regionName": "拜泉县"
					},
					"230281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230281,
						"regionName": "讷河市"
					}
				},
				"regionGrade": 2,
				"regionId": 230200,
				"regionName": "齐齐哈尔市"
			},
			"230300": {
				"areaId": 0,
				"children": {
					"230302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230302,
						"regionName": "鸡冠区"
					},
					"230303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230303,
						"regionName": "恒山区"
					},
					"230304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230304,
						"regionName": "滴道区"
					},
					"230305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230305,
						"regionName": "梨树区"
					},
					"230306": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230306,
						"regionName": "城子河区"
					},
					"230307": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230307,
						"regionName": "麻山区"
					},
					"230321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230321,
						"regionName": "鸡东县"
					},
					"230381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230381,
						"regionName": "虎林市"
					},
					"230382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230382,
						"regionName": "密山市"
					}
				},
				"regionGrade": 2,
				"regionId": 230300,
				"regionName": "鸡西市"
			},
			"230400": {
				"areaId": 0,
				"children": {
					"230402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230402,
						"regionName": "向阳区"
					},
					"230403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230403,
						"regionName": "工农区"
					},
					"230404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230404,
						"regionName": "南山区"
					},
					"230405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230405,
						"regionName": "兴安区"
					},
					"230406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230406,
						"regionName": "东山区"
					},
					"230407": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230407,
						"regionName": "兴山区"
					},
					"230421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230421,
						"regionName": "萝北县"
					},
					"230422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230422,
						"regionName": "绥滨县"
					}
				},
				"regionGrade": 2,
				"regionId": 230400,
				"regionName": "鹤岗市"
			},
			"230500": {
				"areaId": 0,
				"children": {
					"230502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230502,
						"regionName": "尖山区"
					},
					"230503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230503,
						"regionName": "岭东区"
					},
					"230505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230505,
						"regionName": "四方台区"
					},
					"230506": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230506,
						"regionName": "宝山区"
					},
					"230521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230521,
						"regionName": "集贤县"
					},
					"230522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230522,
						"regionName": "友谊县"
					},
					"230523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230523,
						"regionName": "宝清县"
					},
					"230524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230524,
						"regionName": "饶河县"
					}
				},
				"regionGrade": 2,
				"regionId": 230500,
				"regionName": "双鸭山市"
			},
			"230600": {
				"areaId": 0,
				"children": {
					"230602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230602,
						"regionName": "萨尔图区"
					},
					"230603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230603,
						"regionName": "龙凤区"
					},
					"230604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230604,
						"regionName": "让胡路区"
					},
					"230605": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230605,
						"regionName": "红岗区"
					},
					"230606": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230606,
						"regionName": "大同区"
					},
					"230621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230621,
						"regionName": "肇州县"
					},
					"230622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230622,
						"regionName": "肇源县"
					},
					"230623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230623,
						"regionName": "林甸县"
					},
					"230624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230624,
						"regionName": "杜尔伯特蒙古族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 230600,
				"regionName": "大庆市"
			},
			"230700": {
				"areaId": 0,
				"children": {
					"230702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230702,
						"regionName": "伊春区"
					},
					"230703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230703,
						"regionName": "南岔区"
					},
					"230704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230704,
						"regionName": "友好区"
					},
					"230705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230705,
						"regionName": "西林区"
					},
					"230706": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230706,
						"regionName": "翠峦区"
					},
					"230707": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230707,
						"regionName": "新青区"
					},
					"230708": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230708,
						"regionName": "美溪区"
					},
					"230709": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230709,
						"regionName": "金山屯区"
					},
					"230710": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230710,
						"regionName": "五营区"
					},
					"230711": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230711,
						"regionName": "乌马河区"
					},
					"230712": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230712,
						"regionName": "汤旺河区"
					},
					"230713": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230713,
						"regionName": "带岭区"
					},
					"230714": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230714,
						"regionName": "乌伊岭区"
					},
					"230715": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230715,
						"regionName": "红星区"
					},
					"230716": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230716,
						"regionName": "上甘岭区"
					},
					"230722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230722,
						"regionName": "嘉荫县"
					},
					"230781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230781,
						"regionName": "铁力市"
					}
				},
				"regionGrade": 2,
				"regionId": 230700,
				"regionName": "伊春市"
			},
			"230800": {
				"areaId": 0,
				"children": {
					"230803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230803,
						"regionName": "向阳区"
					},
					"230804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230804,
						"regionName": "前进区"
					},
					"230805": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230805,
						"regionName": "东风区"
					},
					"230811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230811,
						"regionName": "郊区"
					},
					"230822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230822,
						"regionName": "桦南县"
					},
					"230826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230826,
						"regionName": "桦川县"
					},
					"230828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230828,
						"regionName": "汤原县"
					},
					"230881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230881,
						"regionName": "同江市"
					},
					"230882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230882,
						"regionName": "富锦市"
					},
					"230883": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230883,
						"regionName": "抚远市"
					}
				},
				"regionGrade": 2,
				"regionId": 230800,
				"regionName": "佳木斯市"
			},
			"230900": {
				"areaId": 0,
				"children": {
					"230902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230902,
						"regionName": "新兴区"
					},
					"230903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230903,
						"regionName": "桃山区"
					},
					"230904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230904,
						"regionName": "茄子河区"
					},
					"230921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 230921,
						"regionName": "勃利县"
					}
				},
				"regionGrade": 2,
				"regionId": 230900,
				"regionName": "七台河市"
			},
			"231000": {
				"areaId": 0,
				"children": {
					"231002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231002,
						"regionName": "东安区"
					},
					"231003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231003,
						"regionName": "阳明区"
					},
					"231004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231004,
						"regionName": "爱民区"
					},
					"231005": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231005,
						"regionName": "西安区"
					},
					"231025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231025,
						"regionName": "林口县"
					},
					"231081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231081,
						"regionName": "绥芬河市"
					},
					"231083": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231083,
						"regionName": "海林市"
					},
					"231084": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231084,
						"regionName": "宁安市"
					},
					"231085": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231085,
						"regionName": "穆棱市"
					},
					"231086": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231086,
						"regionName": "东宁市"
					}
				},
				"regionGrade": 2,
				"regionId": 231000,
				"regionName": "牡丹江市"
			},
			"231100": {
				"areaId": 0,
				"children": {
					"231102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231102,
						"regionName": "爱辉区"
					},
					"231121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231121,
						"regionName": "嫩江县"
					},
					"231123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231123,
						"regionName": "逊克县"
					},
					"231124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231124,
						"regionName": "孙吴县"
					},
					"231181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231181,
						"regionName": "北安市"
					},
					"231182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231182,
						"regionName": "五大连池市"
					}
				},
				"regionGrade": 2,
				"regionId": 231100,
				"regionName": "黑河市"
			},
			"231200": {
				"areaId": 0,
				"children": {
					"231202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231202,
						"regionName": "北林区"
					},
					"231221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231221,
						"regionName": "望奎县"
					},
					"231222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231222,
						"regionName": "兰西县"
					},
					"231223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231223,
						"regionName": "青冈县"
					},
					"231224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231224,
						"regionName": "庆安县"
					},
					"231225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231225,
						"regionName": "明水县"
					},
					"231226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231226,
						"regionName": "绥棱县"
					},
					"231281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231281,
						"regionName": "安达市"
					},
					"231282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231282,
						"regionName": "肇东市"
					},
					"231283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 231283,
						"regionName": "海伦市"
					}
				},
				"regionGrade": 2,
				"regionId": 231200,
				"regionName": "绥化市"
			},
			"232700": {
				"areaId": 0,
				"children": {
					"232721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232721,
						"regionName": "呼玛县"
					},
					"232722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232722,
						"regionName": "塔河县"
					},
					"232723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232723,
						"regionName": "漠河县"
					},
					"232724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232724,
						"regionName": "加格达奇区"
					},
					"232725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232725,
						"regionName": "松岭区"
					},
					"232726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232726,
						"regionName": "新林区"
					},
					"232727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 232727,
						"regionName": "呼中区"
					}
				},
				"regionGrade": 2,
				"regionId": 232700,
				"regionName": "大兴安岭地区"
			}
		},
		"regionGrade": 1,
		"regionId": 230000,
		"regionName": "黑龙江省"
	},
	"310000": {
		"areaId": 1,
		"children": {
			"310100": {
				"areaId": 1,
				"children": {
					"310101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310101,
						"regionName": "黄浦区"
					},
					"310104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310104,
						"regionName": "徐汇区"
					},
					"310105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310105,
						"regionName": "长宁区"
					},
					"310106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310106,
						"regionName": "静安区"
					},
					"310107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310107,
						"regionName": "普陀区"
					},
					"310109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310109,
						"regionName": "虹口区"
					},
					"310110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310110,
						"regionName": "杨浦区"
					},
					"310112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310112,
						"regionName": "闵行区"
					},
					"310113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310113,
						"regionName": "宝山区"
					},
					"310114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310114,
						"regionName": "嘉定区"
					},
					"310115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310115,
						"regionName": "浦东新区"
					},
					"310116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310116,
						"regionName": "金山区"
					},
					"310117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310117,
						"regionName": "松江区"
					},
					"310118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310118,
						"regionName": "青浦区"
					},
					"310120": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310120,
						"regionName": "奉贤区"
					},
					"310151": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 310151,
						"regionName": "崇明区"
					}
				},
				"regionGrade": 2,
				"regionId": 310100,
				"regionName": "上海市"
			}
		},
		"regionGrade": 1,
		"regionId": 310000,
		"regionName": "上海市"
	},
	"320000": {
		"areaId": 1,
		"children": {
			"320100": {
				"areaId": 0,
				"children": {
					"320102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320102,
						"regionName": "玄武区"
					},
					"320104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320104,
						"regionName": "秦淮区"
					},
					"320105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320105,
						"regionName": "建邺区"
					},
					"320106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320106,
						"regionName": "鼓楼区"
					},
					"320111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320111,
						"regionName": "浦口区"
					},
					"320113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320113,
						"regionName": "栖霞区"
					},
					"320114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320114,
						"regionName": "雨花台区"
					},
					"320115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320115,
						"regionName": "江宁区"
					},
					"320116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320116,
						"regionName": "六合区"
					},
					"320117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320117,
						"regionName": "溧水区"
					},
					"320118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320118,
						"regionName": "高淳区"
					}
				},
				"regionGrade": 2,
				"regionId": 320100,
				"regionName": "南京市"
			},
			"320200": {
				"areaId": 0,
				"children": {
					"320205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320205,
						"regionName": "锡山区"
					},
					"320206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320206,
						"regionName": "惠山区"
					},
					"320211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320211,
						"regionName": "滨湖区"
					},
					"320213": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320213,
						"regionName": "梁溪区"
					},
					"320214": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320214,
						"regionName": "新吴区"
					},
					"320281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320281,
						"regionName": "江阴市"
					},
					"320282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320282,
						"regionName": "宜兴市"
					}
				},
				"regionGrade": 2,
				"regionId": 320200,
				"regionName": "无锡市"
			},
			"320300": {
				"areaId": 0,
				"children": {
					"320302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320302,
						"regionName": "鼓楼区"
					},
					"320303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320303,
						"regionName": "云龙区"
					},
					"320305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320305,
						"regionName": "贾汪区"
					},
					"320311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320311,
						"regionName": "泉山区"
					},
					"320312": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320312,
						"regionName": "铜山区"
					},
					"320321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320321,
						"regionName": "丰县"
					},
					"320322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320322,
						"regionName": "沛县"
					},
					"320324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320324,
						"regionName": "睢宁县"
					},
					"320381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320381,
						"regionName": "新沂市"
					},
					"320382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320382,
						"regionName": "邳州市"
					}
				},
				"regionGrade": 2,
				"regionId": 320300,
				"regionName": "徐州市"
			},
			"320400": {
				"areaId": 0,
				"children": {
					"320402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320402,
						"regionName": "天宁区"
					},
					"320404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320404,
						"regionName": "钟楼区"
					},
					"320411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320411,
						"regionName": "新北区"
					},
					"320412": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320412,
						"regionName": "武进区"
					},
					"320413": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320413,
						"regionName": "金坛区"
					},
					"320481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320481,
						"regionName": "溧阳市"
					}
				},
				"regionGrade": 2,
				"regionId": 320400,
				"regionName": "常州市"
			},
			"320500": {
				"areaId": 0,
				"children": {
					"320505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320505,
						"regionName": "虎丘区"
					},
					"320506": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320506,
						"regionName": "吴中区"
					},
					"320507": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320507,
						"regionName": "相城区"
					},
					"320509": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320509,
						"regionName": "吴江区"
					},
					"320581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320581,
						"regionName": "常熟市"
					},
					"320582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320582,
						"regionName": "张家港市"
					},
					"320583": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320583,
						"regionName": "昆山市"
					},
					"320585": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320585,
						"regionName": "太仓市"
					},
					"320586": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320586,
						"regionName": "工业园区"
					},
					"320587": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320587,
						"regionName": "高新区"
					},
					"320588": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320588,
						"regionName": "姑苏区"
					}
				},
				"regionGrade": 2,
				"regionId": 320500,
				"regionName": "苏州市"
			},
			"320600": {
				"areaId": 0,
				"children": {
					"320602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320602,
						"regionName": "崇川区"
					},
					"320611": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320611,
						"regionName": "港闸区"
					},
					"320612": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320612,
						"regionName": "通州区"
					},
					"320621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320621,
						"regionName": "海安县"
					},
					"320623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320623,
						"regionName": "如东县"
					},
					"320681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320681,
						"regionName": "启东市"
					},
					"320682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320682,
						"regionName": "如皋市"
					},
					"320684": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320684,
						"regionName": "海门市"
					}
				},
				"regionGrade": 2,
				"regionId": 320600,
				"regionName": "南通市"
			},
			"320700": {
				"areaId": 0,
				"children": {
					"320703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320703,
						"regionName": "连云区"
					},
					"320706": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320706,
						"regionName": "海州区"
					},
					"320707": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320707,
						"regionName": "赣榆区"
					},
					"320722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320722,
						"regionName": "东海县"
					},
					"320723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320723,
						"regionName": "灌云县"
					},
					"320724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320724,
						"regionName": "灌南县"
					}
				},
				"regionGrade": 2,
				"regionId": 320700,
				"regionName": "连云港市"
			},
			"320800": {
				"areaId": 0,
				"children": {
					"320803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320803,
						"regionName": "淮安区"
					},
					"320804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320804,
						"regionName": "淮阴区"
					},
					"320812": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320812,
						"regionName": "清江浦区"
					},
					"320813": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320813,
						"regionName": "洪泽区"
					},
					"320826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320826,
						"regionName": "涟水县"
					},
					"320830": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320830,
						"regionName": "盱眙县"
					},
					"320831": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320831,
						"regionName": "金湖县"
					},
					"320832": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320832,
						"regionName": "清河区"
					},
					"320833": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320833,
						"regionName": "楚州区"
					}
				},
				"regionGrade": 2,
				"regionId": 320800,
				"regionName": "淮安市"
			},
			"320900": {
				"areaId": 0,
				"children": {
					"320902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320902,
						"regionName": "亭湖区"
					},
					"320903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320903,
						"regionName": "盐都区"
					},
					"320904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320904,
						"regionName": "大丰区"
					},
					"320921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320921,
						"regionName": "响水县"
					},
					"320922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320922,
						"regionName": "滨海县"
					},
					"320923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320923,
						"regionName": "阜宁县"
					},
					"320924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320924,
						"regionName": "射阳县"
					},
					"320925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320925,
						"regionName": "建湖县"
					},
					"320981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 320981,
						"regionName": "东台市"
					}
				},
				"regionGrade": 2,
				"regionId": 320900,
				"regionName": "盐城市"
			},
			"321000": {
				"areaId": 0,
				"children": {
					"321002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321002,
						"regionName": "广陵区"
					},
					"321003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321003,
						"regionName": "邗江区"
					},
					"321012": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321012,
						"regionName": "江都区"
					},
					"321023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321023,
						"regionName": "宝应县"
					},
					"321081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321081,
						"regionName": "仪征市"
					},
					"321084": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321084,
						"regionName": "高邮市"
					}
				},
				"regionGrade": 2,
				"regionId": 321000,
				"regionName": "扬州市"
			},
			"321100": {
				"areaId": 0,
				"children": {
					"321102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321102,
						"regionName": "京口区"
					},
					"321111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321111,
						"regionName": "润州区"
					},
					"321112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321112,
						"regionName": "丹徒区"
					},
					"321181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321181,
						"regionName": "丹阳市"
					},
					"321182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321182,
						"regionName": "扬中市"
					},
					"321183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321183,
						"regionName": "句容市"
					}
				},
				"regionGrade": 2,
				"regionId": 321100,
				"regionName": "镇江市"
			},
			"321200": {
				"areaId": 0,
				"children": {
					"321202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321202,
						"regionName": "海陵区"
					},
					"321203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321203,
						"regionName": "高港区"
					},
					"321204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321204,
						"regionName": "姜堰区"
					},
					"321281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321281,
						"regionName": "兴化市"
					},
					"321282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321282,
						"regionName": "靖江市"
					},
					"321283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321283,
						"regionName": "泰兴市"
					}
				},
				"regionGrade": 2,
				"regionId": 321200,
				"regionName": "泰州市"
			},
			"321300": {
				"areaId": 0,
				"children": {
					"321302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321302,
						"regionName": "宿城区"
					},
					"321311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321311,
						"regionName": "宿豫区"
					},
					"321322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321322,
						"regionName": "沭阳县"
					},
					"321323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321323,
						"regionName": "泗阳县"
					},
					"321324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 321324,
						"regionName": "泗洪县"
					}
				},
				"regionGrade": 2,
				"regionId": 321300,
				"regionName": "宿迁市"
			}
		},
		"regionGrade": 1,
		"regionId": 320000,
		"regionName": "江苏省"
	},
	"330000": {
		"areaId": 1,
		"children": {
			"330100": {
				"areaId": 0,
				"children": {
					"330102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330102,
						"regionName": "上城区"
					},
					"330103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330103,
						"regionName": "下城区"
					},
					"330104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330104,
						"regionName": "江干区"
					},
					"330105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330105,
						"regionName": "拱墅区"
					},
					"330106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330106,
						"regionName": "西湖区"
					},
					"330108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330108,
						"regionName": "滨江区"
					},
					"330109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330109,
						"regionName": "萧山区"
					},
					"330110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330110,
						"regionName": "余杭区"
					},
					"330111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330111,
						"regionName": "富阳区"
					},
					"330112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330112,
						"regionName": "临安区"
					},
					"330122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330122,
						"regionName": "桐庐县"
					},
					"330127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330127,
						"regionName": "淳安县"
					},
					"330182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330182,
						"regionName": "建德市"
					}
				},
				"regionGrade": 2,
				"regionId": 330100,
				"regionName": "杭州市"
			},
			"330200": {
				"areaId": 0,
				"children": {
					"330203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330203,
						"regionName": "海曙区"
					},
					"330205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330205,
						"regionName": "江北区"
					},
					"330206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330206,
						"regionName": "北仑区"
					},
					"330211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330211,
						"regionName": "镇海区"
					},
					"330212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330212,
						"regionName": "鄞州区"
					},
					"330213": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330213,
						"regionName": "奉化区"
					},
					"330225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330225,
						"regionName": "象山县"
					},
					"330226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330226,
						"regionName": "宁海县"
					},
					"330281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330281,
						"regionName": "余姚市"
					},
					"330282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330282,
						"regionName": "慈溪市"
					}
				},
				"regionGrade": 2,
				"regionId": 330200,
				"regionName": "宁波市"
			},
			"330300": {
				"areaId": 0,
				"children": {
					"330302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330302,
						"regionName": "鹿城区"
					},
					"330303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330303,
						"regionName": "龙湾区"
					},
					"330304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330304,
						"regionName": "瓯海区"
					},
					"330305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330305,
						"regionName": "洞头区"
					},
					"330324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330324,
						"regionName": "永嘉县"
					},
					"330326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330326,
						"regionName": "平阳县"
					},
					"330327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330327,
						"regionName": "苍南县"
					},
					"330328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330328,
						"regionName": "文成县"
					},
					"330329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330329,
						"regionName": "泰顺县"
					},
					"330381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330381,
						"regionName": "瑞安市"
					},
					"330382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330382,
						"regionName": "乐清市"
					}
				},
				"regionGrade": 2,
				"regionId": 330300,
				"regionName": "温州市"
			},
			"330400": {
				"areaId": 0,
				"children": {
					"330402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330402,
						"regionName": "南湖区"
					},
					"330411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330411,
						"regionName": "秀洲区"
					},
					"330421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330421,
						"regionName": "嘉善县"
					},
					"330424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330424,
						"regionName": "海盐县"
					},
					"330481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330481,
						"regionName": "海宁市"
					},
					"330482": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330482,
						"regionName": "平湖市"
					},
					"330483": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330483,
						"regionName": "桐乡市"
					}
				},
				"regionGrade": 2,
				"regionId": 330400,
				"regionName": "嘉兴市"
			},
			"330500": {
				"areaId": 0,
				"children": {
					"330502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330502,
						"regionName": "吴兴区"
					},
					"330503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330503,
						"regionName": "南浔区"
					},
					"330521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330521,
						"regionName": "德清县"
					},
					"330522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330522,
						"regionName": "长兴县"
					},
					"330523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330523,
						"regionName": "安吉县"
					}
				},
				"regionGrade": 2,
				"regionId": 330500,
				"regionName": "湖州市"
			},
			"330600": {
				"areaId": 0,
				"children": {
					"330602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330602,
						"regionName": "越城区"
					},
					"330603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330603,
						"regionName": "柯桥区"
					},
					"330604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330604,
						"regionName": "上虞区"
					},
					"330624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330624,
						"regionName": "新昌县"
					},
					"330681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330681,
						"regionName": "诸暨市"
					},
					"330683": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330683,
						"regionName": "嵊州市"
					}
				},
				"regionGrade": 2,
				"regionId": 330600,
				"regionName": "绍兴市"
			},
			"330700": {
				"areaId": 0,
				"children": {
					"330702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330702,
						"regionName": "婺城区"
					},
					"330703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330703,
						"regionName": "金东区"
					},
					"330723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330723,
						"regionName": "武义县"
					},
					"330726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330726,
						"regionName": "浦江县"
					},
					"330727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330727,
						"regionName": "磐安县"
					},
					"330781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330781,
						"regionName": "兰溪市"
					},
					"330782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330782,
						"regionName": "义乌市"
					},
					"330783": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330783,
						"regionName": "东阳市"
					},
					"330784": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330784,
						"regionName": "永康市"
					}
				},
				"regionGrade": 2,
				"regionId": 330700,
				"regionName": "金华市"
			},
			"330800": {
				"areaId": 0,
				"children": {
					"330802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330802,
						"regionName": "柯城区"
					},
					"330803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330803,
						"regionName": "衢江区"
					},
					"330822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330822,
						"regionName": "常山县"
					},
					"330824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330824,
						"regionName": "开化县"
					},
					"330825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330825,
						"regionName": "龙游县"
					},
					"330881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330881,
						"regionName": "江山市"
					}
				},
				"regionGrade": 2,
				"regionId": 330800,
				"regionName": "衢州市"
			},
			"330900": {
				"areaId": 0,
				"children": {
					"330902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330902,
						"regionName": "定海区"
					},
					"330903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330903,
						"regionName": "普陀区"
					},
					"330921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330921,
						"regionName": "岱山县"
					},
					"330922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 330922,
						"regionName": "嵊泗县"
					}
				},
				"regionGrade": 2,
				"regionId": 330900,
				"regionName": "舟山市"
			},
			"331000": {
				"areaId": 0,
				"children": {
					"331002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331002,
						"regionName": "椒江区"
					},
					"331003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331003,
						"regionName": "黄岩区"
					},
					"331004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331004,
						"regionName": "路桥区"
					},
					"331022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331022,
						"regionName": "三门县"
					},
					"331023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331023,
						"regionName": "天台县"
					},
					"331024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331024,
						"regionName": "仙居县"
					},
					"331081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331081,
						"regionName": "温岭市"
					},
					"331082": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331082,
						"regionName": "临海市"
					},
					"331083": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331083,
						"regionName": "玉环市"
					}
				},
				"regionGrade": 2,
				"regionId": 331000,
				"regionName": "台州市"
			},
			"331100": {
				"areaId": 0,
				"children": {
					"331102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331102,
						"regionName": "莲都区"
					},
					"331121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331121,
						"regionName": "青田县"
					},
					"331122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331122,
						"regionName": "缙云县"
					},
					"331123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331123,
						"regionName": "遂昌县"
					},
					"331124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331124,
						"regionName": "松阳县"
					},
					"331125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331125,
						"regionName": "云和县"
					},
					"331126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331126,
						"regionName": "庆元县"
					},
					"331127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331127,
						"regionName": "景宁畲族自治县"
					},
					"331181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 331181,
						"regionName": "龙泉市"
					}
				},
				"regionGrade": 2,
				"regionId": 331100,
				"regionName": "丽水市"
			}
		},
		"regionGrade": 1,
		"regionId": 330000,
		"regionName": "浙江省"
	},
	"340000": {
		"areaId": 1,
		"children": {
			"340100": {
				"areaId": 0,
				"children": {
					"340102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340102,
						"regionName": "瑶海区"
					},
					"340103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340103,
						"regionName": "庐阳区"
					},
					"340104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340104,
						"regionName": "蜀山区"
					},
					"340111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340111,
						"regionName": "包河区"
					},
					"340121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340121,
						"regionName": "长丰县"
					},
					"340122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340122,
						"regionName": "肥东县"
					},
					"340123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340123,
						"regionName": "肥西县"
					},
					"340124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340124,
						"regionName": "庐江县"
					},
					"340181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340181,
						"regionName": "巢湖市"
					}
				},
				"regionGrade": 2,
				"regionId": 340100,
				"regionName": "合肥市"
			},
			"340200": {
				"areaId": 0,
				"children": {
					"340202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340202,
						"regionName": "镜湖区"
					},
					"340203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340203,
						"regionName": "弋江区"
					},
					"340207": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340207,
						"regionName": "鸠江区"
					},
					"340208": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340208,
						"regionName": "三山区"
					},
					"340221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340221,
						"regionName": "芜湖县"
					},
					"340222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340222,
						"regionName": "繁昌县"
					},
					"340223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340223,
						"regionName": "南陵县"
					},
					"340225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340225,
						"regionName": "无为县"
					}
				},
				"regionGrade": 2,
				"regionId": 340200,
				"regionName": "芜湖市"
			},
			"340300": {
				"areaId": 0,
				"children": {
					"340302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340302,
						"regionName": "龙子湖区"
					},
					"340303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340303,
						"regionName": "蚌山区"
					},
					"340304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340304,
						"regionName": "禹会区"
					},
					"340311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340311,
						"regionName": "淮上区"
					},
					"340321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340321,
						"regionName": "怀远县"
					},
					"340322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340322,
						"regionName": "五河县"
					},
					"340323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340323,
						"regionName": "固镇县"
					}
				},
				"regionGrade": 2,
				"regionId": 340300,
				"regionName": "蚌埠市"
			},
			"340400": {
				"areaId": 0,
				"children": {
					"340402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340402,
						"regionName": "大通区"
					},
					"340403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340403,
						"regionName": "田家庵区"
					},
					"340404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340404,
						"regionName": "谢家集区"
					},
					"340405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340405,
						"regionName": "八公山区"
					},
					"340406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340406,
						"regionName": "潘集区"
					},
					"340421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340421,
						"regionName": "凤台县"
					},
					"340422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340422,
						"regionName": "寿县"
					}
				},
				"regionGrade": 2,
				"regionId": 340400,
				"regionName": "淮南市"
			},
			"340500": {
				"areaId": 0,
				"children": {
					"340503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340503,
						"regionName": "花山区"
					},
					"340504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340504,
						"regionName": "雨山区"
					},
					"340506": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340506,
						"regionName": "博望区"
					},
					"340521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340521,
						"regionName": "当涂县"
					},
					"340522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340522,
						"regionName": "含山县"
					},
					"340523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340523,
						"regionName": "和县"
					}
				},
				"regionGrade": 2,
				"regionId": 340500,
				"regionName": "马鞍山市"
			},
			"340600": {
				"areaId": 0,
				"children": {
					"340602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340602,
						"regionName": "杜集区"
					},
					"340603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340603,
						"regionName": "相山区"
					},
					"340604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340604,
						"regionName": "烈山区"
					},
					"340621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340621,
						"regionName": "濉溪县"
					}
				},
				"regionGrade": 2,
				"regionId": 340600,
				"regionName": "淮北市"
			},
			"340700": {
				"areaId": 0,
				"children": {
					"340705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340705,
						"regionName": "铜官区"
					},
					"340706": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340706,
						"regionName": "义安区"
					},
					"340711": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340711,
						"regionName": "郊区"
					},
					"340722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340722,
						"regionName": "枞阳县"
					}
				},
				"regionGrade": 2,
				"regionId": 340700,
				"regionName": "铜陵市"
			},
			"340800": {
				"areaId": 0,
				"children": {
					"340802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340802,
						"regionName": "迎江区"
					},
					"340803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340803,
						"regionName": "大观区"
					},
					"340811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340811,
						"regionName": "宜秀区"
					},
					"340822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340822,
						"regionName": "怀宁县"
					},
					"340824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340824,
						"regionName": "潜山县"
					},
					"340825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340825,
						"regionName": "太湖县"
					},
					"340826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340826,
						"regionName": "宿松县"
					},
					"340827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340827,
						"regionName": "望江县"
					},
					"340828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340828,
						"regionName": "岳西县"
					},
					"340881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 340881,
						"regionName": "桐城市"
					}
				},
				"regionGrade": 2,
				"regionId": 340800,
				"regionName": "安庆市"
			},
			"341000": {
				"areaId": 0,
				"children": {
					"341002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341002,
						"regionName": "屯溪区"
					},
					"341003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341003,
						"regionName": "黄山区"
					},
					"341004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341004,
						"regionName": "徽州区"
					},
					"341021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341021,
						"regionName": "歙县"
					},
					"341022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341022,
						"regionName": "休宁县"
					},
					"341023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341023,
						"regionName": "黟县"
					},
					"341024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341024,
						"regionName": "祁门县"
					}
				},
				"regionGrade": 2,
				"regionId": 341000,
				"regionName": "黄山市"
			},
			"341100": {
				"areaId": 0,
				"children": {
					"341102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341102,
						"regionName": "琅琊区"
					},
					"341103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341103,
						"regionName": "南谯区"
					},
					"341122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341122,
						"regionName": "来安县"
					},
					"341124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341124,
						"regionName": "全椒县"
					},
					"341125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341125,
						"regionName": "定远县"
					},
					"341126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341126,
						"regionName": "凤阳县"
					},
					"341181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341181,
						"regionName": "天长市"
					},
					"341182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341182,
						"regionName": "明光市"
					}
				},
				"regionGrade": 2,
				"regionId": 341100,
				"regionName": "滁州市"
			},
			"341200": {
				"areaId": 0,
				"children": {
					"341202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341202,
						"regionName": "颍州区"
					},
					"341203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341203,
						"regionName": "颍东区"
					},
					"341204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341204,
						"regionName": "颍泉区"
					},
					"341221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341221,
						"regionName": "临泉县"
					},
					"341222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341222,
						"regionName": "太和县"
					},
					"341225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341225,
						"regionName": "阜南县"
					},
					"341226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341226,
						"regionName": "颍上县"
					},
					"341282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341282,
						"regionName": "界首市"
					},
					"341283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341283,
						"regionName": "经济开发区"
					}
				},
				"regionGrade": 2,
				"regionId": 341200,
				"regionName": "阜阳市"
			},
			"341300": {
				"areaId": 0,
				"children": {
					"341302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341302,
						"regionName": "埇桥区"
					},
					"341321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341321,
						"regionName": "砀山县"
					},
					"341322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341322,
						"regionName": "萧县"
					},
					"341323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341323,
						"regionName": "灵璧县"
					},
					"341324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341324,
						"regionName": "泗县"
					}
				},
				"regionGrade": 2,
				"regionId": 341300,
				"regionName": "宿州市"
			},
			"341500": {
				"areaId": 0,
				"children": {
					"341502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341502,
						"regionName": "金安区"
					},
					"341503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341503,
						"regionName": "裕安区"
					},
					"341504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341504,
						"regionName": "叶集区"
					},
					"341522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341522,
						"regionName": "霍邱县"
					},
					"341523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341523,
						"regionName": "舒城县"
					},
					"341524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341524,
						"regionName": "金寨县"
					},
					"341525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341525,
						"regionName": "霍山县"
					}
				},
				"regionGrade": 2,
				"regionId": 341500,
				"regionName": "六安市"
			},
			"341600": {
				"areaId": 0,
				"children": {
					"341602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341602,
						"regionName": "谯城区"
					},
					"341621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341621,
						"regionName": "涡阳县"
					},
					"341622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341622,
						"regionName": "蒙城县"
					},
					"341623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341623,
						"regionName": "利辛县"
					}
				},
				"regionGrade": 2,
				"regionId": 341600,
				"regionName": "亳州市"
			},
			"341700": {
				"areaId": 0,
				"children": {
					"341702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341702,
						"regionName": "贵池区"
					},
					"341721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341721,
						"regionName": "东至县"
					},
					"341722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341722,
						"regionName": "石台县"
					},
					"341723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341723,
						"regionName": "青阳县"
					}
				},
				"regionGrade": 2,
				"regionId": 341700,
				"regionName": "池州市"
			},
			"341800": {
				"areaId": 0,
				"children": {
					"341802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341802,
						"regionName": "宣州区"
					},
					"341821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341821,
						"regionName": "郎溪县"
					},
					"341822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341822,
						"regionName": "广德县"
					},
					"341823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341823,
						"regionName": "泾县"
					},
					"341824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341824,
						"regionName": "绩溪县"
					},
					"341825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341825,
						"regionName": "旌德县"
					},
					"341881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 341881,
						"regionName": "宁国市"
					}
				},
				"regionGrade": 2,
				"regionId": 341800,
				"regionName": "宣城市"
			}
		},
		"regionGrade": 1,
		"regionId": 340000,
		"regionName": "安徽省"
	},
	"350000": {
		"areaId": 4,
		"children": {
			"350100": {
				"areaId": 0,
				"children": {
					"350102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350102,
						"regionName": "鼓楼区"
					},
					"350103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350103,
						"regionName": "台江区"
					},
					"350104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350104,
						"regionName": "仓山区"
					},
					"350105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350105,
						"regionName": "马尾区"
					},
					"350111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350111,
						"regionName": "晋安区"
					},
					"350112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350112,
						"regionName": "长乐区"
					},
					"350121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350121,
						"regionName": "闽侯县"
					},
					"350122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350122,
						"regionName": "连江县"
					},
					"350123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350123,
						"regionName": "罗源县"
					},
					"350124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350124,
						"regionName": "闽清县"
					},
					"350125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350125,
						"regionName": "永泰县"
					},
					"350128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350128,
						"regionName": "平潭县"
					},
					"350181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350181,
						"regionName": "福清市"
					}
				},
				"regionGrade": 2,
				"regionId": 350100,
				"regionName": "福州市"
			},
			"350200": {
				"areaId": 0,
				"children": {
					"350203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350203,
						"regionName": "思明区"
					},
					"350205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350205,
						"regionName": "海沧区"
					},
					"350206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350206,
						"regionName": "湖里区"
					},
					"350211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350211,
						"regionName": "集美区"
					},
					"350212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350212,
						"regionName": "同安区"
					},
					"350213": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350213,
						"regionName": "翔安区"
					}
				},
				"regionGrade": 2,
				"regionId": 350200,
				"regionName": "厦门市"
			},
			"350300": {
				"areaId": 0,
				"children": {
					"350302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350302,
						"regionName": "城厢区"
					},
					"350303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350303,
						"regionName": "涵江区"
					},
					"350304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350304,
						"regionName": "荔城区"
					},
					"350305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350305,
						"regionName": "秀屿区"
					},
					"350322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350322,
						"regionName": "仙游县"
					}
				},
				"regionGrade": 2,
				"regionId": 350300,
				"regionName": "莆田市"
			},
			"350400": {
				"areaId": 0,
				"children": {
					"350402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350402,
						"regionName": "梅列区"
					},
					"350403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350403,
						"regionName": "三元区"
					},
					"350421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350421,
						"regionName": "明溪县"
					},
					"350423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350423,
						"regionName": "清流县"
					},
					"350424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350424,
						"regionName": "宁化县"
					},
					"350425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350425,
						"regionName": "大田县"
					},
					"350426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350426,
						"regionName": "尤溪县"
					},
					"350427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350427,
						"regionName": "沙县"
					},
					"350428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350428,
						"regionName": "将乐县"
					},
					"350429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350429,
						"regionName": "泰宁县"
					},
					"350430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350430,
						"regionName": "建宁县"
					},
					"350481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350481,
						"regionName": "永安市"
					}
				},
				"regionGrade": 2,
				"regionId": 350400,
				"regionName": "三明市"
			},
			"350500": {
				"areaId": 0,
				"children": {
					"350502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350502,
						"regionName": "鲤城区"
					},
					"350503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350503,
						"regionName": "丰泽区"
					},
					"350504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350504,
						"regionName": "洛江区"
					},
					"350505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350505,
						"regionName": "泉港区"
					},
					"350521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350521,
						"regionName": "惠安县"
					},
					"350524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350524,
						"regionName": "安溪县"
					},
					"350525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350525,
						"regionName": "永春县"
					},
					"350526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350526,
						"regionName": "德化县"
					},
					"350527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350527,
						"regionName": "金门县"
					},
					"350581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350581,
						"regionName": "石狮市"
					},
					"350582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350582,
						"regionName": "晋江市"
					},
					"350583": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350583,
						"regionName": "南安市"
					}
				},
				"regionGrade": 2,
				"regionId": 350500,
				"regionName": "泉州市"
			},
			"350600": {
				"areaId": 0,
				"children": {
					"350602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350602,
						"regionName": "芗城区"
					},
					"350603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350603,
						"regionName": "龙文区"
					},
					"350622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350622,
						"regionName": "云霄县"
					},
					"350623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350623,
						"regionName": "漳浦县"
					},
					"350624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350624,
						"regionName": "诏安县"
					},
					"350625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350625,
						"regionName": "长泰县"
					},
					"350626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350626,
						"regionName": "东山县"
					},
					"350627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350627,
						"regionName": "南靖县"
					},
					"350628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350628,
						"regionName": "平和县"
					},
					"350629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350629,
						"regionName": "华安县"
					},
					"350681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350681,
						"regionName": "龙海市"
					}
				},
				"regionGrade": 2,
				"regionId": 350600,
				"regionName": "漳州市"
			},
			"350700": {
				"areaId": 0,
				"children": {
					"350702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350702,
						"regionName": "延平区"
					},
					"350703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350703,
						"regionName": "建阳区"
					},
					"350721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350721,
						"regionName": "顺昌县"
					},
					"350722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350722,
						"regionName": "浦城县"
					},
					"350723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350723,
						"regionName": "光泽县"
					},
					"350724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350724,
						"regionName": "松溪县"
					},
					"350725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350725,
						"regionName": "政和县"
					},
					"350781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350781,
						"regionName": "邵武市"
					},
					"350782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350782,
						"regionName": "武夷山市"
					},
					"350783": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350783,
						"regionName": "建瓯市"
					}
				},
				"regionGrade": 2,
				"regionId": 350700,
				"regionName": "南平市"
			},
			"350800": {
				"areaId": 0,
				"children": {
					"350802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350802,
						"regionName": "新罗区"
					},
					"350803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350803,
						"regionName": "永定区"
					},
					"350821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350821,
						"regionName": "长汀县"
					},
					"350823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350823,
						"regionName": "上杭县"
					},
					"350824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350824,
						"regionName": "武平县"
					},
					"350825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350825,
						"regionName": "连城县"
					},
					"350881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350881,
						"regionName": "漳平市"
					}
				},
				"regionGrade": 2,
				"regionId": 350800,
				"regionName": "龙岩市"
			},
			"350900": {
				"areaId": 0,
				"children": {
					"350902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350902,
						"regionName": "蕉城区"
					},
					"350921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350921,
						"regionName": "霞浦县"
					},
					"350922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350922,
						"regionName": "古田县"
					},
					"350923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350923,
						"regionName": "屏南县"
					},
					"350924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350924,
						"regionName": "寿宁县"
					},
					"350925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350925,
						"regionName": "周宁县"
					},
					"350926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350926,
						"regionName": "柘荣县"
					},
					"350981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350981,
						"regionName": "福安市"
					},
					"350982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 350982,
						"regionName": "福鼎市"
					}
				},
				"regionGrade": 2,
				"regionId": 350900,
				"regionName": "宁德市"
			}
		},
		"regionGrade": 1,
		"regionId": 350000,
		"regionName": "福建省"
	},
	"360000": {
		"areaId": 1,
		"children": {
			"360100": {
				"areaId": 0,
				"children": {
					"360102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360102,
						"regionName": "东湖区"
					},
					"360103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360103,
						"regionName": "西湖区"
					},
					"360104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360104,
						"regionName": "青云谱区"
					},
					"360105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360105,
						"regionName": "湾里区"
					},
					"360111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360111,
						"regionName": "青山湖区"
					},
					"360112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360112,
						"regionName": "新建区"
					},
					"360121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360121,
						"regionName": "南昌县"
					},
					"360123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360123,
						"regionName": "安义县"
					},
					"360124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360124,
						"regionName": "进贤县"
					},
					"360125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360125,
						"regionName": "红谷滩新区"
					}
				},
				"regionGrade": 2,
				"regionId": 360100,
				"regionName": "南昌市"
			},
			"360200": {
				"areaId": 0,
				"children": {
					"360202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360202,
						"regionName": "昌江区"
					},
					"360203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360203,
						"regionName": "珠山区"
					},
					"360222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360222,
						"regionName": "浮梁县"
					},
					"360281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360281,
						"regionName": "乐平市"
					}
				},
				"regionGrade": 2,
				"regionId": 360200,
				"regionName": "景德镇市"
			},
			"360300": {
				"areaId": 0,
				"children": {
					"360302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360302,
						"regionName": "安源区"
					},
					"360313": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360313,
						"regionName": "湘东区"
					},
					"360321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360321,
						"regionName": "莲花县"
					},
					"360322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360322,
						"regionName": "上栗县"
					},
					"360323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360323,
						"regionName": "芦溪县"
					}
				},
				"regionGrade": 2,
				"regionId": 360300,
				"regionName": "萍乡市"
			},
			"360400": {
				"areaId": 0,
				"children": {
					"360402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360402,
						"regionName": "濂溪区"
					},
					"360403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360403,
						"regionName": "浔阳区"
					},
					"360404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360404,
						"regionName": "柴桑区"
					},
					"360421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360421,
						"regionName": "九江经济技术开发区"
					},
					"360423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360423,
						"regionName": "武宁县"
					},
					"360424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360424,
						"regionName": "修水县"
					},
					"360425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360425,
						"regionName": "永修县"
					},
					"360426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360426,
						"regionName": "德安县"
					},
					"360428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360428,
						"regionName": "都昌县"
					},
					"360429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360429,
						"regionName": "湖口县"
					},
					"360430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360430,
						"regionName": "彭泽县"
					},
					"360481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360481,
						"regionName": "瑞昌市"
					},
					"360482": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360482,
						"regionName": "共青城市"
					},
					"360483": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360483,
						"regionName": "庐山市"
					}
				},
				"regionGrade": 2,
				"regionId": 360400,
				"regionName": "九江市"
			},
			"360500": {
				"areaId": 0,
				"children": {
					"360502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360502,
						"regionName": "渝水区"
					},
					"360521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360521,
						"regionName": "分宜县"
					}
				},
				"regionGrade": 2,
				"regionId": 360500,
				"regionName": "新余市"
			},
			"360600": {
				"areaId": 0,
				"children": {
					"360602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360602,
						"regionName": "月湖区"
					},
					"360622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360622,
						"regionName": "余江县"
					},
					"360681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360681,
						"regionName": "贵溪市"
					}
				},
				"regionGrade": 2,
				"regionId": 360600,
				"regionName": "鹰潭市"
			},
			"360700": {
				"areaId": 0,
				"children": {
					"360702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360702,
						"regionName": "章贡区"
					},
					"360703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360703,
						"regionName": "南康区"
					},
					"360704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360704,
						"regionName": "赣县区"
					},
					"360722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360722,
						"regionName": "信丰县"
					},
					"360723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360723,
						"regionName": "大余县"
					},
					"360724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360724,
						"regionName": "上犹县"
					},
					"360725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360725,
						"regionName": "崇义县"
					},
					"360726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360726,
						"regionName": "安远县"
					},
					"360727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360727,
						"regionName": "龙南县"
					},
					"360728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360728,
						"regionName": "定南县"
					},
					"360729": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360729,
						"regionName": "全南县"
					},
					"360730": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360730,
						"regionName": "宁都县"
					},
					"360731": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360731,
						"regionName": "于都县"
					},
					"360732": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360732,
						"regionName": "兴国县"
					},
					"360733": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360733,
						"regionName": "会昌县"
					},
					"360734": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360734,
						"regionName": "寻乌县"
					},
					"360735": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360735,
						"regionName": "石城县"
					},
					"360781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360781,
						"regionName": "瑞金市"
					}
				},
				"regionGrade": 2,
				"regionId": 360700,
				"regionName": "赣州市"
			},
			"360800": {
				"areaId": 0,
				"children": {
					"360802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360802,
						"regionName": "吉州区"
					},
					"360803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360803,
						"regionName": "青原区"
					},
					"360821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360821,
						"regionName": "吉安县"
					},
					"360822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360822,
						"regionName": "吉水县"
					},
					"360823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360823,
						"regionName": "峡江县"
					},
					"360824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360824,
						"regionName": "新干县"
					},
					"360825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360825,
						"regionName": "永丰县"
					},
					"360826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360826,
						"regionName": "泰和县"
					},
					"360827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360827,
						"regionName": "遂川县"
					},
					"360828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360828,
						"regionName": "万安县"
					},
					"360829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360829,
						"regionName": "安福县"
					},
					"360830": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360830,
						"regionName": "永新县"
					},
					"360881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360881,
						"regionName": "井冈山市"
					}
				},
				"regionGrade": 2,
				"regionId": 360800,
				"regionName": "吉安市"
			},
			"360900": {
				"areaId": 0,
				"children": {
					"360902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360902,
						"regionName": "袁州区"
					},
					"360921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360921,
						"regionName": "奉新县"
					},
					"360922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360922,
						"regionName": "万载县"
					},
					"360923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360923,
						"regionName": "上高县"
					},
					"360924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360924,
						"regionName": "宜丰县"
					},
					"360925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360925,
						"regionName": "靖安县"
					},
					"360926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360926,
						"regionName": "铜鼓县"
					},
					"360981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360981,
						"regionName": "丰城市"
					},
					"360982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360982,
						"regionName": "樟树市"
					},
					"360983": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 360983,
						"regionName": "高安市"
					}
				},
				"regionGrade": 2,
				"regionId": 360900,
				"regionName": "宜春市"
			},
			"361000": {
				"areaId": 0,
				"children": {
					"361002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361002,
						"regionName": "临川区"
					},
					"361003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361003,
						"regionName": "东乡区"
					},
					"361021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361021,
						"regionName": "南城县"
					},
					"361022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361022,
						"regionName": "黎川县"
					},
					"361023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361023,
						"regionName": "南丰县"
					},
					"361024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361024,
						"regionName": "崇仁县"
					},
					"361025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361025,
						"regionName": "乐安县"
					},
					"361026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361026,
						"regionName": "宜黄县"
					},
					"361027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361027,
						"regionName": "金溪县"
					},
					"361028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361028,
						"regionName": "资溪县"
					},
					"361030": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361030,
						"regionName": "广昌县"
					}
				},
				"regionGrade": 2,
				"regionId": 361000,
				"regionName": "抚州市"
			},
			"361100": {
				"areaId": 0,
				"children": {
					"361102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361102,
						"regionName": "信州区"
					},
					"361103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361103,
						"regionName": "广丰区"
					},
					"361121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361121,
						"regionName": "上饶县"
					},
					"361123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361123,
						"regionName": "玉山县"
					},
					"361124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361124,
						"regionName": "铅山县"
					},
					"361125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361125,
						"regionName": "横峰县"
					},
					"361126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361126,
						"regionName": "弋阳县"
					},
					"361127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361127,
						"regionName": "余干县"
					},
					"361128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361128,
						"regionName": "鄱阳县"
					},
					"361129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361129,
						"regionName": "万年县"
					},
					"361130": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361130,
						"regionName": "婺源县"
					},
					"361181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 361181,
						"regionName": "德兴市"
					}
				},
				"regionGrade": 2,
				"regionId": 361100,
				"regionName": "上饶市"
			}
		},
		"regionGrade": 1,
		"regionId": 360000,
		"regionName": "江西省"
	},
	"370000": {
		"areaId": 1,
		"children": {
			"370100": {
				"areaId": 0,
				"children": {
					"370102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370102,
						"regionName": "历下区"
					},
					"370103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370103,
						"regionName": "市中区"
					},
					"370104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370104,
						"regionName": "槐荫区"
					},
					"370105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370105,
						"regionName": "天桥区"
					},
					"370112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370112,
						"regionName": "历城区"
					},
					"370113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370113,
						"regionName": "长清区"
					},
					"370114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370114,
						"regionName": "章丘区"
					},
					"370124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370124,
						"regionName": "平阴县"
					},
					"370125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370125,
						"regionName": "济阳县"
					},
					"370126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370126,
						"regionName": "商河县"
					}
				},
				"regionGrade": 2,
				"regionId": 370100,
				"regionName": "济南市"
			},
			"370200": {
				"areaId": 0,
				"children": {
					"370202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370202,
						"regionName": "市南区"
					},
					"370203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370203,
						"regionName": "市北区"
					},
					"370211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370211,
						"regionName": "黄岛区"
					},
					"370212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370212,
						"regionName": "崂山区"
					},
					"370213": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370213,
						"regionName": "李沧区"
					},
					"370214": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370214,
						"regionName": "城阳区"
					},
					"370215": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370215,
						"regionName": "即墨区"
					},
					"370281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370281,
						"regionName": "胶州市"
					},
					"370283": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370283,
						"regionName": "平度市"
					},
					"370285": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370285,
						"regionName": "莱西市"
					}
				},
				"regionGrade": 2,
				"regionId": 370200,
				"regionName": "青岛市"
			},
			"370300": {
				"areaId": 0,
				"children": {
					"370302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370302,
						"regionName": "淄川区"
					},
					"370303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370303,
						"regionName": "张店区"
					},
					"370304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370304,
						"regionName": "博山区"
					},
					"370305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370305,
						"regionName": "临淄区"
					},
					"370306": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370306,
						"regionName": "周村区"
					},
					"370321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370321,
						"regionName": "桓台县"
					},
					"370322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370322,
						"regionName": "高青县"
					},
					"370323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370323,
						"regionName": "沂源县"
					}
				},
				"regionGrade": 2,
				"regionId": 370300,
				"regionName": "淄博市"
			},
			"370400": {
				"areaId": 0,
				"children": {
					"370402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370402,
						"regionName": "市中区"
					},
					"370403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370403,
						"regionName": "薛城区"
					},
					"370404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370404,
						"regionName": "峄城区"
					},
					"370405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370405,
						"regionName": "台儿庄区"
					},
					"370406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370406,
						"regionName": "山亭区"
					},
					"370481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370481,
						"regionName": "滕州市"
					}
				},
				"regionGrade": 2,
				"regionId": 370400,
				"regionName": "枣庄市"
			},
			"370500": {
				"areaId": 0,
				"children": {
					"370502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370502,
						"regionName": "东营区"
					},
					"370503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370503,
						"regionName": "河口区"
					},
					"370505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370505,
						"regionName": "垦利区"
					},
					"370522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370522,
						"regionName": "利津县"
					},
					"370523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370523,
						"regionName": "广饶县"
					}
				},
				"regionGrade": 2,
				"regionId": 370500,
				"regionName": "东营市"
			},
			"370600": {
				"areaId": 0,
				"children": {
					"370602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370602,
						"regionName": "芝罘区"
					},
					"370611": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370611,
						"regionName": "福山区"
					},
					"370612": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370612,
						"regionName": "牟平区"
					},
					"370613": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370613,
						"regionName": "莱山区"
					},
					"370634": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370634,
						"regionName": "长岛县"
					},
					"370681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370681,
						"regionName": "龙口市"
					},
					"370682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370682,
						"regionName": "莱阳市"
					},
					"370683": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370683,
						"regionName": "莱州市"
					},
					"370684": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370684,
						"regionName": "蓬莱市"
					},
					"370685": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370685,
						"regionName": "招远市"
					},
					"370686": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370686,
						"regionName": "栖霞市"
					},
					"370687": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370687,
						"regionName": "海阳市"
					}
				},
				"regionGrade": 2,
				"regionId": 370600,
				"regionName": "烟台市"
			},
			"370700": {
				"areaId": 0,
				"children": {
					"370702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370702,
						"regionName": "潍城区"
					},
					"370703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370703,
						"regionName": "寒亭区"
					},
					"370704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370704,
						"regionName": "坊子区"
					},
					"370705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370705,
						"regionName": "奎文区"
					},
					"370724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370724,
						"regionName": "临朐县"
					},
					"370725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370725,
						"regionName": "昌乐县"
					},
					"370781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370781,
						"regionName": "青州市"
					},
					"370782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370782,
						"regionName": "诸城市"
					},
					"370783": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370783,
						"regionName": "寿光市"
					},
					"370784": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370784,
						"regionName": "安丘市"
					},
					"370785": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370785,
						"regionName": "高密市"
					},
					"370786": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370786,
						"regionName": "昌邑市"
					}
				},
				"regionGrade": 2,
				"regionId": 370700,
				"regionName": "潍坊市"
			},
			"370800": {
				"areaId": 0,
				"children": {
					"370811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370811,
						"regionName": "任城区"
					},
					"370812": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370812,
						"regionName": "兖州区"
					},
					"370826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370826,
						"regionName": "微山县"
					},
					"370827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370827,
						"regionName": "鱼台县"
					},
					"370828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370828,
						"regionName": "金乡县"
					},
					"370829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370829,
						"regionName": "嘉祥县"
					},
					"370830": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370830,
						"regionName": "汶上县"
					},
					"370831": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370831,
						"regionName": "泗水县"
					},
					"370832": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370832,
						"regionName": "梁山县"
					},
					"370881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370881,
						"regionName": "曲阜市"
					},
					"370883": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370883,
						"regionName": "邹城市"
					},
					"370884": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370884,
						"regionName": "其它区"
					}
				},
				"regionGrade": 2,
				"regionId": 370800,
				"regionName": "济宁市"
			},
			"370900": {
				"areaId": 0,
				"children": {
					"370902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370902,
						"regionName": "泰山区"
					},
					"370911": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370911,
						"regionName": "岱岳区"
					},
					"370921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370921,
						"regionName": "宁阳县"
					},
					"370923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370923,
						"regionName": "东平县"
					},
					"370982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370982,
						"regionName": "新泰市"
					},
					"370983": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 370983,
						"regionName": "肥城市"
					}
				},
				"regionGrade": 2,
				"regionId": 370900,
				"regionName": "泰安市"
			},
			"371000": {
				"areaId": 0,
				"children": {
					"371002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371002,
						"regionName": "环翠区"
					},
					"371003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371003,
						"regionName": "文登区"
					},
					"371082": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371082,
						"regionName": "荣成市"
					},
					"371083": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371083,
						"regionName": "乳山市"
					}
				},
				"regionGrade": 2,
				"regionId": 371000,
				"regionName": "威海市"
			},
			"371100": {
				"areaId": 0,
				"children": {
					"371102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371102,
						"regionName": "东港区"
					},
					"371103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371103,
						"regionName": "岚山区"
					},
					"371121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371121,
						"regionName": "五莲县"
					},
					"371122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371122,
						"regionName": "莒县"
					}
				},
				"regionGrade": 2,
				"regionId": 371100,
				"regionName": "日照市"
			},
			"371200": {
				"areaId": 0,
				"children": {
					"371202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371202,
						"regionName": "莱城区"
					},
					"371203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371203,
						"regionName": "钢城区"
					}
				},
				"regionGrade": 2,
				"regionId": 371200,
				"regionName": "莱芜市"
			},
			"371300": {
				"areaId": 0,
				"children": {
					"371302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371302,
						"regionName": "兰山区"
					},
					"371311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371311,
						"regionName": "罗庄区"
					},
					"371312": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371312,
						"regionName": "河东区"
					},
					"371321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371321,
						"regionName": "沂南县"
					},
					"371322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371322,
						"regionName": "郯城县"
					},
					"371323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371323,
						"regionName": "沂水县"
					},
					"371324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371324,
						"regionName": "兰陵县"
					},
					"371325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371325,
						"regionName": "费县"
					},
					"371326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371326,
						"regionName": "平邑县"
					},
					"371327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371327,
						"regionName": "莒南县"
					},
					"371328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371328,
						"regionName": "蒙阴县"
					},
					"371329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371329,
						"regionName": "临沭县"
					},
					"371330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371330,
						"regionName": "苍山县"
					}
				},
				"regionGrade": 2,
				"regionId": 371300,
				"regionName": "临沂市"
			},
			"371400": {
				"areaId": 0,
				"children": {
					"371402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371402,
						"regionName": "德城区"
					},
					"371403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371403,
						"regionName": "陵城区"
					},
					"371422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371422,
						"regionName": "宁津县"
					},
					"371423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371423,
						"regionName": "庆云县"
					},
					"371424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371424,
						"regionName": "临邑县"
					},
					"371425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371425,
						"regionName": "齐河县"
					},
					"371426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371426,
						"regionName": "平原县"
					},
					"371427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371427,
						"regionName": "夏津县"
					},
					"371428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371428,
						"regionName": "武城县"
					},
					"371481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371481,
						"regionName": "乐陵市"
					},
					"371482": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371482,
						"regionName": "禹城市"
					}
				},
				"regionGrade": 2,
				"regionId": 371400,
				"regionName": "德州市"
			},
			"371500": {
				"areaId": 0,
				"children": {
					"371502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371502,
						"regionName": "东昌府区"
					},
					"371521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371521,
						"regionName": "阳谷县"
					},
					"371522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371522,
						"regionName": "莘县"
					},
					"371523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371523,
						"regionName": "茌平县"
					},
					"371524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371524,
						"regionName": "东阿县"
					},
					"371525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371525,
						"regionName": "冠县"
					},
					"371526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371526,
						"regionName": "高唐县"
					},
					"371581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371581,
						"regionName": "临清市"
					}
				},
				"regionGrade": 2,
				"regionId": 371500,
				"regionName": "聊城市"
			},
			"371600": {
				"areaId": 0,
				"children": {
					"371602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371602,
						"regionName": "滨城区"
					},
					"371603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371603,
						"regionName": "沾化区"
					},
					"371621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371621,
						"regionName": "惠民县"
					},
					"371622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371622,
						"regionName": "阳信县"
					},
					"371623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371623,
						"regionName": "无棣县"
					},
					"371625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371625,
						"regionName": "博兴县"
					},
					"371626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371626,
						"regionName": "邹平县"
					}
				},
				"regionGrade": 2,
				"regionId": 371600,
				"regionName": "滨州市"
			},
			"371700": {
				"areaId": 0,
				"children": {
					"371702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371702,
						"regionName": "牡丹区"
					},
					"371703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371703,
						"regionName": "定陶区"
					},
					"371721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371721,
						"regionName": "曹县"
					},
					"371722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371722,
						"regionName": "单县"
					},
					"371723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371723,
						"regionName": "成武县"
					},
					"371724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371724,
						"regionName": "巨野县"
					},
					"371725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371725,
						"regionName": "郓城县"
					},
					"371726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371726,
						"regionName": "鄄城县"
					},
					"371728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 371728,
						"regionName": "东明县"
					}
				},
				"regionGrade": 2,
				"regionId": 371700,
				"regionName": "菏泽市"
			}
		},
		"regionGrade": 1,
		"regionId": 370000,
		"regionName": "山东省"
	},
	"410000": {
		"areaId": 3,
		"children": {
			"410100": {
				"areaId": 0,
				"children": {
					"410102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410102,
						"regionName": "中原区"
					},
					"410103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410103,
						"regionName": "二七区"
					},
					"410104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410104,
						"regionName": "管城回族区"
					},
					"410105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410105,
						"regionName": "金水区"
					},
					"410106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410106,
						"regionName": "上街区"
					},
					"410108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410108,
						"regionName": "惠济区"
					},
					"410122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410122,
						"regionName": "中牟县"
					},
					"410181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410181,
						"regionName": "巩义市"
					},
					"410182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410182,
						"regionName": "荥阳市"
					},
					"410183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410183,
						"regionName": "新密市"
					},
					"410184": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410184,
						"regionName": "新郑市"
					},
					"410185": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410185,
						"regionName": "登封市"
					},
					"410186": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410186,
						"regionName": "高新技术开发区"
					},
					"410187": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410187,
						"regionName": "经济技术开发区"
					},
					"410188": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410188,
						"regionName": "出口加工区"
					},
					"410189": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410189,
						"regionName": "郑东新区"
					}
				},
				"regionGrade": 2,
				"regionId": 410100,
				"regionName": "郑州市"
			},
			"410200": {
				"areaId": 0,
				"children": {
					"410202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410202,
						"regionName": "龙亭区"
					},
					"410203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410203,
						"regionName": "顺河回族区"
					},
					"410204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410204,
						"regionName": "鼓楼区"
					},
					"410205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410205,
						"regionName": "禹王台区"
					},
					"410212": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410212,
						"regionName": "祥符区"
					},
					"410221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410221,
						"regionName": "杞县"
					},
					"410222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410222,
						"regionName": "通许县"
					},
					"410223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410223,
						"regionName": "尉氏县"
					},
					"410225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410225,
						"regionName": "兰考县"
					}
				},
				"regionGrade": 2,
				"regionId": 410200,
				"regionName": "开封市"
			},
			"410300": {
				"areaId": 0,
				"children": {
					"410302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410302,
						"regionName": "老城区"
					},
					"410303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410303,
						"regionName": "西工区"
					},
					"410304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410304,
						"regionName": "瀍河回族区"
					},
					"410305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410305,
						"regionName": "涧西区"
					},
					"410306": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410306,
						"regionName": "吉利区"
					},
					"410311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410311,
						"regionName": "洛龙区"
					},
					"410322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410322,
						"regionName": "孟津县"
					},
					"410323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410323,
						"regionName": "新安县"
					},
					"410324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410324,
						"regionName": "栾川县"
					},
					"410325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410325,
						"regionName": "嵩县"
					},
					"410326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410326,
						"regionName": "汝阳县"
					},
					"410327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410327,
						"regionName": "宜阳县"
					},
					"410328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410328,
						"regionName": "洛宁县"
					},
					"410329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410329,
						"regionName": "伊川县"
					},
					"410381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410381,
						"regionName": "偃师市"
					}
				},
				"regionGrade": 2,
				"regionId": 410300,
				"regionName": "洛阳市"
			},
			"410400": {
				"areaId": 0,
				"children": {
					"410402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410402,
						"regionName": "新华区"
					},
					"410403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410403,
						"regionName": "卫东区"
					},
					"410404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410404,
						"regionName": "石龙区"
					},
					"410411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410411,
						"regionName": "湛河区"
					},
					"410421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410421,
						"regionName": "宝丰县"
					},
					"410422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410422,
						"regionName": "叶县"
					},
					"410423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410423,
						"regionName": "鲁山县"
					},
					"410425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410425,
						"regionName": "郏县"
					},
					"410481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410481,
						"regionName": "舞钢市"
					},
					"410482": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410482,
						"regionName": "汝州市"
					}
				},
				"regionGrade": 2,
				"regionId": 410400,
				"regionName": "平顶山市"
			},
			"410500": {
				"areaId": 0,
				"children": {
					"410502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410502,
						"regionName": "文峰区"
					},
					"410503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410503,
						"regionName": "北关区"
					},
					"410505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410505,
						"regionName": "殷都区"
					},
					"410506": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410506,
						"regionName": "龙安区"
					},
					"410522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410522,
						"regionName": "安阳县"
					},
					"410523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410523,
						"regionName": "汤阴县"
					},
					"410526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410526,
						"regionName": "滑县"
					},
					"410527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410527,
						"regionName": "内黄县"
					},
					"410581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410581,
						"regionName": "林州市"
					}
				},
				"regionGrade": 2,
				"regionId": 410500,
				"regionName": "安阳市"
			},
			"410600": {
				"areaId": 0,
				"children": {
					"410602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410602,
						"regionName": "鹤山区"
					},
					"410603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410603,
						"regionName": "山城区"
					},
					"410611": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410611,
						"regionName": "淇滨区"
					},
					"410621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410621,
						"regionName": "浚县"
					},
					"410622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410622,
						"regionName": "淇县"
					}
				},
				"regionGrade": 2,
				"regionId": 410600,
				"regionName": "鹤壁市"
			},
			"410700": {
				"areaId": 0,
				"children": {
					"410702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410702,
						"regionName": "红旗区"
					},
					"410703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410703,
						"regionName": "卫滨区"
					},
					"410704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410704,
						"regionName": "凤泉区"
					},
					"410711": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410711,
						"regionName": "牧野区"
					},
					"410721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410721,
						"regionName": "新乡县"
					},
					"410724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410724,
						"regionName": "获嘉县"
					},
					"410725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410725,
						"regionName": "原阳县"
					},
					"410726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410726,
						"regionName": "延津县"
					},
					"410727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410727,
						"regionName": "封丘县"
					},
					"410728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410728,
						"regionName": "长垣县"
					},
					"410781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410781,
						"regionName": "卫辉市"
					},
					"410782": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410782,
						"regionName": "辉县市"
					}
				},
				"regionGrade": 2,
				"regionId": 410700,
				"regionName": "新乡市"
			},
			"410800": {
				"areaId": 0,
				"children": {
					"410802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410802,
						"regionName": "解放区"
					},
					"410803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410803,
						"regionName": "中站区"
					},
					"410804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410804,
						"regionName": "马村区"
					},
					"410811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410811,
						"regionName": "山阳区"
					},
					"410821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410821,
						"regionName": "修武县"
					},
					"410822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410822,
						"regionName": "博爱县"
					},
					"410823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410823,
						"regionName": "武陟县"
					},
					"410825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410825,
						"regionName": "温县"
					},
					"410882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410882,
						"regionName": "沁阳市"
					},
					"410883": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410883,
						"regionName": "孟州市"
					}
				},
				"regionGrade": 2,
				"regionId": 410800,
				"regionName": "焦作市"
			},
			"410900": {
				"areaId": 0,
				"children": {
					"410902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410902,
						"regionName": "华龙区"
					},
					"410922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410922,
						"regionName": "清丰县"
					},
					"410923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410923,
						"regionName": "南乐县"
					},
					"410926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410926,
						"regionName": "范县"
					},
					"410927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410927,
						"regionName": "台前县"
					},
					"410928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 410928,
						"regionName": "濮阳县"
					}
				},
				"regionGrade": 2,
				"regionId": 410900,
				"regionName": "濮阳市"
			},
			"411000": {
				"areaId": 0,
				"children": {
					"411002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411002,
						"regionName": "魏都区"
					},
					"411003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411003,
						"regionName": "建安区"
					},
					"411024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411024,
						"regionName": "鄢陵县"
					},
					"411025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411025,
						"regionName": "襄城县"
					},
					"411081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411081,
						"regionName": "禹州市"
					},
					"411082": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411082,
						"regionName": "长葛市"
					}
				},
				"regionGrade": 2,
				"regionId": 411000,
				"regionName": "许昌市"
			},
			"411100": {
				"areaId": 0,
				"children": {
					"411102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411102,
						"regionName": "源汇区"
					},
					"411103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411103,
						"regionName": "郾城区"
					},
					"411104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411104,
						"regionName": "召陵区"
					},
					"411121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411121,
						"regionName": "舞阳县"
					},
					"411122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411122,
						"regionName": "临颍县"
					}
				},
				"regionGrade": 2,
				"regionId": 411100,
				"regionName": "漯河市"
			},
			"411200": {
				"areaId": 0,
				"children": {
					"411202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411202,
						"regionName": "湖滨区"
					},
					"411203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411203,
						"regionName": "陕州区"
					},
					"411221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411221,
						"regionName": "渑池县"
					},
					"411224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411224,
						"regionName": "卢氏县"
					},
					"411281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411281,
						"regionName": "义马市"
					},
					"411282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411282,
						"regionName": "灵宝市"
					}
				},
				"regionGrade": 2,
				"regionId": 411200,
				"regionName": "三门峡市"
			},
			"411300": {
				"areaId": 0,
				"children": {
					"411302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411302,
						"regionName": "宛城区"
					},
					"411303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411303,
						"regionName": "卧龙区"
					},
					"411321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411321,
						"regionName": "南召县"
					},
					"411322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411322,
						"regionName": "方城县"
					},
					"411323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411323,
						"regionName": "西峡县"
					},
					"411324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411324,
						"regionName": "镇平县"
					},
					"411325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411325,
						"regionName": "内乡县"
					},
					"411326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411326,
						"regionName": "淅川县"
					},
					"411327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411327,
						"regionName": "社旗县"
					},
					"411328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411328,
						"regionName": "唐河县"
					},
					"411329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411329,
						"regionName": "新野县"
					},
					"411330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411330,
						"regionName": "桐柏县"
					},
					"411381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411381,
						"regionName": "邓州市"
					}
				},
				"regionGrade": 2,
				"regionId": 411300,
				"regionName": "南阳市"
			},
			"411400": {
				"areaId": 0,
				"children": {
					"411402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411402,
						"regionName": "梁园区"
					},
					"411403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411403,
						"regionName": "睢阳区"
					},
					"411421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411421,
						"regionName": "民权县"
					},
					"411422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411422,
						"regionName": "睢县"
					},
					"411423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411423,
						"regionName": "宁陵县"
					},
					"411424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411424,
						"regionName": "柘城县"
					},
					"411425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411425,
						"regionName": "虞城县"
					},
					"411426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411426,
						"regionName": "夏邑县"
					},
					"411481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411481,
						"regionName": "永城市"
					}
				},
				"regionGrade": 2,
				"regionId": 411400,
				"regionName": "商丘市"
			},
			"411500": {
				"areaId": 0,
				"children": {
					"411502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411502,
						"regionName": "浉河区"
					},
					"411503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411503,
						"regionName": "平桥区"
					},
					"411521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411521,
						"regionName": "罗山县"
					},
					"411522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411522,
						"regionName": "光山县"
					},
					"411523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411523,
						"regionName": "新县"
					},
					"411524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411524,
						"regionName": "商城县"
					},
					"411525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411525,
						"regionName": "固始县"
					},
					"411526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411526,
						"regionName": "潢川县"
					},
					"411527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411527,
						"regionName": "淮滨县"
					},
					"411528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411528,
						"regionName": "息县"
					}
				},
				"regionGrade": 2,
				"regionId": 411500,
				"regionName": "信阳市"
			},
			"411600": {
				"areaId": 0,
				"children": {
					"411602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411602,
						"regionName": "川汇区"
					},
					"411621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411621,
						"regionName": "扶沟县"
					},
					"411622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411622,
						"regionName": "西华县"
					},
					"411623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411623,
						"regionName": "商水县"
					},
					"411624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411624,
						"regionName": "沈丘县"
					},
					"411625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411625,
						"regionName": "郸城县"
					},
					"411626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411626,
						"regionName": "淮阳县"
					},
					"411627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411627,
						"regionName": "太康县"
					},
					"411628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411628,
						"regionName": "鹿邑县"
					},
					"411681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411681,
						"regionName": "项城市"
					}
				},
				"regionGrade": 2,
				"regionId": 411600,
				"regionName": "周口市"
			},
			"411700": {
				"areaId": 0,
				"children": {
					"411702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411702,
						"regionName": "驿城区"
					},
					"411721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411721,
						"regionName": "西平县"
					},
					"411722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411722,
						"regionName": "上蔡县"
					},
					"411723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411723,
						"regionName": "平舆县"
					},
					"411724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411724,
						"regionName": "正阳县"
					},
					"411725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411725,
						"regionName": "确山县"
					},
					"411726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411726,
						"regionName": "泌阳县"
					},
					"411727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411727,
						"regionName": "汝南县"
					},
					"411728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411728,
						"regionName": "遂平县"
					},
					"411729": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 411729,
						"regionName": "新蔡县"
					}
				},
				"regionGrade": 2,
				"regionId": 411700,
				"regionName": "驻马店市"
			},
			"419000": {
				"areaId": 0,
				"children": {
					"419001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 419001,
						"regionName": "济源市"
					}
				},
				"regionGrade": 2,
				"regionId": 419000,
				"regionName": "济源市"
			}
		},
		"regionGrade": 1,
		"regionId": 410000,
		"regionName": "河南省"
	},
	"420000": {
		"areaId": 3,
		"children": {
			"420100": {
				"areaId": 0,
				"children": {
					"420102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420102,
						"regionName": "江岸区"
					},
					"420103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420103,
						"regionName": "江汉区"
					},
					"420104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420104,
						"regionName": "硚口区"
					},
					"420105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420105,
						"regionName": "汉阳区"
					},
					"420106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420106,
						"regionName": "武昌区"
					},
					"420107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420107,
						"regionName": "青山区"
					},
					"420111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420111,
						"regionName": "洪山区"
					},
					"420112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420112,
						"regionName": "东西湖区"
					},
					"420113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420113,
						"regionName": "汉南区"
					},
					"420114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420114,
						"regionName": "蔡甸区"
					},
					"420115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420115,
						"regionName": "江夏区"
					},
					"420116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420116,
						"regionName": "黄陂区"
					},
					"420117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420117,
						"regionName": "新洲区"
					}
				},
				"regionGrade": 2,
				"regionId": 420100,
				"regionName": "武汉市"
			},
			"420200": {
				"areaId": 0,
				"children": {
					"420202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420202,
						"regionName": "黄石港区"
					},
					"420203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420203,
						"regionName": "西塞山区"
					},
					"420204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420204,
						"regionName": "下陆区"
					},
					"420205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420205,
						"regionName": "铁山区"
					},
					"420222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420222,
						"regionName": "阳新县"
					},
					"420281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420281,
						"regionName": "大冶市"
					}
				},
				"regionGrade": 2,
				"regionId": 420200,
				"regionName": "黄石市"
			},
			"420300": {
				"areaId": 0,
				"children": {
					"420302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420302,
						"regionName": "茅箭区"
					},
					"420303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420303,
						"regionName": "张湾区"
					},
					"420304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420304,
						"regionName": "郧阳区"
					},
					"420322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420322,
						"regionName": "郧西县"
					},
					"420323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420323,
						"regionName": "竹山县"
					},
					"420324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420324,
						"regionName": "竹溪县"
					},
					"420325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420325,
						"regionName": "房县"
					},
					"420381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420381,
						"regionName": "丹江口市"
					}
				},
				"regionGrade": 2,
				"regionId": 420300,
				"regionName": "十堰市"
			},
			"420500": {
				"areaId": 0,
				"children": {
					"420502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420502,
						"regionName": "西陵区"
					},
					"420503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420503,
						"regionName": "伍家岗区"
					},
					"420504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420504,
						"regionName": "点军区"
					},
					"420505": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420505,
						"regionName": "猇亭区"
					},
					"420506": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420506,
						"regionName": "夷陵区"
					},
					"420525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420525,
						"regionName": "远安县"
					},
					"420526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420526,
						"regionName": "兴山县"
					},
					"420527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420527,
						"regionName": "秭归县"
					},
					"420528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420528,
						"regionName": "长阳土家族自治县"
					},
					"420529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420529,
						"regionName": "五峰土家族自治县"
					},
					"420581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420581,
						"regionName": "宜都市"
					},
					"420582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420582,
						"regionName": "当阳市"
					},
					"420583": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420583,
						"regionName": "枝江市"
					}
				},
				"regionGrade": 2,
				"regionId": 420500,
				"regionName": "宜昌市"
			},
			"420600": {
				"areaId": 0,
				"children": {
					"420602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420602,
						"regionName": "襄城区"
					},
					"420606": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420606,
						"regionName": "樊城区"
					},
					"420607": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420607,
						"regionName": "襄州区"
					},
					"420624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420624,
						"regionName": "南漳县"
					},
					"420625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420625,
						"regionName": "谷城县"
					},
					"420626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420626,
						"regionName": "保康县"
					},
					"420682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420682,
						"regionName": "老河口市"
					},
					"420683": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420683,
						"regionName": "枣阳市"
					},
					"420684": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420684,
						"regionName": "宜城市"
					}
				},
				"regionGrade": 2,
				"regionId": 420600,
				"regionName": "襄阳市"
			},
			"420700": {
				"areaId": 0,
				"children": {
					"420702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420702,
						"regionName": "梁子湖区"
					},
					"420703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420703,
						"regionName": "华容区"
					},
					"420704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420704,
						"regionName": "鄂城区"
					}
				},
				"regionGrade": 2,
				"regionId": 420700,
				"regionName": "鄂州市"
			},
			"420800": {
				"areaId": 0,
				"children": {
					"420802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420802,
						"regionName": "东宝区"
					},
					"420804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420804,
						"regionName": "掇刀区"
					},
					"420821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420821,
						"regionName": "京山县"
					},
					"420822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420822,
						"regionName": "沙洋县"
					},
					"420881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420881,
						"regionName": "钟祥市"
					}
				},
				"regionGrade": 2,
				"regionId": 420800,
				"regionName": "荆门市"
			},
			"420900": {
				"areaId": 0,
				"children": {
					"420902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420902,
						"regionName": "孝南区"
					},
					"420921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420921,
						"regionName": "孝昌县"
					},
					"420922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420922,
						"regionName": "大悟县"
					},
					"420923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420923,
						"regionName": "云梦县"
					},
					"420981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420981,
						"regionName": "应城市"
					},
					"420982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420982,
						"regionName": "安陆市"
					},
					"420984": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 420984,
						"regionName": "汉川市"
					}
				},
				"regionGrade": 2,
				"regionId": 420900,
				"regionName": "孝感市"
			},
			"421000": {
				"areaId": 0,
				"children": {
					"421002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421002,
						"regionName": "沙市区"
					},
					"421003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421003,
						"regionName": "荆州区"
					},
					"421022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421022,
						"regionName": "公安县"
					},
					"421023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421023,
						"regionName": "监利县"
					},
					"421024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421024,
						"regionName": "江陵县"
					},
					"421081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421081,
						"regionName": "石首市"
					},
					"421083": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421083,
						"regionName": "洪湖市"
					},
					"421087": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421087,
						"regionName": "松滋市"
					}
				},
				"regionGrade": 2,
				"regionId": 421000,
				"regionName": "荆州市"
			},
			"421100": {
				"areaId": 0,
				"children": {
					"421102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421102,
						"regionName": "黄州区"
					},
					"421121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421121,
						"regionName": "团风县"
					},
					"421122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421122,
						"regionName": "红安县"
					},
					"421123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421123,
						"regionName": "罗田县"
					},
					"421124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421124,
						"regionName": "英山县"
					},
					"421125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421125,
						"regionName": "浠水县"
					},
					"421126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421126,
						"regionName": "蕲春县"
					},
					"421127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421127,
						"regionName": "黄梅县"
					},
					"421181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421181,
						"regionName": "麻城市"
					},
					"421182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421182,
						"regionName": "武穴市"
					}
				},
				"regionGrade": 2,
				"regionId": 421100,
				"regionName": "黄冈市"
			},
			"421200": {
				"areaId": 0,
				"children": {
					"421202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421202,
						"regionName": "咸安区"
					},
					"421221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421221,
						"regionName": "嘉鱼县"
					},
					"421222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421222,
						"regionName": "通城县"
					},
					"421223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421223,
						"regionName": "崇阳县"
					},
					"421224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421224,
						"regionName": "通山县"
					},
					"421281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421281,
						"regionName": "赤壁市"
					}
				},
				"regionGrade": 2,
				"regionId": 421200,
				"regionName": "咸宁市"
			},
			"421300": {
				"areaId": 0,
				"children": {
					"421303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421303,
						"regionName": "曾都区"
					},
					"421321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421321,
						"regionName": "随县"
					},
					"421381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 421381,
						"regionName": "广水市"
					}
				},
				"regionGrade": 2,
				"regionId": 421300,
				"regionName": "随州市"
			},
			"422800": {
				"areaId": 0,
				"children": {
					"422801": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422801,
						"regionName": "恩施市"
					},
					"422802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422802,
						"regionName": "利川市"
					},
					"422822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422822,
						"regionName": "建始县"
					},
					"422823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422823,
						"regionName": "巴东县"
					},
					"422825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422825,
						"regionName": "宣恩县"
					},
					"422826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422826,
						"regionName": "咸丰县"
					},
					"422827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422827,
						"regionName": "来凤县"
					},
					"422828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 422828,
						"regionName": "鹤峰县"
					}
				},
				"regionGrade": 2,
				"regionId": 422800,
				"regionName": "恩施土家族苗族自治州"
			},
			"469000": {
				"areaId": 0,
				"children": {
					"469004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 469004,
						"regionName": "仙桃市"
					},
					"469005": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 469005,
						"regionName": "潜江市"
					},
					"469006": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 469006,
						"regionName": "天门市"
					},
					"469021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 469021,
						"regionName": "神农架林区"
					}
				},
				"regionGrade": 2,
				"regionId": 469000,
				"regionName": "省直辖县级行政单位"
			}
		},
		"regionGrade": 1,
		"regionId": 420000,
		"regionName": "湖北省"
	},
	"430000": {
		"areaId": 3,
		"children": {
			"430100": {
				"areaId": 0,
				"children": {
					"430102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430102,
						"regionName": "芙蓉区"
					},
					"430103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430103,
						"regionName": "天心区"
					},
					"430104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430104,
						"regionName": "岳麓区"
					},
					"430105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430105,
						"regionName": "开福区"
					},
					"430111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430111,
						"regionName": "雨花区"
					},
					"430112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430112,
						"regionName": "望城区"
					},
					"430121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430121,
						"regionName": "长沙县"
					},
					"430181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430181,
						"regionName": "浏阳市"
					},
					"430182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430182,
						"regionName": "宁乡市"
					}
				},
				"regionGrade": 2,
				"regionId": 430100,
				"regionName": "长沙市"
			},
			"430200": {
				"areaId": 0,
				"children": {
					"430202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430202,
						"regionName": "荷塘区"
					},
					"430203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430203,
						"regionName": "芦淞区"
					},
					"430204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430204,
						"regionName": "石峰区"
					},
					"430211": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430211,
						"regionName": "天元区"
					},
					"430221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430221,
						"regionName": "株洲县"
					},
					"430223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430223,
						"regionName": "攸县"
					},
					"430224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430224,
						"regionName": "茶陵县"
					},
					"430225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430225,
						"regionName": "炎陵县"
					},
					"430281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430281,
						"regionName": "醴陵市"
					}
				},
				"regionGrade": 2,
				"regionId": 430200,
				"regionName": "株洲市"
			},
			"430300": {
				"areaId": 0,
				"children": {
					"430302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430302,
						"regionName": "雨湖区"
					},
					"430304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430304,
						"regionName": "岳塘区"
					},
					"430321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430321,
						"regionName": "湘潭县"
					},
					"430381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430381,
						"regionName": "湘乡市"
					},
					"430382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430382,
						"regionName": "韶山市"
					}
				},
				"regionGrade": 2,
				"regionId": 430300,
				"regionName": "湘潭市"
			},
			"430400": {
				"areaId": 0,
				"children": {
					"430405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430405,
						"regionName": "珠晖区"
					},
					"430406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430406,
						"regionName": "雁峰区"
					},
					"430407": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430407,
						"regionName": "石鼓区"
					},
					"430408": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430408,
						"regionName": "蒸湘区"
					},
					"430412": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430412,
						"regionName": "南岳区"
					},
					"430421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430421,
						"regionName": "衡阳县"
					},
					"430422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430422,
						"regionName": "衡南县"
					},
					"430423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430423,
						"regionName": "衡山县"
					},
					"430424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430424,
						"regionName": "衡东县"
					},
					"430426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430426,
						"regionName": "祁东县"
					},
					"430481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430481,
						"regionName": "耒阳市"
					},
					"430482": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430482,
						"regionName": "常宁市"
					}
				},
				"regionGrade": 2,
				"regionId": 430400,
				"regionName": "衡阳市"
			},
			"430500": {
				"areaId": 0,
				"children": {
					"430502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430502,
						"regionName": "双清区"
					},
					"430503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430503,
						"regionName": "大祥区"
					},
					"430511": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430511,
						"regionName": "北塔区"
					},
					"430521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430521,
						"regionName": "邵东县"
					},
					"430522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430522,
						"regionName": "新邵县"
					},
					"430523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430523,
						"regionName": "邵阳县"
					},
					"430524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430524,
						"regionName": "隆回县"
					},
					"430525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430525,
						"regionName": "洞口县"
					},
					"430527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430527,
						"regionName": "绥宁县"
					},
					"430528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430528,
						"regionName": "新宁县"
					},
					"430529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430529,
						"regionName": "城步苗族自治县"
					},
					"430581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430581,
						"regionName": "武冈市"
					}
				},
				"regionGrade": 2,
				"regionId": 430500,
				"regionName": "邵阳市"
			},
			"430600": {
				"areaId": 0,
				"children": {
					"430602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430602,
						"regionName": "岳阳楼区"
					},
					"430603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430603,
						"regionName": "云溪区"
					},
					"430611": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430611,
						"regionName": "君山区"
					},
					"430621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430621,
						"regionName": "岳阳县"
					},
					"430623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430623,
						"regionName": "华容县"
					},
					"430624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430624,
						"regionName": "湘阴县"
					},
					"430626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430626,
						"regionName": "平江县"
					},
					"430681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430681,
						"regionName": "汨罗市"
					},
					"430682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430682,
						"regionName": "临湘市"
					}
				},
				"regionGrade": 2,
				"regionId": 430600,
				"regionName": "岳阳市"
			},
			"430700": {
				"areaId": 0,
				"children": {
					"430702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430702,
						"regionName": "武陵区"
					},
					"430703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430703,
						"regionName": "鼎城区"
					},
					"430721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430721,
						"regionName": "安乡县"
					},
					"430722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430722,
						"regionName": "汉寿县"
					},
					"430723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430723,
						"regionName": "澧县"
					},
					"430724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430724,
						"regionName": "临澧县"
					},
					"430725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430725,
						"regionName": "桃源县"
					},
					"430726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430726,
						"regionName": "石门县"
					},
					"430781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430781,
						"regionName": "津市市"
					}
				},
				"regionGrade": 2,
				"regionId": 430700,
				"regionName": "常德市"
			},
			"430800": {
				"areaId": 0,
				"children": {
					"430802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430802,
						"regionName": "永定区"
					},
					"430811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430811,
						"regionName": "武陵源区"
					},
					"430821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430821,
						"regionName": "慈利县"
					},
					"430822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430822,
						"regionName": "桑植县"
					}
				},
				"regionGrade": 2,
				"regionId": 430800,
				"regionName": "张家界市"
			},
			"430900": {
				"areaId": 0,
				"children": {
					"430902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430902,
						"regionName": "资阳区"
					},
					"430903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430903,
						"regionName": "赫山区"
					},
					"430921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430921,
						"regionName": "南县"
					},
					"430922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430922,
						"regionName": "桃江县"
					},
					"430923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430923,
						"regionName": "安化县"
					},
					"430981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 430981,
						"regionName": "沅江市"
					}
				},
				"regionGrade": 2,
				"regionId": 430900,
				"regionName": "益阳市"
			},
			"431000": {
				"areaId": 0,
				"children": {
					"431002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431002,
						"regionName": "北湖区"
					},
					"431003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431003,
						"regionName": "苏仙区"
					},
					"431021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431021,
						"regionName": "桂阳县"
					},
					"431022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431022,
						"regionName": "宜章县"
					},
					"431023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431023,
						"regionName": "永兴县"
					},
					"431024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431024,
						"regionName": "嘉禾县"
					},
					"431025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431025,
						"regionName": "临武县"
					},
					"431026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431026,
						"regionName": "汝城县"
					},
					"431027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431027,
						"regionName": "桂东县"
					},
					"431028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431028,
						"regionName": "安仁县"
					},
					"431081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431081,
						"regionName": "资兴市"
					}
				},
				"regionGrade": 2,
				"regionId": 431000,
				"regionName": "郴州市"
			},
			"431100": {
				"areaId": 0,
				"children": {
					"431102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431102,
						"regionName": "零陵区"
					},
					"431103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431103,
						"regionName": "冷水滩区"
					},
					"431121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431121,
						"regionName": "祁阳县"
					},
					"431122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431122,
						"regionName": "东安县"
					},
					"431123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431123,
						"regionName": "双牌县"
					},
					"431124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431124,
						"regionName": "道县"
					},
					"431125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431125,
						"regionName": "江永县"
					},
					"431126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431126,
						"regionName": "宁远县"
					},
					"431127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431127,
						"regionName": "蓝山县"
					},
					"431128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431128,
						"regionName": "新田县"
					},
					"431129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431129,
						"regionName": "江华瑶族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 431100,
				"regionName": "永州市"
			},
			"431200": {
				"areaId": 0,
				"children": {
					"431202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431202,
						"regionName": "鹤城区"
					},
					"431221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431221,
						"regionName": "中方县"
					},
					"431222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431222,
						"regionName": "沅陵县"
					},
					"431223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431223,
						"regionName": "辰溪县"
					},
					"431224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431224,
						"regionName": "溆浦县"
					},
					"431225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431225,
						"regionName": "会同县"
					},
					"431226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431226,
						"regionName": "麻阳苗族自治县"
					},
					"431227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431227,
						"regionName": "新晃侗族自治县"
					},
					"431228": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431228,
						"regionName": "芷江侗族自治县"
					},
					"431229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431229,
						"regionName": "靖州苗族侗族自治县"
					},
					"431230": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431230,
						"regionName": "通道侗族自治县"
					},
					"431281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431281,
						"regionName": "洪江市"
					}
				},
				"regionGrade": 2,
				"regionId": 431200,
				"regionName": "怀化市"
			},
			"431300": {
				"areaId": 0,
				"children": {
					"431302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431302,
						"regionName": "娄星区"
					},
					"431321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431321,
						"regionName": "双峰县"
					},
					"431322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431322,
						"regionName": "新化县"
					},
					"431381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431381,
						"regionName": "冷水江市"
					},
					"431382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 431382,
						"regionName": "涟源市"
					}
				},
				"regionGrade": 2,
				"regionId": 431300,
				"regionName": "娄底市"
			},
			"433100": {
				"areaId": 0,
				"children": {
					"433101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433101,
						"regionName": "吉首市"
					},
					"433122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433122,
						"regionName": "泸溪县"
					},
					"433123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433123,
						"regionName": "凤凰县"
					},
					"433124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433124,
						"regionName": "花垣县"
					},
					"433125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433125,
						"regionName": "保靖县"
					},
					"433126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433126,
						"regionName": "古丈县"
					},
					"433127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433127,
						"regionName": "永顺县"
					},
					"433130": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 433130,
						"regionName": "龙山县"
					}
				},
				"regionGrade": 2,
				"regionId": 433100,
				"regionName": "湘西土家族苗族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 430000,
		"regionName": "湖南省"
	},
	"440000": {
		"areaId": 4,
		"children": {
			"440100": {
				"areaId": 0,
				"children": {
					"440103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440103,
						"regionName": "荔湾区"
					},
					"440104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440104,
						"regionName": "越秀区"
					},
					"440105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440105,
						"regionName": "海珠区"
					},
					"440106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440106,
						"regionName": "天河区"
					},
					"440111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440111,
						"regionName": "白云区"
					},
					"440112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440112,
						"regionName": "黄埔区"
					},
					"440113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440113,
						"regionName": "番禺区"
					},
					"440114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440114,
						"regionName": "花都区"
					},
					"440115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440115,
						"regionName": "南沙区"
					},
					"440117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440117,
						"regionName": "从化区"
					},
					"440118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440118,
						"regionName": "增城区"
					}
				},
				"regionGrade": 2,
				"regionId": 440100,
				"regionName": "广州市"
			},
			"440200": {
				"areaId": 0,
				"children": {
					"440203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440203,
						"regionName": "武江区"
					},
					"440204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440204,
						"regionName": "浈江区"
					},
					"440205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440205,
						"regionName": "曲江区"
					},
					"440222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440222,
						"regionName": "始兴县"
					},
					"440224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440224,
						"regionName": "仁化县"
					},
					"440229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440229,
						"regionName": "翁源县"
					},
					"440232": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440232,
						"regionName": "乳源瑶族自治县"
					},
					"440233": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440233,
						"regionName": "新丰县"
					},
					"440281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440281,
						"regionName": "乐昌市"
					},
					"440282": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440282,
						"regionName": "南雄市"
					}
				},
				"regionGrade": 2,
				"regionId": 440200,
				"regionName": "韶关市"
			},
			"440300": {
				"areaId": 0,
				"children": {
					"440303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440303,
						"regionName": "罗湖区"
					},
					"440304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440304,
						"regionName": "福田区"
					},
					"440305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440305,
						"regionName": "南山区"
					},
					"440306": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440306,
						"regionName": "宝安区"
					},
					"440307": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440307,
						"regionName": "龙岗区"
					},
					"440308": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440308,
						"regionName": "盐田区"
					},
					"440309": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440309,
						"regionName": "龙华区"
					},
					"440310": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440310,
						"regionName": "坪山区"
					}
				},
				"regionGrade": 2,
				"regionId": 440300,
				"regionName": "深圳市"
			},
			"440400": {
				"areaId": 0,
				"children": {
					"440402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440402,
						"regionName": "香洲区"
					},
					"440403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440403,
						"regionName": "斗门区"
					},
					"440404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440404,
						"regionName": "金湾区"
					}
				},
				"regionGrade": 2,
				"regionId": 440400,
				"regionName": "珠海市"
			},
			"440500": {
				"areaId": 0,
				"children": {
					"440507": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440507,
						"regionName": "龙湖区"
					},
					"440511": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440511,
						"regionName": "金平区"
					},
					"440512": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440512,
						"regionName": "濠江区"
					},
					"440513": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440513,
						"regionName": "潮阳区"
					},
					"440514": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440514,
						"regionName": "潮南区"
					},
					"440515": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440515,
						"regionName": "澄海区"
					},
					"440523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440523,
						"regionName": "南澳县"
					}
				},
				"regionGrade": 2,
				"regionId": 440500,
				"regionName": "汕头市"
			},
			"440600": {
				"areaId": 0,
				"children": {
					"440604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440604,
						"regionName": "禅城区"
					},
					"440605": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440605,
						"regionName": "南海区"
					},
					"440606": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440606,
						"regionName": "顺德区"
					},
					"440607": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440607,
						"regionName": "三水区"
					},
					"440608": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440608,
						"regionName": "高明区"
					}
				},
				"regionGrade": 2,
				"regionId": 440600,
				"regionName": "佛山市"
			},
			"440700": {
				"areaId": 0,
				"children": {
					"440703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440703,
						"regionName": "蓬江区"
					},
					"440704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440704,
						"regionName": "江海区"
					},
					"440705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440705,
						"regionName": "新会区"
					},
					"440781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440781,
						"regionName": "台山市"
					},
					"440783": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440783,
						"regionName": "开平市"
					},
					"440784": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440784,
						"regionName": "鹤山市"
					},
					"440785": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440785,
						"regionName": "恩平市"
					}
				},
				"regionGrade": 2,
				"regionId": 440700,
				"regionName": "江门市"
			},
			"440800": {
				"areaId": 0,
				"children": {
					"440802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440802,
						"regionName": "赤坎区"
					},
					"440803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440803,
						"regionName": "霞山区"
					},
					"440804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440804,
						"regionName": "坡头区"
					},
					"440811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440811,
						"regionName": "麻章区"
					},
					"440823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440823,
						"regionName": "遂溪县"
					},
					"440825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440825,
						"regionName": "徐闻县"
					},
					"440881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440881,
						"regionName": "廉江市"
					},
					"440882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440882,
						"regionName": "雷州市"
					},
					"440883": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440883,
						"regionName": "吴川市"
					}
				},
				"regionGrade": 2,
				"regionId": 440800,
				"regionName": "湛江市"
			},
			"440900": {
				"areaId": 0,
				"children": {
					"440902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440902,
						"regionName": "茂南区"
					},
					"440904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440904,
						"regionName": "电白区"
					},
					"440981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440981,
						"regionName": "高州市"
					},
					"440982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440982,
						"regionName": "化州市"
					},
					"440983": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 440983,
						"regionName": "信宜市"
					}
				},
				"regionGrade": 2,
				"regionId": 440900,
				"regionName": "茂名市"
			},
			"441200": {
				"areaId": 0,
				"children": {
					"441202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441202,
						"regionName": "端州区"
					},
					"441203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441203,
						"regionName": "鼎湖区"
					},
					"441204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441204,
						"regionName": "高要区"
					},
					"441223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441223,
						"regionName": "广宁县"
					},
					"441224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441224,
						"regionName": "怀集县"
					},
					"441225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441225,
						"regionName": "封开县"
					},
					"441226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441226,
						"regionName": "德庆县"
					},
					"441284": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441284,
						"regionName": "四会市"
					}
				},
				"regionGrade": 2,
				"regionId": 441200,
				"regionName": "肇庆市"
			},
			"441300": {
				"areaId": 0,
				"children": {
					"441302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441302,
						"regionName": "惠城区"
					},
					"441303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441303,
						"regionName": "惠阳区"
					},
					"441322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441322,
						"regionName": "博罗县"
					},
					"441323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441323,
						"regionName": "惠东县"
					},
					"441324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441324,
						"regionName": "龙门县"
					}
				},
				"regionGrade": 2,
				"regionId": 441300,
				"regionName": "惠州市"
			},
			"441400": {
				"areaId": 0,
				"children": {
					"441402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441402,
						"regionName": "梅江区"
					},
					"441403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441403,
						"regionName": "梅县区"
					},
					"441422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441422,
						"regionName": "大埔县"
					},
					"441423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441423,
						"regionName": "丰顺县"
					},
					"441424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441424,
						"regionName": "五华县"
					},
					"441426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441426,
						"regionName": "平远县"
					},
					"441427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441427,
						"regionName": "蕉岭县"
					},
					"441481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441481,
						"regionName": "兴宁市"
					}
				},
				"regionGrade": 2,
				"regionId": 441400,
				"regionName": "梅州市"
			},
			"441500": {
				"areaId": 0,
				"children": {
					"441502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441502,
						"regionName": "城区"
					},
					"441521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441521,
						"regionName": "海丰县"
					},
					"441523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441523,
						"regionName": "陆河县"
					},
					"441581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441581,
						"regionName": "陆丰市"
					}
				},
				"regionGrade": 2,
				"regionId": 441500,
				"regionName": "汕尾市"
			},
			"441600": {
				"areaId": 0,
				"children": {
					"441602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441602,
						"regionName": "源城区"
					},
					"441621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441621,
						"regionName": "紫金县"
					},
					"441622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441622,
						"regionName": "龙川县"
					},
					"441623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441623,
						"regionName": "连平县"
					},
					"441624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441624,
						"regionName": "和平县"
					},
					"441625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441625,
						"regionName": "东源县"
					}
				},
				"regionGrade": 2,
				"regionId": 441600,
				"regionName": "河源市"
			},
			"441700": {
				"areaId": 0,
				"children": {
					"441702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441702,
						"regionName": "江城区"
					},
					"441704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441704,
						"regionName": "阳东区"
					},
					"441721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441721,
						"regionName": "阳西县"
					},
					"441781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441781,
						"regionName": "阳春市"
					}
				},
				"regionGrade": 2,
				"regionId": 441700,
				"regionName": "阳江市"
			},
			"441800": {
				"areaId": 0,
				"children": {
					"441802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441802,
						"regionName": "清城区"
					},
					"441803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441803,
						"regionName": "清新区"
					},
					"441821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441821,
						"regionName": "佛冈县"
					},
					"441823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441823,
						"regionName": "阳山县"
					},
					"441825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441825,
						"regionName": "连山壮族瑶族自治县"
					},
					"441826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441826,
						"regionName": "连南瑶族自治县"
					},
					"441881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441881,
						"regionName": "英德市"
					},
					"441882": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441882,
						"regionName": "连州市"
					}
				},
				"regionGrade": 2,
				"regionId": 441800,
				"regionName": "清远市"
			},
			"441900": {
				"areaId": 0,
				"children": {
					"441901": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441901,
						"regionName": "南城区"
					},
					"441902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441902,
						"regionName": "石龙镇"
					},
					"441903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441903,
						"regionName": "长安镇"
					},
					"441904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441904,
						"regionName": "虎门镇"
					},
					"441905": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441905,
						"regionName": "横沥镇"
					},
					"441906": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441906,
						"regionName": "寮步镇"
					},
					"441907": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441907,
						"regionName": "黄江镇"
					},
					"441908": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441908,
						"regionName": "清溪镇"
					},
					"441909": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441909,
						"regionName": "莞城区"
					},
					"441910": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441910,
						"regionName": "万江区"
					},
					"441911": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441911,
						"regionName": "东城区"
					},
					"441912": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441912,
						"regionName": "石碣镇"
					},
					"441913": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441913,
						"regionName": "茶山镇"
					},
					"441914": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441914,
						"regionName": "石排镇"
					},
					"441915": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441915,
						"regionName": "企石镇"
					},
					"441916": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441916,
						"regionName": "桥头镇"
					},
					"441917": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441917,
						"regionName": "谢岗镇"
					},
					"441918": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441918,
						"regionName": "东坑镇"
					},
					"441919": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441919,
						"regionName": "常平镇"
					},
					"441920": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441920,
						"regionName": "大朗镇"
					},
					"441921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441921,
						"regionName": "塘厦镇"
					},
					"441922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441922,
						"regionName": "凤岗镇"
					},
					"441923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441923,
						"regionName": "厚街镇"
					},
					"441924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441924,
						"regionName": "沙田镇"
					},
					"441925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441925,
						"regionName": "道滘镇"
					},
					"441926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441926,
						"regionName": "洪梅镇"
					},
					"441927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441927,
						"regionName": "麻涌镇"
					},
					"441928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441928,
						"regionName": "中堂镇"
					},
					"441929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441929,
						"regionName": "高埗镇"
					},
					"441930": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441930,
						"regionName": "樟木头镇"
					},
					"441931": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441931,
						"regionName": "大岭山镇"
					},
					"441932": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441932,
						"regionName": "望牛墩镇"
					},
					"441933": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441933,
						"regionName": "科技产业园区"
					},
					"441934": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441934,
						"regionName": "莞城街道"
					},
					"441935": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441935,
						"regionName": "南城街道"
					},
					"441936": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441936,
						"regionName": "东城街道"
					},
					"441937": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 441937,
						"regionName": "万江街道"
					}
				},
				"regionGrade": 2,
				"regionId": 441900,
				"regionName": "东莞市"
			},
			"442000": {
				"areaId": 0,
				"children": {
					"442001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 442001,
						"regionName": "中山市"
					},
					"442002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 442002,
						"regionName": "东区"
					},
					"442003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 442003,
						"regionName": "西区"
					}
				},
				"regionGrade": 2,
				"regionId": 442000,
				"regionName": "中山市"
			},
			"445100": {
				"areaId": 0,
				"children": {
					"445102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445102,
						"regionName": "湘桥区"
					},
					"445103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445103,
						"regionName": "潮安区"
					},
					"445122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445122,
						"regionName": "饶平县"
					}
				},
				"regionGrade": 2,
				"regionId": 445100,
				"regionName": "潮州市"
			},
			"445200": {
				"areaId": 0,
				"children": {
					"445202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445202,
						"regionName": "榕城区"
					},
					"445203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445203,
						"regionName": "揭东区"
					},
					"445222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445222,
						"regionName": "揭西县"
					},
					"445224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445224,
						"regionName": "惠来县"
					},
					"445281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445281,
						"regionName": "普宁市"
					}
				},
				"regionGrade": 2,
				"regionId": 445200,
				"regionName": "揭阳市"
			},
			"445300": {
				"areaId": 0,
				"children": {
					"445302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445302,
						"regionName": "云城区"
					},
					"445303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445303,
						"regionName": "云安区"
					},
					"445321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445321,
						"regionName": "新兴县"
					},
					"445322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445322,
						"regionName": "郁南县"
					},
					"445381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 445381,
						"regionName": "罗定市"
					}
				},
				"regionGrade": 2,
				"regionId": 445300,
				"regionName": "云浮市"
			}
		},
		"regionGrade": 1,
		"regionId": 440000,
		"regionName": "广东省"
	},
	"450000": {
		"areaId": 4,
		"children": {
			"450100": {
				"areaId": 0,
				"children": {
					"450102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450102,
						"regionName": "兴宁区"
					},
					"450103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450103,
						"regionName": "青秀区"
					},
					"450105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450105,
						"regionName": "江南区"
					},
					"450107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450107,
						"regionName": "西乡塘区"
					},
					"450108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450108,
						"regionName": "良庆区"
					},
					"450109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450109,
						"regionName": "邕宁区"
					},
					"450110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450110,
						"regionName": "武鸣区"
					},
					"450123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450123,
						"regionName": "隆安县"
					},
					"450124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450124,
						"regionName": "马山县"
					},
					"450125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450125,
						"regionName": "上林县"
					},
					"450126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450126,
						"regionName": "宾阳县"
					},
					"450127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450127,
						"regionName": "横县"
					}
				},
				"regionGrade": 2,
				"regionId": 450100,
				"regionName": "南宁市"
			},
			"450200": {
				"areaId": 0,
				"children": {
					"450202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450202,
						"regionName": "城中区"
					},
					"450203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450203,
						"regionName": "鱼峰区"
					},
					"450204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450204,
						"regionName": "柳南区"
					},
					"450205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450205,
						"regionName": "柳北区"
					},
					"450206": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450206,
						"regionName": "柳江区"
					},
					"450222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450222,
						"regionName": "柳城县"
					},
					"450223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450223,
						"regionName": "鹿寨县"
					},
					"450224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450224,
						"regionName": "融安县"
					},
					"450225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450225,
						"regionName": "融水苗族自治县"
					},
					"450226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450226,
						"regionName": "三江侗族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 450200,
				"regionName": "柳州市"
			},
			"450300": {
				"areaId": 0,
				"children": {
					"450302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450302,
						"regionName": "秀峰区"
					},
					"450303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450303,
						"regionName": "叠彩区"
					},
					"450304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450304,
						"regionName": "象山区"
					},
					"450305": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450305,
						"regionName": "七星区"
					},
					"450311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450311,
						"regionName": "雁山区"
					},
					"450312": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450312,
						"regionName": "临桂区"
					},
					"450321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450321,
						"regionName": "阳朔县"
					},
					"450323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450323,
						"regionName": "灵川县"
					},
					"450324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450324,
						"regionName": "全州县"
					},
					"450325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450325,
						"regionName": "兴安县"
					},
					"450326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450326,
						"regionName": "永福县"
					},
					"450327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450327,
						"regionName": "灌阳县"
					},
					"450328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450328,
						"regionName": "龙胜各族自治县"
					},
					"450329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450329,
						"regionName": "资源县"
					},
					"450330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450330,
						"regionName": "平乐县"
					},
					"450331": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450331,
						"regionName": "荔浦县"
					},
					"450332": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450332,
						"regionName": "恭城瑶族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 450300,
				"regionName": "桂林市"
			},
			"450400": {
				"areaId": 0,
				"children": {
					"450403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450403,
						"regionName": "万秀区"
					},
					"450405": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450405,
						"regionName": "长洲区"
					},
					"450406": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450406,
						"regionName": "龙圩区"
					},
					"450421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450421,
						"regionName": "苍梧县"
					},
					"450422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450422,
						"regionName": "藤县"
					},
					"450423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450423,
						"regionName": "蒙山县"
					},
					"450481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450481,
						"regionName": "岑溪市"
					}
				},
				"regionGrade": 2,
				"regionId": 450400,
				"regionName": "梧州市"
			},
			"450500": {
				"areaId": 0,
				"children": {
					"450502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450502,
						"regionName": "海城区"
					},
					"450503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450503,
						"regionName": "银海区"
					},
					"450512": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450512,
						"regionName": "铁山港区"
					},
					"450521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450521,
						"regionName": "合浦县"
					}
				},
				"regionGrade": 2,
				"regionId": 450500,
				"regionName": "北海市"
			},
			"450600": {
				"areaId": 0,
				"children": {
					"450602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450602,
						"regionName": "港口区"
					},
					"450603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450603,
						"regionName": "防城区"
					},
					"450621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450621,
						"regionName": "上思县"
					},
					"450681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450681,
						"regionName": "东兴市"
					}
				},
				"regionGrade": 2,
				"regionId": 450600,
				"regionName": "防城港市"
			},
			"450700": {
				"areaId": 0,
				"children": {
					"450702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450702,
						"regionName": "钦南区"
					},
					"450703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450703,
						"regionName": "钦北区"
					},
					"450721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450721,
						"regionName": "灵山县"
					},
					"450722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450722,
						"regionName": "浦北县"
					}
				},
				"regionGrade": 2,
				"regionId": 450700,
				"regionName": "钦州市"
			},
			"450800": {
				"areaId": 0,
				"children": {
					"450802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450802,
						"regionName": "港北区"
					},
					"450803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450803,
						"regionName": "港南区"
					},
					"450804": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450804,
						"regionName": "覃塘区"
					},
					"450821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450821,
						"regionName": "平南县"
					},
					"450881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450881,
						"regionName": "桂平市"
					}
				},
				"regionGrade": 2,
				"regionId": 450800,
				"regionName": "贵港市"
			},
			"450900": {
				"areaId": 0,
				"children": {
					"450902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450902,
						"regionName": "玉州区"
					},
					"450903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450903,
						"regionName": "福绵区"
					},
					"450921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450921,
						"regionName": "容县"
					},
					"450922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450922,
						"regionName": "陆川县"
					},
					"450923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450923,
						"regionName": "博白县"
					},
					"450924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450924,
						"regionName": "兴业县"
					},
					"450981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 450981,
						"regionName": "北流市"
					}
				},
				"regionGrade": 2,
				"regionId": 450900,
				"regionName": "玉林市"
			},
			"451000": {
				"areaId": 0,
				"children": {
					"451002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451002,
						"regionName": "右江区"
					},
					"451021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451021,
						"regionName": "田阳县"
					},
					"451022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451022,
						"regionName": "田东县"
					},
					"451023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451023,
						"regionName": "平果县"
					},
					"451024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451024,
						"regionName": "德保县"
					},
					"451026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451026,
						"regionName": "那坡县"
					},
					"451027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451027,
						"regionName": "凌云县"
					},
					"451028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451028,
						"regionName": "乐业县"
					},
					"451029": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451029,
						"regionName": "田林县"
					},
					"451030": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451030,
						"regionName": "西林县"
					},
					"451031": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451031,
						"regionName": "隆林各族自治县"
					},
					"451081": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451081,
						"regionName": "靖西市"
					}
				},
				"regionGrade": 2,
				"regionId": 451000,
				"regionName": "百色市"
			},
			"451100": {
				"areaId": 0,
				"children": {
					"451102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451102,
						"regionName": "八步区"
					},
					"451103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451103,
						"regionName": "平桂区"
					},
					"451121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451121,
						"regionName": "昭平县"
					},
					"451122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451122,
						"regionName": "钟山县"
					},
					"451123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451123,
						"regionName": "富川瑶族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 451100,
				"regionName": "贺州市"
			},
			"451200": {
				"areaId": 0,
				"children": {
					"451202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451202,
						"regionName": "金城江区"
					},
					"451203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451203,
						"regionName": "宜州区"
					},
					"451221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451221,
						"regionName": "南丹县"
					},
					"451222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451222,
						"regionName": "天峨县"
					},
					"451223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451223,
						"regionName": "凤山县"
					},
					"451224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451224,
						"regionName": "东兰县"
					},
					"451225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451225,
						"regionName": "罗城仫佬族自治县"
					},
					"451226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451226,
						"regionName": "环江毛南族自治县"
					},
					"451227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451227,
						"regionName": "巴马瑶族自治县"
					},
					"451228": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451228,
						"regionName": "都安瑶族自治县"
					},
					"451229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451229,
						"regionName": "大化瑶族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 451200,
				"regionName": "河池市"
			},
			"451300": {
				"areaId": 0,
				"children": {
					"451302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451302,
						"regionName": "兴宾区"
					},
					"451321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451321,
						"regionName": "忻城县"
					},
					"451322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451322,
						"regionName": "象州县"
					},
					"451323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451323,
						"regionName": "武宣县"
					},
					"451324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451324,
						"regionName": "金秀瑶族自治县"
					},
					"451381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451381,
						"regionName": "合山市"
					}
				},
				"regionGrade": 2,
				"regionId": 451300,
				"regionName": "来宾市"
			},
			"451400": {
				"areaId": 0,
				"children": {
					"451402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451402,
						"regionName": "江州区"
					},
					"451421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451421,
						"regionName": "扶绥县"
					},
					"451422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451422,
						"regionName": "宁明县"
					},
					"451423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451423,
						"regionName": "龙州县"
					},
					"451424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451424,
						"regionName": "大新县"
					},
					"451425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451425,
						"regionName": "天等县"
					},
					"451481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 451481,
						"regionName": "凭祥市"
					}
				},
				"regionGrade": 2,
				"regionId": 451400,
				"regionName": "崇左市"
			}
		},
		"regionGrade": 1,
		"regionId": 450000,
		"regionName": "广西壮族自治区"
	},
	"460000": {
		"areaId": 4,
		"children": {
			"429000": {
				"areaId": 0,
				"children": {
					"429001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429001,
						"regionName": "五指山市"
					},
					"429002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429002,
						"regionName": "琼海市"
					},
					"429005": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429005,
						"regionName": "文昌市"
					},
					"429006": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429006,
						"regionName": "万宁市"
					},
					"429007": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429007,
						"regionName": "东方市"
					},
					"429021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429021,
						"regionName": "定安县"
					},
					"429022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429022,
						"regionName": "屯昌县"
					},
					"429023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429023,
						"regionName": "澄迈县"
					},
					"429024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429024,
						"regionName": "临高县"
					},
					"429025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429025,
						"regionName": "白沙黎族自治县"
					},
					"429026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429026,
						"regionName": "昌江黎族自治县"
					},
					"429027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429027,
						"regionName": "乐东黎族自治县"
					},
					"429028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429028,
						"regionName": "陵水黎族自治县"
					},
					"429029": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429029,
						"regionName": "保亭黎族苗族自治县"
					},
					"429030": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 429030,
						"regionName": "琼中黎族苗族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 429000,
				"regionName": "省直辖县级行政单位"
			},
			"460100": {
				"areaId": 0,
				"children": {
					"460105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460105,
						"regionName": "秀英区"
					},
					"460106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460106,
						"regionName": "龙华区"
					},
					"460107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460107,
						"regionName": "琼山区"
					},
					"460108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460108,
						"regionName": "美兰区"
					}
				},
				"regionGrade": 2,
				"regionId": 460100,
				"regionName": "海口市"
			},
			"460200": {
				"areaId": 0,
				"children": {
					"460202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460202,
						"regionName": "海棠区"
					},
					"460203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460203,
						"regionName": "吉阳区"
					},
					"460204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460204,
						"regionName": "天涯区"
					},
					"460205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460205,
						"regionName": "崖州区"
					}
				},
				"regionGrade": 2,
				"regionId": 460200,
				"regionName": "三亚市"
			},
			"460300": {
				"areaId": 0,
				"children": {
					"460301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460301,
						"regionName": "三沙市"
					}
				},
				"regionGrade": 2,
				"regionId": 460300,
				"regionName": "三沙市"
			},
			"460400": {
				"areaId": 0,
				"children": {
					"460401": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 460401,
						"regionName": "儋州市"
					}
				},
				"regionGrade": 2,
				"regionId": 460400,
				"regionName": "儋州市"
			}
		},
		"regionGrade": 1,
		"regionId": 460000,
		"regionName": "海南省"
	},
	"500000": {
		"areaId": 7,
		"children": {
			"500100": {
				"areaId": 7,
				"children": {
					"500101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500101,
						"regionName": "万州区"
					},
					"500102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500102,
						"regionName": "涪陵区"
					},
					"500103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500103,
						"regionName": "渝中区"
					},
					"500104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500104,
						"regionName": "大渡口区"
					},
					"500105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500105,
						"regionName": "江北区"
					},
					"500106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500106,
						"regionName": "沙坪坝区"
					},
					"500107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500107,
						"regionName": "九龙坡区"
					},
					"500108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500108,
						"regionName": "南岸区"
					},
					"500109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500109,
						"regionName": "北碚区"
					},
					"500110": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500110,
						"regionName": "綦江区"
					},
					"500111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500111,
						"regionName": "大足区"
					},
					"500112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500112,
						"regionName": "渝北区"
					},
					"500113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500113,
						"regionName": "巴南区"
					},
					"500114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500114,
						"regionName": "黔江区"
					},
					"500115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500115,
						"regionName": "长寿区"
					},
					"500116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500116,
						"regionName": "江津区"
					},
					"500117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500117,
						"regionName": "合川区"
					},
					"500118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500118,
						"regionName": "永川区"
					},
					"500119": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500119,
						"regionName": "南川区"
					},
					"500120": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500120,
						"regionName": "璧山区"
					},
					"500151": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500151,
						"regionName": "铜梁区"
					},
					"500152": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500152,
						"regionName": "潼南区"
					},
					"500153": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500153,
						"regionName": "荣昌区"
					},
					"500154": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500154,
						"regionName": "开州区"
					},
					"500155": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500155,
						"regionName": "梁平区"
					},
					"500156": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500156,
						"regionName": "武隆区"
					},
					"500229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500229,
						"regionName": "城口县"
					},
					"500230": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500230,
						"regionName": "丰都县"
					},
					"500231": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500231,
						"regionName": "垫江县"
					},
					"500233": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500233,
						"regionName": "忠县"
					},
					"500235": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500235,
						"regionName": "云阳县"
					},
					"500236": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500236,
						"regionName": "奉节县"
					},
					"500237": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500237,
						"regionName": "巫山县"
					},
					"500238": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500238,
						"regionName": "巫溪县"
					},
					"500240": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500240,
						"regionName": "石柱土家族自治县"
					},
					"500241": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500241,
						"regionName": "秀山土家族苗族自治县"
					},
					"500242": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500242,
						"regionName": "酉阳土家族苗族自治县"
					},
					"500243": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500243,
						"regionName": "彭水苗族土家族自治县"
					},
					"500244": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500244,
						"regionName": "万盛区"
					},
					"500245": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 500245,
						"regionName": "双桥区"
					}
				},
				"regionGrade": 2,
				"regionId": 500100,
				"regionName": "重庆市"
			}
		},
		"regionGrade": 1,
		"regionId": 500000,
		"regionName": "重庆市"
	},
	"510000": {
		"areaId": 7,
		"children": {
			"510100": {
				"areaId": 0,
				"children": {
					"510104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510104,
						"regionName": "锦江区"
					},
					"510105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510105,
						"regionName": "青羊区"
					},
					"510106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510106,
						"regionName": "金牛区"
					},
					"510107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510107,
						"regionName": "武侯区"
					},
					"510108": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510108,
						"regionName": "成华区"
					},
					"510112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510112,
						"regionName": "龙泉驿区"
					},
					"510113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510113,
						"regionName": "青白江区"
					},
					"510114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510114,
						"regionName": "新都区"
					},
					"510115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510115,
						"regionName": "温江区"
					},
					"510116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510116,
						"regionName": "双流区"
					},
					"510117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510117,
						"regionName": "郫都区"
					},
					"510121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510121,
						"regionName": "金堂县"
					},
					"510129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510129,
						"regionName": "大邑县"
					},
					"510131": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510131,
						"regionName": "蒲江县"
					},
					"510132": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510132,
						"regionName": "新津县"
					},
					"510181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510181,
						"regionName": "都江堰市"
					},
					"510182": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510182,
						"regionName": "彭州市"
					},
					"510183": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510183,
						"regionName": "邛崃市"
					},
					"510184": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510184,
						"regionName": "崇州市"
					},
					"510185": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510185,
						"regionName": "简阳市"
					},
					"510186": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510186,
						"regionName": "高新区"
					},
					"510187": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510187,
						"regionName": "高新西区"
					}
				},
				"regionGrade": 2,
				"regionId": 510100,
				"regionName": "成都市"
			},
			"510300": {
				"areaId": 0,
				"children": {
					"510302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510302,
						"regionName": "自流井区"
					},
					"510303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510303,
						"regionName": "贡井区"
					},
					"510304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510304,
						"regionName": "大安区"
					},
					"510311": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510311,
						"regionName": "沿滩区"
					},
					"510321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510321,
						"regionName": "荣县"
					},
					"510322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510322,
						"regionName": "富顺县"
					}
				},
				"regionGrade": 2,
				"regionId": 510300,
				"regionName": "自贡市"
			},
			"510400": {
				"areaId": 0,
				"children": {
					"510402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510402,
						"regionName": "东区"
					},
					"510403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510403,
						"regionName": "西区"
					},
					"510411": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510411,
						"regionName": "仁和区"
					},
					"510421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510421,
						"regionName": "米易县"
					},
					"510422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510422,
						"regionName": "盐边县"
					}
				},
				"regionGrade": 2,
				"regionId": 510400,
				"regionName": "攀枝花市"
			},
			"510500": {
				"areaId": 0,
				"children": {
					"510502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510502,
						"regionName": "江阳区"
					},
					"510503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510503,
						"regionName": "纳溪区"
					},
					"510504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510504,
						"regionName": "龙马潭区"
					},
					"510521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510521,
						"regionName": "泸县"
					},
					"510522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510522,
						"regionName": "合江县"
					},
					"510524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510524,
						"regionName": "叙永县"
					},
					"510525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510525,
						"regionName": "古蔺县"
					}
				},
				"regionGrade": 2,
				"regionId": 510500,
				"regionName": "泸州市"
			},
			"510600": {
				"areaId": 0,
				"children": {
					"510603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510603,
						"regionName": "旌阳区"
					},
					"510604": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510604,
						"regionName": "罗江区"
					},
					"510623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510623,
						"regionName": "中江县"
					},
					"510681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510681,
						"regionName": "广汉市"
					},
					"510682": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510682,
						"regionName": "什邡市"
					},
					"510683": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510683,
						"regionName": "绵竹市"
					}
				},
				"regionGrade": 2,
				"regionId": 510600,
				"regionName": "德阳市"
			},
			"510700": {
				"areaId": 0,
				"children": {
					"510703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510703,
						"regionName": "涪城区"
					},
					"510704": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510704,
						"regionName": "游仙区"
					},
					"510705": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510705,
						"regionName": "安州区"
					},
					"510722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510722,
						"regionName": "三台县"
					},
					"510723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510723,
						"regionName": "盐亭县"
					},
					"510725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510725,
						"regionName": "梓潼县"
					},
					"510726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510726,
						"regionName": "北川羌族自治县"
					},
					"510727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510727,
						"regionName": "平武县"
					},
					"510781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510781,
						"regionName": "江油市"
					}
				},
				"regionGrade": 2,
				"regionId": 510700,
				"regionName": "绵阳市"
			},
			"510800": {
				"areaId": 0,
				"children": {
					"510802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510802,
						"regionName": "利州区"
					},
					"510811": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510811,
						"regionName": "昭化区"
					},
					"510812": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510812,
						"regionName": "朝天区"
					},
					"510821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510821,
						"regionName": "旺苍县"
					},
					"510822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510822,
						"regionName": "青川县"
					},
					"510823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510823,
						"regionName": "剑阁县"
					},
					"510824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510824,
						"regionName": "苍溪县"
					}
				},
				"regionGrade": 2,
				"regionId": 510800,
				"regionName": "广元市"
			},
			"510900": {
				"areaId": 0,
				"children": {
					"510903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510903,
						"regionName": "船山区"
					},
					"510904": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510904,
						"regionName": "安居区"
					},
					"510921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510921,
						"regionName": "蓬溪县"
					},
					"510922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510922,
						"regionName": "射洪县"
					},
					"510923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 510923,
						"regionName": "大英县"
					}
				},
				"regionGrade": 2,
				"regionId": 510900,
				"regionName": "遂宁市"
			},
			"511000": {
				"areaId": 0,
				"children": {
					"511002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511002,
						"regionName": "市中区"
					},
					"511011": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511011,
						"regionName": "东兴区"
					},
					"511024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511024,
						"regionName": "威远县"
					},
					"511025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511025,
						"regionName": "资中县"
					},
					"511083": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511083,
						"regionName": "隆昌市"
					}
				},
				"regionGrade": 2,
				"regionId": 511000,
				"regionName": "内江市"
			},
			"511100": {
				"areaId": 0,
				"children": {
					"511102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511102,
						"regionName": "市中区"
					},
					"511111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511111,
						"regionName": "沙湾区"
					},
					"511112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511112,
						"regionName": "五通桥区"
					},
					"511113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511113,
						"regionName": "金口河区"
					},
					"511123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511123,
						"regionName": "犍为县"
					},
					"511124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511124,
						"regionName": "井研县"
					},
					"511126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511126,
						"regionName": "夹江县"
					},
					"511129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511129,
						"regionName": "沐川县"
					},
					"511132": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511132,
						"regionName": "峨边彝族自治县"
					},
					"511133": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511133,
						"regionName": "马边彝族自治县"
					},
					"511181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511181,
						"regionName": "峨眉山市"
					}
				},
				"regionGrade": 2,
				"regionId": 511100,
				"regionName": "乐山市"
			},
			"511300": {
				"areaId": 0,
				"children": {
					"511302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511302,
						"regionName": "顺庆区"
					},
					"511303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511303,
						"regionName": "高坪区"
					},
					"511304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511304,
						"regionName": "嘉陵区"
					},
					"511321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511321,
						"regionName": "南部县"
					},
					"511322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511322,
						"regionName": "营山县"
					},
					"511323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511323,
						"regionName": "蓬安县"
					},
					"511324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511324,
						"regionName": "仪陇县"
					},
					"511325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511325,
						"regionName": "西充县"
					},
					"511381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511381,
						"regionName": "阆中市"
					}
				},
				"regionGrade": 2,
				"regionId": 511300,
				"regionName": "南充市"
			},
			"511400": {
				"areaId": 0,
				"children": {
					"511402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511402,
						"regionName": "东坡区"
					},
					"511403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511403,
						"regionName": "彭山区"
					},
					"511421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511421,
						"regionName": "仁寿县"
					},
					"511423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511423,
						"regionName": "洪雅县"
					},
					"511424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511424,
						"regionName": "丹棱县"
					},
					"511425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511425,
						"regionName": "青神县"
					}
				},
				"regionGrade": 2,
				"regionId": 511400,
				"regionName": "眉山市"
			},
			"511500": {
				"areaId": 0,
				"children": {
					"511502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511502,
						"regionName": "翠屏区"
					},
					"511503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511503,
						"regionName": "南溪区"
					},
					"511521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511521,
						"regionName": "宜宾县"
					},
					"511523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511523,
						"regionName": "江安县"
					},
					"511524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511524,
						"regionName": "长宁县"
					},
					"511525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511525,
						"regionName": "高县"
					},
					"511526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511526,
						"regionName": "珙县"
					},
					"511527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511527,
						"regionName": "筠连县"
					},
					"511528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511528,
						"regionName": "兴文县"
					},
					"511529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511529,
						"regionName": "屏山县"
					}
				},
				"regionGrade": 2,
				"regionId": 511500,
				"regionName": "宜宾市"
			},
			"511600": {
				"areaId": 0,
				"children": {
					"511602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511602,
						"regionName": "广安区"
					},
					"511603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511603,
						"regionName": "前锋区"
					},
					"511621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511621,
						"regionName": "岳池县"
					},
					"511622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511622,
						"regionName": "武胜县"
					},
					"511623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511623,
						"regionName": "邻水县"
					},
					"511681": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511681,
						"regionName": "华蓥市"
					}
				},
				"regionGrade": 2,
				"regionId": 511600,
				"regionName": "广安市"
			},
			"511700": {
				"areaId": 0,
				"children": {
					"511702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511702,
						"regionName": "通川区"
					},
					"511703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511703,
						"regionName": "达川区"
					},
					"511722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511722,
						"regionName": "宣汉县"
					},
					"511723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511723,
						"regionName": "开江县"
					},
					"511724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511724,
						"regionName": "大竹县"
					},
					"511725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511725,
						"regionName": "渠县"
					},
					"511781": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511781,
						"regionName": "万源市"
					}
				},
				"regionGrade": 2,
				"regionId": 511700,
				"regionName": "达州市"
			},
			"511800": {
				"areaId": 0,
				"children": {
					"511802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511802,
						"regionName": "雨城区"
					},
					"511803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511803,
						"regionName": "名山区"
					},
					"511822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511822,
						"regionName": "荥经县"
					},
					"511823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511823,
						"regionName": "汉源县"
					},
					"511824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511824,
						"regionName": "石棉县"
					},
					"511825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511825,
						"regionName": "天全县"
					},
					"511826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511826,
						"regionName": "芦山县"
					},
					"511827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511827,
						"regionName": "宝兴县"
					}
				},
				"regionGrade": 2,
				"regionId": 511800,
				"regionName": "雅安市"
			},
			"511900": {
				"areaId": 0,
				"children": {
					"511902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511902,
						"regionName": "巴州区"
					},
					"511903": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511903,
						"regionName": "恩阳区"
					},
					"511921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511921,
						"regionName": "通江县"
					},
					"511922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511922,
						"regionName": "南江县"
					},
					"511923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 511923,
						"regionName": "平昌县"
					}
				},
				"regionGrade": 2,
				"regionId": 511900,
				"regionName": "巴中市"
			},
			"512000": {
				"areaId": 0,
				"children": {
					"512002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 512002,
						"regionName": "雁江区"
					},
					"512021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 512021,
						"regionName": "安岳县"
					},
					"512022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 512022,
						"regionName": "乐至县"
					}
				},
				"regionGrade": 2,
				"regionId": 512000,
				"regionName": "资阳市"
			},
			"513200": {
				"areaId": 0,
				"children": {
					"513201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513201,
						"regionName": "马尔康市"
					},
					"513221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513221,
						"regionName": "汶川县"
					},
					"513222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513222,
						"regionName": "理县"
					},
					"513223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513223,
						"regionName": "茂县"
					},
					"513224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513224,
						"regionName": "松潘县"
					},
					"513225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513225,
						"regionName": "九寨沟县"
					},
					"513226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513226,
						"regionName": "金川县"
					},
					"513227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513227,
						"regionName": "小金县"
					},
					"513228": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513228,
						"regionName": "黑水县"
					},
					"513230": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513230,
						"regionName": "壤塘县"
					},
					"513231": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513231,
						"regionName": "阿坝县"
					},
					"513232": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513232,
						"regionName": "若尔盖县"
					},
					"513233": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513233,
						"regionName": "红原县"
					}
				},
				"regionGrade": 2,
				"regionId": 513200,
				"regionName": "阿坝藏族羌族自治州"
			},
			"513300": {
				"areaId": 0,
				"children": {
					"513301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513301,
						"regionName": "康定市"
					},
					"513322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513322,
						"regionName": "泸定县"
					},
					"513323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513323,
						"regionName": "丹巴县"
					},
					"513324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513324,
						"regionName": "九龙县"
					},
					"513325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513325,
						"regionName": "雅江县"
					},
					"513326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513326,
						"regionName": "道孚县"
					},
					"513327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513327,
						"regionName": "炉霍县"
					},
					"513328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513328,
						"regionName": "甘孜县"
					},
					"513329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513329,
						"regionName": "新龙县"
					},
					"513330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513330,
						"regionName": "德格县"
					},
					"513331": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513331,
						"regionName": "白玉县"
					},
					"513332": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513332,
						"regionName": "石渠县"
					},
					"513333": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513333,
						"regionName": "色达县"
					},
					"513334": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513334,
						"regionName": "理塘县"
					},
					"513335": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513335,
						"regionName": "巴塘县"
					},
					"513336": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513336,
						"regionName": "乡城县"
					},
					"513337": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513337,
						"regionName": "稻城县"
					},
					"513338": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513338,
						"regionName": "得荣县"
					}
				},
				"regionGrade": 2,
				"regionId": 513300,
				"regionName": "甘孜藏族自治州"
			},
			"513400": {
				"areaId": 0,
				"children": {
					"513401": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513401,
						"regionName": "西昌市"
					},
					"513422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513422,
						"regionName": "木里藏族自治县"
					},
					"513423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513423,
						"regionName": "盐源县"
					},
					"513424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513424,
						"regionName": "德昌县"
					},
					"513425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513425,
						"regionName": "会理县"
					},
					"513426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513426,
						"regionName": "会东县"
					},
					"513427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513427,
						"regionName": "宁南县"
					},
					"513428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513428,
						"regionName": "普格县"
					},
					"513429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513429,
						"regionName": "布拖县"
					},
					"513430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513430,
						"regionName": "金阳县"
					},
					"513431": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513431,
						"regionName": "昭觉县"
					},
					"513432": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513432,
						"regionName": "喜德县"
					},
					"513433": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513433,
						"regionName": "冕宁县"
					},
					"513434": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513434,
						"regionName": "越西县"
					},
					"513435": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513435,
						"regionName": "甘洛县"
					},
					"513436": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513436,
						"regionName": "美姑县"
					},
					"513437": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 513437,
						"regionName": "雷波县"
					}
				},
				"regionGrade": 2,
				"regionId": 513400,
				"regionName": "凉山彝族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 510000,
		"regionName": "四川省"
	},
	"520000": {
		"areaId": 7,
		"children": {
			"520100": {
				"areaId": 0,
				"children": {
					"520102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520102,
						"regionName": "南明区"
					},
					"520103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520103,
						"regionName": "云岩区"
					},
					"520111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520111,
						"regionName": "花溪区"
					},
					"520112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520112,
						"regionName": "乌当区"
					},
					"520113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520113,
						"regionName": "白云区"
					},
					"520115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520115,
						"regionName": "观山湖区"
					},
					"520121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520121,
						"regionName": "开阳县"
					},
					"520122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520122,
						"regionName": "息烽县"
					},
					"520123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520123,
						"regionName": "修文县"
					},
					"520181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520181,
						"regionName": "清镇市"
					}
				},
				"regionGrade": 2,
				"regionId": 520100,
				"regionName": "贵阳市"
			},
			"520200": {
				"areaId": 0,
				"children": {
					"520201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520201,
						"regionName": "钟山区"
					},
					"520203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520203,
						"regionName": "六枝特区"
					},
					"520221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520221,
						"regionName": "水城县"
					},
					"520281": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520281,
						"regionName": "盘州市"
					}
				},
				"regionGrade": 2,
				"regionId": 520200,
				"regionName": "六盘水市"
			},
			"520300": {
				"areaId": 0,
				"children": {
					"520302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520302,
						"regionName": "红花岗区"
					},
					"520303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520303,
						"regionName": "汇川区"
					},
					"520304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520304,
						"regionName": "播州区"
					},
					"520322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520322,
						"regionName": "桐梓县"
					},
					"520323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520323,
						"regionName": "绥阳县"
					},
					"520324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520324,
						"regionName": "正安县"
					},
					"520325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520325,
						"regionName": "道真仡佬族苗族自治县"
					},
					"520326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520326,
						"regionName": "务川仡佬族苗族自治县"
					},
					"520327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520327,
						"regionName": "凤冈县"
					},
					"520328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520328,
						"regionName": "湄潭县"
					},
					"520329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520329,
						"regionName": "余庆县"
					},
					"520330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520330,
						"regionName": "习水县"
					},
					"520381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520381,
						"regionName": "赤水市"
					},
					"520382": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520382,
						"regionName": "仁怀市"
					}
				},
				"regionGrade": 2,
				"regionId": 520300,
				"regionName": "遵义市"
			},
			"520400": {
				"areaId": 0,
				"children": {
					"520402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520402,
						"regionName": "西秀区"
					},
					"520403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520403,
						"regionName": "平坝区"
					},
					"520422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520422,
						"regionName": "普定县"
					},
					"520423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520423,
						"regionName": "镇宁布依族苗族自治县"
					},
					"520424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520424,
						"regionName": "关岭布依族苗族自治县"
					},
					"520425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520425,
						"regionName": "紫云苗族布依族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 520400,
				"regionName": "安顺市"
			},
			"520500": {
				"areaId": 0,
				"children": {
					"520502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520502,
						"regionName": "七星关区"
					},
					"520521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520521,
						"regionName": "大方县"
					},
					"520522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520522,
						"regionName": "黔西县"
					},
					"520523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520523,
						"regionName": "金沙县"
					},
					"520524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520524,
						"regionName": "织金县"
					},
					"520525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520525,
						"regionName": "纳雍县"
					},
					"520526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520526,
						"regionName": "威宁彝族回族苗族自治县"
					},
					"520527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520527,
						"regionName": "赫章县"
					}
				},
				"regionGrade": 2,
				"regionId": 520500,
				"regionName": "毕节市"
			},
			"520600": {
				"areaId": 0,
				"children": {
					"520602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520602,
						"regionName": "碧江区"
					},
					"520603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520603,
						"regionName": "万山区"
					},
					"520621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520621,
						"regionName": "江口县"
					},
					"520622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520622,
						"regionName": "玉屏侗族自治县"
					},
					"520623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520623,
						"regionName": "石阡县"
					},
					"520624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520624,
						"regionName": "思南县"
					},
					"520625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520625,
						"regionName": "印江土家族苗族自治县"
					},
					"520626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520626,
						"regionName": "德江县"
					},
					"520627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520627,
						"regionName": "沿河土家族自治县"
					},
					"520628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 520628,
						"regionName": "松桃苗族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 520600,
				"regionName": "铜仁市"
			},
			"522300": {
				"areaId": 0,
				"children": {
					"522301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522301,
						"regionName": "兴义市"
					},
					"522322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522322,
						"regionName": "兴仁县"
					},
					"522323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522323,
						"regionName": "普安县"
					},
					"522324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522324,
						"regionName": "晴隆县"
					},
					"522325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522325,
						"regionName": "贞丰县"
					},
					"522326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522326,
						"regionName": "望谟县"
					},
					"522327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522327,
						"regionName": "册亨县"
					},
					"522328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522328,
						"regionName": "安龙县"
					}
				},
				"regionGrade": 2,
				"regionId": 522300,
				"regionName": "黔西南布依族苗族自治州"
			},
			"522600": {
				"areaId": 0,
				"children": {
					"522601": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522601,
						"regionName": "凯里市"
					},
					"522622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522622,
						"regionName": "黄平县"
					},
					"522623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522623,
						"regionName": "施秉县"
					},
					"522624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522624,
						"regionName": "三穗县"
					},
					"522625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522625,
						"regionName": "镇远县"
					},
					"522626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522626,
						"regionName": "岑巩县"
					},
					"522627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522627,
						"regionName": "天柱县"
					},
					"522628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522628,
						"regionName": "锦屏县"
					},
					"522629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522629,
						"regionName": "剑河县"
					},
					"522630": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522630,
						"regionName": "台江县"
					},
					"522631": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522631,
						"regionName": "黎平县"
					},
					"522632": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522632,
						"regionName": "榕江县"
					},
					"522633": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522633,
						"regionName": "从江县"
					},
					"522634": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522634,
						"regionName": "雷山县"
					},
					"522635": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522635,
						"regionName": "麻江县"
					},
					"522636": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522636,
						"regionName": "丹寨县"
					}
				},
				"regionGrade": 2,
				"regionId": 522600,
				"regionName": "黔东南苗族侗族自治州"
			},
			"522700": {
				"areaId": 0,
				"children": {
					"522701": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522701,
						"regionName": "都匀市"
					},
					"522702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522702,
						"regionName": "福泉市"
					},
					"522722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522722,
						"regionName": "荔波县"
					},
					"522723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522723,
						"regionName": "贵定县"
					},
					"522725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522725,
						"regionName": "瓮安县"
					},
					"522726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522726,
						"regionName": "独山县"
					},
					"522727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522727,
						"regionName": "平塘县"
					},
					"522728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522728,
						"regionName": "罗甸县"
					},
					"522729": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522729,
						"regionName": "长顺县"
					},
					"522730": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522730,
						"regionName": "龙里县"
					},
					"522731": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522731,
						"regionName": "惠水县"
					},
					"522732": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 522732,
						"regionName": "三都水族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 522700,
				"regionName": "黔南布依族苗族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 520000,
		"regionName": "贵州省"
	},
	"530000": {
		"areaId": 7,
		"children": {
			"530100": {
				"areaId": 0,
				"children": {
					"530102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530102,
						"regionName": "五华区"
					},
					"530103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530103,
						"regionName": "盘龙区"
					},
					"530111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530111,
						"regionName": "官渡区"
					},
					"530112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530112,
						"regionName": "西山区"
					},
					"530113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530113,
						"regionName": "东川区"
					},
					"530114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530114,
						"regionName": "呈贡区"
					},
					"530115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530115,
						"regionName": "晋宁区"
					},
					"530124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530124,
						"regionName": "富民县"
					},
					"530125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530125,
						"regionName": "宜良县"
					},
					"530126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530126,
						"regionName": "石林彝族自治县"
					},
					"530127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530127,
						"regionName": "嵩明县"
					},
					"530128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530128,
						"regionName": "禄劝彝族苗族自治县"
					},
					"530129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530129,
						"regionName": "寻甸回族彝族自治县"
					},
					"530181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530181,
						"regionName": "安宁市"
					}
				},
				"regionGrade": 2,
				"regionId": 530100,
				"regionName": "昆明市"
			},
			"530300": {
				"areaId": 0,
				"children": {
					"530302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530302,
						"regionName": "麒麟区"
					},
					"530303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530303,
						"regionName": "沾益区"
					},
					"530321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530321,
						"regionName": "马龙县"
					},
					"530322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530322,
						"regionName": "陆良县"
					},
					"530323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530323,
						"regionName": "师宗县"
					},
					"530324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530324,
						"regionName": "罗平县"
					},
					"530325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530325,
						"regionName": "富源县"
					},
					"530326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530326,
						"regionName": "会泽县"
					},
					"530381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530381,
						"regionName": "宣威市"
					}
				},
				"regionGrade": 2,
				"regionId": 530300,
				"regionName": "曲靖市"
			},
			"530400": {
				"areaId": 0,
				"children": {
					"530402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530402,
						"regionName": "红塔区"
					},
					"530403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530403,
						"regionName": "江川区"
					},
					"530422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530422,
						"regionName": "澄江县"
					},
					"530423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530423,
						"regionName": "通海县"
					},
					"530424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530424,
						"regionName": "华宁县"
					},
					"530425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530425,
						"regionName": "易门县"
					},
					"530426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530426,
						"regionName": "峨山彝族自治县"
					},
					"530427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530427,
						"regionName": "新平彝族傣族自治县"
					},
					"530428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530428,
						"regionName": "元江哈尼族彝族傣族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 530400,
				"regionName": "玉溪市"
			},
			"530500": {
				"areaId": 0,
				"children": {
					"530502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530502,
						"regionName": "隆阳区"
					},
					"530521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530521,
						"regionName": "施甸县"
					},
					"530523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530523,
						"regionName": "龙陵县"
					},
					"530524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530524,
						"regionName": "昌宁县"
					},
					"530581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530581,
						"regionName": "腾冲市"
					}
				},
				"regionGrade": 2,
				"regionId": 530500,
				"regionName": "保山市"
			},
			"530600": {
				"areaId": 0,
				"children": {
					"530602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530602,
						"regionName": "昭阳区"
					},
					"530621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530621,
						"regionName": "鲁甸县"
					},
					"530622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530622,
						"regionName": "巧家县"
					},
					"530623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530623,
						"regionName": "盐津县"
					},
					"530624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530624,
						"regionName": "大关县"
					},
					"530625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530625,
						"regionName": "永善县"
					},
					"530626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530626,
						"regionName": "绥江县"
					},
					"530627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530627,
						"regionName": "镇雄县"
					},
					"530628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530628,
						"regionName": "彝良县"
					},
					"530629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530629,
						"regionName": "威信县"
					},
					"530630": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530630,
						"regionName": "水富县"
					}
				},
				"regionGrade": 2,
				"regionId": 530600,
				"regionName": "昭通市"
			},
			"530700": {
				"areaId": 0,
				"children": {
					"530702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530702,
						"regionName": "古城区"
					},
					"530721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530721,
						"regionName": "玉龙纳西族自治县"
					},
					"530722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530722,
						"regionName": "永胜县"
					},
					"530723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530723,
						"regionName": "华坪县"
					},
					"530724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530724,
						"regionName": "宁蒗彝族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 530700,
				"regionName": "丽江市"
			},
			"530800": {
				"areaId": 0,
				"children": {
					"530802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530802,
						"regionName": "思茅区"
					},
					"530821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530821,
						"regionName": "宁洱哈尼族彝族自治县"
					},
					"530822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530822,
						"regionName": "墨江哈尼族自治县"
					},
					"530823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530823,
						"regionName": "景东彝族自治县"
					},
					"530824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530824,
						"regionName": "景谷傣族彝族自治县"
					},
					"530825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530825,
						"regionName": "镇沅彝族哈尼族拉祜族自治县"
					},
					"530826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530826,
						"regionName": "江城哈尼族彝族自治县"
					},
					"530827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530827,
						"regionName": "孟连傣族拉祜族佤族自治县"
					},
					"530828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530828,
						"regionName": "澜沧拉祜族自治县"
					},
					"530829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530829,
						"regionName": "西盟佤族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 530800,
				"regionName": "普洱市"
			},
			"530900": {
				"areaId": 0,
				"children": {
					"530902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530902,
						"regionName": "临翔区"
					},
					"530921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530921,
						"regionName": "凤庆县"
					},
					"530922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530922,
						"regionName": "云县"
					},
					"530923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530923,
						"regionName": "永德县"
					},
					"530924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530924,
						"regionName": "镇康县"
					},
					"530925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530925,
						"regionName": "双江拉祜族佤族布朗族傣族自治县"
					},
					"530926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530926,
						"regionName": "耿马傣族佤族自治县"
					},
					"530927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 530927,
						"regionName": "沧源佤族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 530900,
				"regionName": "临沧市"
			},
			"532300": {
				"areaId": 0,
				"children": {
					"532301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532301,
						"regionName": "楚雄市"
					},
					"532322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532322,
						"regionName": "双柏县"
					},
					"532323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532323,
						"regionName": "牟定县"
					},
					"532324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532324,
						"regionName": "南华县"
					},
					"532325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532325,
						"regionName": "姚安县"
					},
					"532326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532326,
						"regionName": "大姚县"
					},
					"532327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532327,
						"regionName": "永仁县"
					},
					"532328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532328,
						"regionName": "元谋县"
					},
					"532329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532329,
						"regionName": "武定县"
					},
					"532331": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532331,
						"regionName": "禄丰县"
					}
				},
				"regionGrade": 2,
				"regionId": 532300,
				"regionName": "楚雄彝族自治州"
			},
			"532500": {
				"areaId": 0,
				"children": {
					"532501": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532501,
						"regionName": "个旧市"
					},
					"532502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532502,
						"regionName": "开远市"
					},
					"532503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532503,
						"regionName": "蒙自市"
					},
					"532504": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532504,
						"regionName": "弥勒市"
					},
					"532523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532523,
						"regionName": "屏边苗族自治县"
					},
					"532524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532524,
						"regionName": "建水县"
					},
					"532525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532525,
						"regionName": "石屏县"
					},
					"532527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532527,
						"regionName": "泸西县"
					},
					"532528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532528,
						"regionName": "元阳县"
					},
					"532529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532529,
						"regionName": "红河县"
					},
					"532530": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532530,
						"regionName": "金平苗族瑶族傣族自治县"
					},
					"532531": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532531,
						"regionName": "绿春县"
					},
					"532532": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532532,
						"regionName": "河口瑶族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 532500,
				"regionName": "红河哈尼族彝族自治州"
			},
			"532600": {
				"areaId": 0,
				"children": {
					"532601": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532601,
						"regionName": "文山市"
					},
					"532622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532622,
						"regionName": "砚山县"
					},
					"532623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532623,
						"regionName": "西畴县"
					},
					"532624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532624,
						"regionName": "麻栗坡县"
					},
					"532625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532625,
						"regionName": "马关县"
					},
					"532626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532626,
						"regionName": "丘北县"
					},
					"532627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532627,
						"regionName": "广南县"
					},
					"532628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532628,
						"regionName": "富宁县"
					}
				},
				"regionGrade": 2,
				"regionId": 532600,
				"regionName": "文山壮族苗族自治州"
			},
			"532800": {
				"areaId": 0,
				"children": {
					"532801": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532801,
						"regionName": "景洪市"
					},
					"532822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532822,
						"regionName": "勐海县"
					},
					"532823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532823,
						"regionName": "勐腊县"
					}
				},
				"regionGrade": 2,
				"regionId": 532800,
				"regionName": "西双版纳傣族自治州"
			},
			"532900": {
				"areaId": 0,
				"children": {
					"532901": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532901,
						"regionName": "大理市"
					},
					"532922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532922,
						"regionName": "漾濞彝族自治县"
					},
					"532923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532923,
						"regionName": "祥云县"
					},
					"532924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532924,
						"regionName": "宾川县"
					},
					"532925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532925,
						"regionName": "弥渡县"
					},
					"532926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532926,
						"regionName": "南涧彝族自治县"
					},
					"532927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532927,
						"regionName": "巍山彝族回族自治县"
					},
					"532928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532928,
						"regionName": "永平县"
					},
					"532929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532929,
						"regionName": "云龙县"
					},
					"532930": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532930,
						"regionName": "洱源县"
					},
					"532931": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532931,
						"regionName": "剑川县"
					},
					"532932": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 532932,
						"regionName": "鹤庆县"
					}
				},
				"regionGrade": 2,
				"regionId": 532900,
				"regionName": "大理白族自治州"
			},
			"533100": {
				"areaId": 0,
				"children": {
					"533102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533102,
						"regionName": "瑞丽市"
					},
					"533103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533103,
						"regionName": "芒市"
					},
					"533122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533122,
						"regionName": "梁河县"
					},
					"533123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533123,
						"regionName": "盈江县"
					},
					"533124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533124,
						"regionName": "陇川县"
					}
				},
				"regionGrade": 2,
				"regionId": 533100,
				"regionName": "德宏傣族景颇族自治州"
			},
			"533300": {
				"areaId": 0,
				"children": {
					"533301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533301,
						"regionName": "泸水市"
					},
					"533323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533323,
						"regionName": "福贡县"
					},
					"533324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533324,
						"regionName": "贡山独龙族怒族自治县"
					},
					"533325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533325,
						"regionName": "兰坪白族普米族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 533300,
				"regionName": "怒江傈僳族自治州"
			},
			"533400": {
				"areaId": 0,
				"children": {
					"533401": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533401,
						"regionName": "香格里拉市"
					},
					"533422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533422,
						"regionName": "德钦县"
					},
					"533423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 533423,
						"regionName": "维西傈僳族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 533400,
				"regionName": "迪庆藏族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 530000,
		"regionName": "云南省"
	},
	"540000": {
		"areaId": 7,
		"children": {
			"540100": {
				"areaId": 0,
				"children": {
					"540102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540102,
						"regionName": "城关区"
					},
					"540103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540103,
						"regionName": "堆龙德庆区"
					},
					"540104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540104,
						"regionName": "达孜区"
					},
					"540121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540121,
						"regionName": "林周县"
					},
					"540122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540122,
						"regionName": "当雄县"
					},
					"540123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540123,
						"regionName": "尼木县"
					},
					"540124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540124,
						"regionName": "曲水县"
					},
					"540127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540127,
						"regionName": "墨竹工卡县"
					}
				},
				"regionGrade": 2,
				"regionId": 540100,
				"regionName": "拉萨市"
			},
			"540200": {
				"areaId": 0,
				"children": {
					"540202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540202,
						"regionName": "桑珠孜区"
					},
					"540221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540221,
						"regionName": "南木林县"
					},
					"540222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540222,
						"regionName": "江孜县"
					},
					"540223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540223,
						"regionName": "定日县"
					},
					"540224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540224,
						"regionName": "萨迦县"
					},
					"540225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540225,
						"regionName": "拉孜县"
					},
					"540226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540226,
						"regionName": "昂仁县"
					},
					"540227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540227,
						"regionName": "谢通门县"
					},
					"540228": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540228,
						"regionName": "白朗县"
					},
					"540229": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540229,
						"regionName": "仁布县"
					},
					"540230": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540230,
						"regionName": "康马县"
					},
					"540231": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540231,
						"regionName": "定结县"
					},
					"540232": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540232,
						"regionName": "仲巴县"
					},
					"540233": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540233,
						"regionName": "亚东县"
					},
					"540234": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540234,
						"regionName": "吉隆县"
					},
					"540235": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540235,
						"regionName": "聂拉木县"
					},
					"540236": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540236,
						"regionName": "萨嘎县"
					},
					"540237": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540237,
						"regionName": "岗巴县"
					}
				},
				"regionGrade": 2,
				"regionId": 540200,
				"regionName": "日喀则市"
			},
			"540300": {
				"areaId": 0,
				"children": {
					"540302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540302,
						"regionName": "卡若区"
					},
					"540321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540321,
						"regionName": "江达县"
					},
					"540322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540322,
						"regionName": "贡觉县"
					},
					"540323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540323,
						"regionName": "类乌齐县"
					},
					"540324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540324,
						"regionName": "丁青县"
					},
					"540325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540325,
						"regionName": "察雅县"
					},
					"540326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540326,
						"regionName": "八宿县"
					},
					"540327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540327,
						"regionName": "左贡县"
					},
					"540328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540328,
						"regionName": "芒康县"
					},
					"540329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540329,
						"regionName": "洛隆县"
					},
					"540330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540330,
						"regionName": "边坝县"
					}
				},
				"regionGrade": 2,
				"regionId": 540300,
				"regionName": "昌都市"
			},
			"540400": {
				"areaId": 0,
				"children": {
					"540402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540402,
						"regionName": "巴宜区"
					},
					"540421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540421,
						"regionName": "工布江达县"
					},
					"540422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540422,
						"regionName": "米林县"
					},
					"540423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540423,
						"regionName": "墨脱县"
					},
					"540424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540424,
						"regionName": "波密县"
					},
					"540425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540425,
						"regionName": "察隅县"
					},
					"540426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540426,
						"regionName": "朗县"
					}
				},
				"regionGrade": 2,
				"regionId": 540400,
				"regionName": "林芝市"
			},
			"540500": {
				"areaId": 0,
				"children": {
					"540502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540502,
						"regionName": "乃东区"
					},
					"540521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540521,
						"regionName": "扎囊县"
					},
					"540522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540522,
						"regionName": "贡嘎县"
					},
					"540523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540523,
						"regionName": "桑日县"
					},
					"540524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540524,
						"regionName": "琼结县"
					},
					"540525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540525,
						"regionName": "曲松县"
					},
					"540526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540526,
						"regionName": "措美县"
					},
					"540527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540527,
						"regionName": "洛扎县"
					},
					"540528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540528,
						"regionName": "加查县"
					},
					"540529": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540529,
						"regionName": "隆子县"
					},
					"540530": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540530,
						"regionName": "错那县"
					},
					"540531": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540531,
						"regionName": "浪卡子县"
					}
				},
				"regionGrade": 2,
				"regionId": 540500,
				"regionName": "山南市"
			},
			"540600": {
				"areaId": 0,
				"children": {
					"540602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540602,
						"regionName": "色尼区"
					},
					"540621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540621,
						"regionName": "嘉黎县"
					},
					"540622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540622,
						"regionName": "比如县"
					},
					"540623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540623,
						"regionName": "聂荣县"
					},
					"540624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540624,
						"regionName": "安多县"
					},
					"540625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540625,
						"regionName": "申扎县"
					},
					"540626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540626,
						"regionName": "索县"
					},
					"540627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540627,
						"regionName": "班戈县"
					},
					"540628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540628,
						"regionName": "巴青县"
					},
					"540629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540629,
						"regionName": "尼玛县"
					},
					"540630": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 540630,
						"regionName": "双湖县"
					}
				},
				"regionGrade": 2,
				"regionId": 540600,
				"regionName": "那曲市"
			},
			"542500": {
				"areaId": 0,
				"children": {
					"542521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542521,
						"regionName": "普兰县"
					},
					"542522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542522,
						"regionName": "札达县"
					},
					"542523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542523,
						"regionName": "噶尔县"
					},
					"542524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542524,
						"regionName": "日土县"
					},
					"542525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542525,
						"regionName": "革吉县"
					},
					"542526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542526,
						"regionName": "改则县"
					},
					"542527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 542527,
						"regionName": "措勤县"
					}
				},
				"regionGrade": 2,
				"regionId": 542500,
				"regionName": "阿里地区"
			}
		},
		"regionGrade": 1,
		"regionId": 540000,
		"regionName": "西藏自治区"
	},
	"610000": {
		"areaId": 6,
		"children": {
			"610100": {
				"areaId": 0,
				"children": {
					"610102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610102,
						"regionName": "新城区"
					},
					"610103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610103,
						"regionName": "碑林区"
					},
					"610104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610104,
						"regionName": "莲湖区"
					},
					"610111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610111,
						"regionName": "灞桥区"
					},
					"610112": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610112,
						"regionName": "未央区"
					},
					"610113": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610113,
						"regionName": "雁塔区"
					},
					"610114": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610114,
						"regionName": "阎良区"
					},
					"610115": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610115,
						"regionName": "临潼区"
					},
					"610116": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610116,
						"regionName": "长安区"
					},
					"610117": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610117,
						"regionName": "高陵区"
					},
					"610118": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610118,
						"regionName": "鄠邑区"
					},
					"610122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610122,
						"regionName": "蓝田县"
					},
					"610124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610124,
						"regionName": "周至县"
					}
				},
				"regionGrade": 2,
				"regionId": 610100,
				"regionName": "西安市"
			},
			"610200": {
				"areaId": 0,
				"children": {
					"610202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610202,
						"regionName": "王益区"
					},
					"610203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610203,
						"regionName": "印台区"
					},
					"610204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610204,
						"regionName": "耀州区"
					},
					"610222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610222,
						"regionName": "宜君县"
					}
				},
				"regionGrade": 2,
				"regionId": 610200,
				"regionName": "铜川市"
			},
			"610300": {
				"areaId": 0,
				"children": {
					"610302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610302,
						"regionName": "渭滨区"
					},
					"610303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610303,
						"regionName": "金台区"
					},
					"610304": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610304,
						"regionName": "陈仓区"
					},
					"610322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610322,
						"regionName": "凤翔县"
					},
					"610323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610323,
						"regionName": "岐山县"
					},
					"610324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610324,
						"regionName": "扶风县"
					},
					"610326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610326,
						"regionName": "眉县"
					},
					"610327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610327,
						"regionName": "陇县"
					},
					"610328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610328,
						"regionName": "千阳县"
					},
					"610329": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610329,
						"regionName": "麟游县"
					},
					"610330": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610330,
						"regionName": "凤县"
					},
					"610331": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610331,
						"regionName": "太白县"
					}
				},
				"regionGrade": 2,
				"regionId": 610300,
				"regionName": "宝鸡市"
			},
			"610400": {
				"areaId": 0,
				"children": {
					"610402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610402,
						"regionName": "秦都区"
					},
					"610403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610403,
						"regionName": "杨陵区"
					},
					"610404": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610404,
						"regionName": "渭城区"
					},
					"610422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610422,
						"regionName": "三原县"
					},
					"610423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610423,
						"regionName": "泾阳县"
					},
					"610424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610424,
						"regionName": "乾县"
					},
					"610425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610425,
						"regionName": "礼泉县"
					},
					"610426": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610426,
						"regionName": "永寿县"
					},
					"610427": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610427,
						"regionName": "彬县"
					},
					"610428": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610428,
						"regionName": "长武县"
					},
					"610429": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610429,
						"regionName": "旬邑县"
					},
					"610430": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610430,
						"regionName": "淳化县"
					},
					"610431": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610431,
						"regionName": "武功县"
					},
					"610481": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610481,
						"regionName": "兴平市"
					}
				},
				"regionGrade": 2,
				"regionId": 610400,
				"regionName": "咸阳市"
			},
			"610500": {
				"areaId": 0,
				"children": {
					"610502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610502,
						"regionName": "临渭区"
					},
					"610503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610503,
						"regionName": "华州区"
					},
					"610522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610522,
						"regionName": "潼关县"
					},
					"610523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610523,
						"regionName": "大荔县"
					},
					"610524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610524,
						"regionName": "合阳县"
					},
					"610525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610525,
						"regionName": "澄城县"
					},
					"610526": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610526,
						"regionName": "蒲城县"
					},
					"610527": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610527,
						"regionName": "白水县"
					},
					"610528": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610528,
						"regionName": "富平县"
					},
					"610581": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610581,
						"regionName": "韩城市"
					},
					"610582": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610582,
						"regionName": "华阴市"
					}
				},
				"regionGrade": 2,
				"regionId": 610500,
				"regionName": "渭南市"
			},
			"610600": {
				"areaId": 0,
				"children": {
					"610602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610602,
						"regionName": "宝塔区"
					},
					"610603": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610603,
						"regionName": "安塞区"
					},
					"610621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610621,
						"regionName": "延长县"
					},
					"610622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610622,
						"regionName": "延川县"
					},
					"610623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610623,
						"regionName": "子长县"
					},
					"610625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610625,
						"regionName": "志丹县"
					},
					"610626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610626,
						"regionName": "吴起县"
					},
					"610627": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610627,
						"regionName": "甘泉县"
					},
					"610628": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610628,
						"regionName": "富县"
					},
					"610629": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610629,
						"regionName": "洛川县"
					},
					"610630": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610630,
						"regionName": "宜川县"
					},
					"610631": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610631,
						"regionName": "黄龙县"
					},
					"610632": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610632,
						"regionName": "黄陵县"
					}
				},
				"regionGrade": 2,
				"regionId": 610600,
				"regionName": "延安市"
			},
			"610700": {
				"areaId": 0,
				"children": {
					"610702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610702,
						"regionName": "汉台区"
					},
					"610703": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610703,
						"regionName": "南郑区"
					},
					"610722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610722,
						"regionName": "城固县"
					},
					"610723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610723,
						"regionName": "洋县"
					},
					"610724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610724,
						"regionName": "西乡县"
					},
					"610725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610725,
						"regionName": "勉县"
					},
					"610726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610726,
						"regionName": "宁强县"
					},
					"610727": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610727,
						"regionName": "略阳县"
					},
					"610728": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610728,
						"regionName": "镇巴县"
					},
					"610729": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610729,
						"regionName": "留坝县"
					},
					"610730": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610730,
						"regionName": "佛坪县"
					}
				},
				"regionGrade": 2,
				"regionId": 610700,
				"regionName": "汉中市"
			},
			"610800": {
				"areaId": 0,
				"children": {
					"610802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610802,
						"regionName": "榆阳区"
					},
					"610803": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610803,
						"regionName": "横山区"
					},
					"610822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610822,
						"regionName": "府谷县"
					},
					"610824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610824,
						"regionName": "靖边县"
					},
					"610825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610825,
						"regionName": "定边县"
					},
					"610826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610826,
						"regionName": "绥德县"
					},
					"610827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610827,
						"regionName": "米脂县"
					},
					"610828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610828,
						"regionName": "佳县"
					},
					"610829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610829,
						"regionName": "吴堡县"
					},
					"610830": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610830,
						"regionName": "清涧县"
					},
					"610831": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610831,
						"regionName": "子洲县"
					},
					"610881": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610881,
						"regionName": "神木市"
					}
				},
				"regionGrade": 2,
				"regionId": 610800,
				"regionName": "榆林市"
			},
			"610900": {
				"areaId": 0,
				"children": {
					"610902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610902,
						"regionName": "汉滨区"
					},
					"610921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610921,
						"regionName": "汉阴县"
					},
					"610922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610922,
						"regionName": "石泉县"
					},
					"610923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610923,
						"regionName": "宁陕县"
					},
					"610924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610924,
						"regionName": "紫阳县"
					},
					"610925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610925,
						"regionName": "岚皋县"
					},
					"610926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610926,
						"regionName": "平利县"
					},
					"610927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610927,
						"regionName": "镇坪县"
					},
					"610928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610928,
						"regionName": "旬阳县"
					},
					"610929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 610929,
						"regionName": "白河县"
					}
				},
				"regionGrade": 2,
				"regionId": 610900,
				"regionName": "安康市"
			},
			"611000": {
				"areaId": 0,
				"children": {
					"611002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611002,
						"regionName": "商州区"
					},
					"611021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611021,
						"regionName": "洛南县"
					},
					"611022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611022,
						"regionName": "丹凤县"
					},
					"611023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611023,
						"regionName": "商南县"
					},
					"611024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611024,
						"regionName": "山阳县"
					},
					"611025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611025,
						"regionName": "镇安县"
					},
					"611026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 611026,
						"regionName": "柞水县"
					}
				},
				"regionGrade": 2,
				"regionId": 611000,
				"regionName": "商洛市"
			}
		},
		"regionGrade": 1,
		"regionId": 610000,
		"regionName": "陕西省"
	},
	"620000": {
		"areaId": 6,
		"children": {
			"620100": {
				"areaId": 0,
				"children": {
					"620102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620102,
						"regionName": "城关区"
					},
					"620103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620103,
						"regionName": "七里河区"
					},
					"620104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620104,
						"regionName": "西固区"
					},
					"620105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620105,
						"regionName": "安宁区"
					},
					"620111": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620111,
						"regionName": "红古区"
					},
					"620121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620121,
						"regionName": "永登县"
					},
					"620122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620122,
						"regionName": "皋兰县"
					},
					"620123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620123,
						"regionName": "榆中县"
					}
				},
				"regionGrade": 2,
				"regionId": 620100,
				"regionName": "兰州市"
			},
			"620200": {
				"areaId": 0,
				"children": {
					"620201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620201,
						"regionName": "嘉峪关市"
					}
				},
				"regionGrade": 2,
				"regionId": 620200,
				"regionName": "嘉峪关市"
			},
			"620300": {
				"areaId": 0,
				"children": {
					"620302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620302,
						"regionName": "金川区"
					},
					"620321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620321,
						"regionName": "永昌县"
					}
				},
				"regionGrade": 2,
				"regionId": 620300,
				"regionName": "金昌市"
			},
			"620400": {
				"areaId": 0,
				"children": {
					"620402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620402,
						"regionName": "白银区"
					},
					"620403": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620403,
						"regionName": "平川区"
					},
					"620421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620421,
						"regionName": "靖远县"
					},
					"620422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620422,
						"regionName": "会宁县"
					},
					"620423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620423,
						"regionName": "景泰县"
					}
				},
				"regionGrade": 2,
				"regionId": 620400,
				"regionName": "白银市"
			},
			"620500": {
				"areaId": 0,
				"children": {
					"620502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620502,
						"regionName": "秦州区"
					},
					"620503": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620503,
						"regionName": "麦积区"
					},
					"620521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620521,
						"regionName": "清水县"
					},
					"620522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620522,
						"regionName": "秦安县"
					},
					"620523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620523,
						"regionName": "甘谷县"
					},
					"620524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620524,
						"regionName": "武山县"
					},
					"620525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620525,
						"regionName": "张家川回族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 620500,
				"regionName": "天水市"
			},
			"620600": {
				"areaId": 0,
				"children": {
					"620602": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620602,
						"regionName": "凉州区"
					},
					"620621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620621,
						"regionName": "民勤县"
					},
					"620622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620622,
						"regionName": "古浪县"
					},
					"620623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620623,
						"regionName": "天祝藏族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 620600,
				"regionName": "武威市"
			},
			"620700": {
				"areaId": 0,
				"children": {
					"620702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620702,
						"regionName": "甘州区"
					},
					"620721": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620721,
						"regionName": "肃南裕固族自治县"
					},
					"620722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620722,
						"regionName": "民乐县"
					},
					"620723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620723,
						"regionName": "临泽县"
					},
					"620724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620724,
						"regionName": "高台县"
					},
					"620725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620725,
						"regionName": "山丹县"
					}
				},
				"regionGrade": 2,
				"regionId": 620700,
				"regionName": "张掖市"
			},
			"620800": {
				"areaId": 0,
				"children": {
					"620802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620802,
						"regionName": "崆峒区"
					},
					"620821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620821,
						"regionName": "泾川县"
					},
					"620822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620822,
						"regionName": "灵台县"
					},
					"620823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620823,
						"regionName": "崇信县"
					},
					"620824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620824,
						"regionName": "华亭县"
					},
					"620825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620825,
						"regionName": "庄浪县"
					},
					"620826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620826,
						"regionName": "静宁县"
					}
				},
				"regionGrade": 2,
				"regionId": 620800,
				"regionName": "平凉市"
			},
			"620900": {
				"areaId": 0,
				"children": {
					"620902": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620902,
						"regionName": "肃州区"
					},
					"620921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620921,
						"regionName": "金塔县"
					},
					"620922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620922,
						"regionName": "瓜州县"
					},
					"620923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620923,
						"regionName": "肃北蒙古族自治县"
					},
					"620924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620924,
						"regionName": "阿克塞哈萨克族自治县"
					},
					"620981": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620981,
						"regionName": "玉门市"
					},
					"620982": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 620982,
						"regionName": "敦煌市"
					}
				},
				"regionGrade": 2,
				"regionId": 620900,
				"regionName": "酒泉市"
			},
			"621000": {
				"areaId": 0,
				"children": {
					"621002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621002,
						"regionName": "西峰区"
					},
					"621021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621021,
						"regionName": "庆城县"
					},
					"621022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621022,
						"regionName": "环县"
					},
					"621023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621023,
						"regionName": "华池县"
					},
					"621024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621024,
						"regionName": "合水县"
					},
					"621025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621025,
						"regionName": "正宁县"
					},
					"621026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621026,
						"regionName": "宁县"
					},
					"621027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621027,
						"regionName": "镇原县"
					}
				},
				"regionGrade": 2,
				"regionId": 621000,
				"regionName": "庆阳市"
			},
			"621100": {
				"areaId": 0,
				"children": {
					"621102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621102,
						"regionName": "安定区"
					},
					"621121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621121,
						"regionName": "通渭县"
					},
					"621122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621122,
						"regionName": "陇西县"
					},
					"621123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621123,
						"regionName": "渭源县"
					},
					"621124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621124,
						"regionName": "临洮县"
					},
					"621125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621125,
						"regionName": "漳县"
					},
					"621126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621126,
						"regionName": "岷县"
					}
				},
				"regionGrade": 2,
				"regionId": 621100,
				"regionName": "定西市"
			},
			"621200": {
				"areaId": 0,
				"children": {
					"621202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621202,
						"regionName": "武都区"
					},
					"621221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621221,
						"regionName": "成县"
					},
					"621222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621222,
						"regionName": "文县"
					},
					"621223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621223,
						"regionName": "宕昌县"
					},
					"621224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621224,
						"regionName": "康县"
					},
					"621225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621225,
						"regionName": "西和县"
					},
					"621226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621226,
						"regionName": "礼县"
					},
					"621227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621227,
						"regionName": "徽县"
					},
					"621228": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 621228,
						"regionName": "两当县"
					}
				},
				"regionGrade": 2,
				"regionId": 621200,
				"regionName": "陇南市"
			},
			"622900": {
				"areaId": 0,
				"children": {
					"622901": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622901,
						"regionName": "临夏市"
					},
					"622921": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622921,
						"regionName": "临夏县"
					},
					"622922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622922,
						"regionName": "康乐县"
					},
					"622923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622923,
						"regionName": "永靖县"
					},
					"622924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622924,
						"regionName": "广河县"
					},
					"622925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622925,
						"regionName": "和政县"
					},
					"622926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622926,
						"regionName": "东乡族自治县"
					},
					"622927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 622927,
						"regionName": "积石山保安族东乡族撒拉族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 622900,
				"regionName": "临夏回族自治州"
			},
			"623000": {
				"areaId": 0,
				"children": {
					"623001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623001,
						"regionName": "合作市"
					},
					"623021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623021,
						"regionName": "临潭县"
					},
					"623022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623022,
						"regionName": "卓尼县"
					},
					"623023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623023,
						"regionName": "舟曲县"
					},
					"623024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623024,
						"regionName": "迭部县"
					},
					"623025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623025,
						"regionName": "玛曲县"
					},
					"623026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623026,
						"regionName": "碌曲县"
					},
					"623027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 623027,
						"regionName": "夏河县"
					}
				},
				"regionGrade": 2,
				"regionId": 623000,
				"regionName": "甘南藏族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 620000,
		"regionName": "甘肃省"
	},
	"630000": {
		"areaId": 6,
		"children": {
			"630100": {
				"areaId": 0,
				"children": {
					"630102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630102,
						"regionName": "城东区"
					},
					"630103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630103,
						"regionName": "城中区"
					},
					"630104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630104,
						"regionName": "城西区"
					},
					"630105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630105,
						"regionName": "城北区"
					},
					"630121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630121,
						"regionName": "大通回族土族自治县"
					},
					"630122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630122,
						"regionName": "湟中县"
					},
					"630123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630123,
						"regionName": "湟源县"
					}
				},
				"regionGrade": 2,
				"regionId": 630100,
				"regionName": "西宁市"
			},
			"630200": {
				"areaId": 0,
				"children": {
					"630202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630202,
						"regionName": "乐都区"
					},
					"630203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630203,
						"regionName": "平安区"
					},
					"630222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630222,
						"regionName": "民和回族土族自治县"
					},
					"630223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630223,
						"regionName": "互助土族自治县"
					},
					"630224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630224,
						"regionName": "化隆回族自治县"
					},
					"630225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 630225,
						"regionName": "循化撒拉族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 630200,
				"regionName": "海东市"
			},
			"632200": {
				"areaId": 0,
				"children": {
					"632221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632221,
						"regionName": "门源回族自治县"
					},
					"632222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632222,
						"regionName": "祁连县"
					},
					"632223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632223,
						"regionName": "海晏县"
					},
					"632224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632224,
						"regionName": "刚察县"
					}
				},
				"regionGrade": 2,
				"regionId": 632200,
				"regionName": "海北藏族自治州"
			},
			"632300": {
				"areaId": 0,
				"children": {
					"632321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632321,
						"regionName": "同仁县"
					},
					"632322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632322,
						"regionName": "尖扎县"
					},
					"632323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632323,
						"regionName": "泽库县"
					},
					"632324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632324,
						"regionName": "河南蒙古族自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 632300,
				"regionName": "黄南藏族自治州"
			},
			"632500": {
				"areaId": 0,
				"children": {
					"632521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632521,
						"regionName": "共和县"
					},
					"632522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632522,
						"regionName": "同德县"
					},
					"632523": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632523,
						"regionName": "贵德县"
					},
					"632524": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632524,
						"regionName": "兴海县"
					},
					"632525": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632525,
						"regionName": "贵南县"
					}
				},
				"regionGrade": 2,
				"regionId": 632500,
				"regionName": "海南藏族自治州"
			},
			"632600": {
				"areaId": 0,
				"children": {
					"632621": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632621,
						"regionName": "玛沁县"
					},
					"632622": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632622,
						"regionName": "班玛县"
					},
					"632623": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632623,
						"regionName": "甘德县"
					},
					"632624": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632624,
						"regionName": "达日县"
					},
					"632625": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632625,
						"regionName": "久治县"
					},
					"632626": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632626,
						"regionName": "玛多县"
					}
				},
				"regionGrade": 2,
				"regionId": 632600,
				"regionName": "果洛藏族自治州"
			},
			"632700": {
				"areaId": 0,
				"children": {
					"632701": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632701,
						"regionName": "玉树市"
					},
					"632722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632722,
						"regionName": "杂多县"
					},
					"632723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632723,
						"regionName": "称多县"
					},
					"632724": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632724,
						"regionName": "治多县"
					},
					"632725": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632725,
						"regionName": "囊谦县"
					},
					"632726": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632726,
						"regionName": "曲麻莱县"
					}
				},
				"regionGrade": 2,
				"regionId": 632700,
				"regionName": "玉树藏族自治州"
			},
			"632800": {
				"areaId": 0,
				"children": {
					"632801": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632801,
						"regionName": "格尔木市"
					},
					"632802": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632802,
						"regionName": "德令哈市"
					},
					"632821": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632821,
						"regionName": "乌兰县"
					},
					"632822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632822,
						"regionName": "都兰县"
					},
					"632823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 632823,
						"regionName": "天峻县"
					}
				},
				"regionGrade": 2,
				"regionId": 632800,
				"regionName": "海西蒙古族藏族自治州"
			}
		},
		"regionGrade": 1,
		"regionId": 630000,
		"regionName": "青海省"
	},
	"640000": {
		"areaId": 6,
		"children": {
			"640100": {
				"areaId": 0,
				"children": {
					"640104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640104,
						"regionName": "兴庆区"
					},
					"640105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640105,
						"regionName": "西夏区"
					},
					"640106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640106,
						"regionName": "金凤区"
					},
					"640121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640121,
						"regionName": "永宁县"
					},
					"640122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640122,
						"regionName": "贺兰县"
					},
					"640181": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640181,
						"regionName": "灵武市"
					}
				},
				"regionGrade": 2,
				"regionId": 640100,
				"regionName": "银川市"
			},
			"640200": {
				"areaId": 0,
				"children": {
					"640202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640202,
						"regionName": "大武口区"
					},
					"640205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640205,
						"regionName": "惠农区"
					},
					"640221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640221,
						"regionName": "平罗县"
					}
				},
				"regionGrade": 2,
				"regionId": 640200,
				"regionName": "石嘴山市"
			},
			"640300": {
				"areaId": 0,
				"children": {
					"640302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640302,
						"regionName": "利通区"
					},
					"640303": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640303,
						"regionName": "红寺堡区"
					},
					"640323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640323,
						"regionName": "盐池县"
					},
					"640324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640324,
						"regionName": "同心县"
					},
					"640381": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640381,
						"regionName": "青铜峡市"
					}
				},
				"regionGrade": 2,
				"regionId": 640300,
				"regionName": "吴忠市"
			},
			"640400": {
				"areaId": 0,
				"children": {
					"640402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640402,
						"regionName": "原州区"
					},
					"640422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640422,
						"regionName": "西吉县"
					},
					"640423": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640423,
						"regionName": "隆德县"
					},
					"640424": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640424,
						"regionName": "泾源县"
					},
					"640425": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640425,
						"regionName": "彭阳县"
					}
				},
				"regionGrade": 2,
				"regionId": 640400,
				"regionName": "固原市"
			},
			"640500": {
				"areaId": 0,
				"children": {
					"640502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640502,
						"regionName": "沙坡头区"
					},
					"640521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640521,
						"regionName": "中宁县"
					},
					"640522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 640522,
						"regionName": "海原县"
					}
				},
				"regionGrade": 2,
				"regionId": 640500,
				"regionName": "中卫市"
			}
		},
		"regionGrade": 1,
		"regionId": 640000,
		"regionName": "宁夏回族自治区"
	},
	"650000": {
		"areaId": 6,
		"children": {
			"650100": {
				"areaId": 0,
				"children": {
					"650102": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650102,
						"regionName": "天山区"
					},
					"650103": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650103,
						"regionName": "沙依巴克区"
					},
					"650104": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650104,
						"regionName": "新市区"
					},
					"650105": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650105,
						"regionName": "水磨沟区"
					},
					"650106": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650106,
						"regionName": "头屯河区"
					},
					"650107": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650107,
						"regionName": "达坂城区"
					},
					"650109": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650109,
						"regionName": "米东区"
					},
					"650121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650121,
						"regionName": "乌鲁木齐县"
					}
				},
				"regionGrade": 2,
				"regionId": 650100,
				"regionName": "乌鲁木齐市"
			},
			"650200": {
				"areaId": 0,
				"children": {
					"650202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650202,
						"regionName": "独山子区"
					},
					"650203": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650203,
						"regionName": "克拉玛依区"
					},
					"650204": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650204,
						"regionName": "白碱滩区"
					},
					"650205": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650205,
						"regionName": "乌尔禾区"
					}
				},
				"regionGrade": 2,
				"regionId": 650200,
				"regionName": "克拉玛依市"
			},
			"650400": {
				"areaId": 0,
				"children": {
					"650402": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650402,
						"regionName": "高昌区"
					},
					"650421": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650421,
						"regionName": "鄯善县"
					},
					"650422": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650422,
						"regionName": "托克逊县"
					}
				},
				"regionGrade": 2,
				"regionId": 650400,
				"regionName": "吐鲁番市"
			},
			"650500": {
				"areaId": 0,
				"children": {
					"650502": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650502,
						"regionName": "伊州区"
					},
					"650521": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650521,
						"regionName": "巴里坤哈萨克自治县"
					},
					"650522": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 650522,
						"regionName": "伊吾县"
					}
				},
				"regionGrade": 2,
				"regionId": 650500,
				"regionName": "哈密市"
			},
			"652300": {
				"areaId": 0,
				"children": {
					"652301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652301,
						"regionName": "昌吉市"
					},
					"652302": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652302,
						"regionName": "阜康市"
					},
					"652323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652323,
						"regionName": "呼图壁县"
					},
					"652324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652324,
						"regionName": "玛纳斯县"
					},
					"652325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652325,
						"regionName": "奇台县"
					},
					"652327": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652327,
						"regionName": "吉木萨尔县"
					},
					"652328": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652328,
						"regionName": "木垒哈萨克自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 652300,
				"regionName": "昌吉回族自治州"
			},
			"652700": {
				"areaId": 0,
				"children": {
					"652701": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652701,
						"regionName": "博乐市"
					},
					"652702": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652702,
						"regionName": "阿拉山口市"
					},
					"652722": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652722,
						"regionName": "精河县"
					},
					"652723": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652723,
						"regionName": "温泉县"
					}
				},
				"regionGrade": 2,
				"regionId": 652700,
				"regionName": "博尔塔拉蒙古自治州"
			},
			"652800": {
				"areaId": 0,
				"children": {
					"652801": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652801,
						"regionName": "库尔勒市"
					},
					"652822": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652822,
						"regionName": "轮台县"
					},
					"652823": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652823,
						"regionName": "尉犁县"
					},
					"652824": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652824,
						"regionName": "若羌县"
					},
					"652825": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652825,
						"regionName": "且末县"
					},
					"652826": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652826,
						"regionName": "焉耆回族自治县"
					},
					"652827": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652827,
						"regionName": "和静县"
					},
					"652828": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652828,
						"regionName": "和硕县"
					},
					"652829": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652829,
						"regionName": "博湖县"
					}
				},
				"regionGrade": 2,
				"regionId": 652800,
				"regionName": "巴音郭楞蒙古自治州"
			},
			"652900": {
				"areaId": 0,
				"children": {
					"652901": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652901,
						"regionName": "阿克苏市"
					},
					"652922": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652922,
						"regionName": "温宿县"
					},
					"652923": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652923,
						"regionName": "库车县"
					},
					"652924": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652924,
						"regionName": "沙雅县"
					},
					"652925": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652925,
						"regionName": "新和县"
					},
					"652926": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652926,
						"regionName": "拜城县"
					},
					"652927": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652927,
						"regionName": "乌什县"
					},
					"652928": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652928,
						"regionName": "阿瓦提县"
					},
					"652929": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 652929,
						"regionName": "柯坪县"
					}
				},
				"regionGrade": 2,
				"regionId": 652900,
				"regionName": "阿克苏地区"
			},
			"653000": {
				"areaId": 0,
				"children": {
					"653001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653001,
						"regionName": "阿图什市"
					},
					"653022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653022,
						"regionName": "阿克陶县"
					},
					"653023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653023,
						"regionName": "阿合奇县"
					},
					"653024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653024,
						"regionName": "乌恰县"
					}
				},
				"regionGrade": 2,
				"regionId": 653000,
				"regionName": "克孜勒苏柯尔克孜自治州"
			},
			"653100": {
				"areaId": 0,
				"children": {
					"653101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653101,
						"regionName": "喀什市"
					},
					"653121": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653121,
						"regionName": "疏附县"
					},
					"653122": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653122,
						"regionName": "疏勒县"
					},
					"653123": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653123,
						"regionName": "英吉沙县"
					},
					"653124": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653124,
						"regionName": "泽普县"
					},
					"653125": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653125,
						"regionName": "莎车县"
					},
					"653126": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653126,
						"regionName": "叶城县"
					},
					"653127": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653127,
						"regionName": "麦盖提县"
					},
					"653128": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653128,
						"regionName": "岳普湖县"
					},
					"653129": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653129,
						"regionName": "伽师县"
					},
					"653130": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653130,
						"regionName": "巴楚县"
					},
					"653131": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653131,
						"regionName": "塔什库尔干塔吉克自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 653100,
				"regionName": "喀什地区"
			},
			"653200": {
				"areaId": 0,
				"children": {
					"653201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653201,
						"regionName": "和田市"
					},
					"653221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653221,
						"regionName": "和田县"
					},
					"653222": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653222,
						"regionName": "墨玉县"
					},
					"653223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653223,
						"regionName": "皮山县"
					},
					"653224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653224,
						"regionName": "洛浦县"
					},
					"653225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653225,
						"regionName": "策勒县"
					},
					"653226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653226,
						"regionName": "于田县"
					},
					"653227": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 653227,
						"regionName": "民丰县"
					}
				},
				"regionGrade": 2,
				"regionId": 653200,
				"regionName": "和田地区"
			},
			"654000": {
				"areaId": 0,
				"children": {
					"654002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654002,
						"regionName": "伊宁市"
					},
					"654003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654003,
						"regionName": "奎屯市"
					},
					"654004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654004,
						"regionName": "霍尔果斯市"
					},
					"654021": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654021,
						"regionName": "伊宁县"
					},
					"654022": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654022,
						"regionName": "察布查尔锡伯自治县"
					},
					"654023": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654023,
						"regionName": "霍城县"
					},
					"654024": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654024,
						"regionName": "巩留县"
					},
					"654025": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654025,
						"regionName": "新源县"
					},
					"654026": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654026,
						"regionName": "昭苏县"
					},
					"654027": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654027,
						"regionName": "特克斯县"
					},
					"654028": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654028,
						"regionName": "尼勒克县"
					}
				},
				"regionGrade": 2,
				"regionId": 654000,
				"regionName": "伊犁哈萨克自治州"
			},
			"654200": {
				"areaId": 0,
				"children": {
					"654201": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654201,
						"regionName": "塔城市"
					},
					"654202": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654202,
						"regionName": "乌苏市"
					},
					"654221": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654221,
						"regionName": "额敏县"
					},
					"654223": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654223,
						"regionName": "沙湾县"
					},
					"654224": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654224,
						"regionName": "托里县"
					},
					"654225": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654225,
						"regionName": "裕民县"
					},
					"654226": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654226,
						"regionName": "和布克赛尔蒙古自治县"
					}
				},
				"regionGrade": 2,
				"regionId": 654200,
				"regionName": "塔城地区"
			},
			"654300": {
				"areaId": 0,
				"children": {
					"654301": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654301,
						"regionName": "阿勒泰市"
					},
					"654321": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654321,
						"regionName": "布尔津县"
					},
					"654322": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654322,
						"regionName": "富蕴县"
					},
					"654323": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654323,
						"regionName": "福海县"
					},
					"654324": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654324,
						"regionName": "哈巴河县"
					},
					"654325": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654325,
						"regionName": "青河县"
					},
					"654326": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 654326,
						"regionName": "吉木乃县"
					}
				},
				"regionGrade": 2,
				"regionId": 654300,
				"regionName": "阿勒泰地区"
			},
			"659000": {
				"areaId": 0,
				"children": {
					"659001": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659001,
						"regionName": "石河子市"
					},
					"659002": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659002,
						"regionName": "阿拉尔市"
					},
					"659003": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659003,
						"regionName": "图木舒克市"
					},
					"659004": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659004,
						"regionName": "五家渠市"
					},
					"659005": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659005,
						"regionName": "北屯市"
					},
					"659006": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659006,
						"regionName": "铁门关市"
					},
					"659007": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659007,
						"regionName": "双河市"
					},
					"659008": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659008,
						"regionName": "可克达拉市"
					},
					"659009": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 659009,
						"regionName": "昆玉市"
					}
				},
				"regionGrade": 2,
				"regionId": 659000,
				"regionName": "自治区直辖县级行政单位"
			}
		},
		"regionGrade": 1,
		"regionId": 650000,
		"regionName": "新疆维吾尔自治区"
	},
	"710000": {
		"areaId": 8,
		"children": {
			"710100": {
				"areaId": 0,
				"children": {
					"710101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 710101,
						"regionName": "台湾"
					}
				},
				"regionGrade": 2,
				"regionId": 710100,
				"regionName": "台湾"
			}
		},
		"regionGrade": 1,
		"regionId": 710000,
		"regionName": "台湾省"
	},
	"810000": {
		"areaId": 8,
		"children": {
			"810100": {
				"areaId": 0,
				"children": {
					"810101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 810101,
						"regionName": "香港"
					}
				},
				"regionGrade": 2,
				"regionId": 810100,
				"regionName": "香港"
			}
		},
		"regionGrade": 1,
		"regionId": 810000,
		"regionName": "香港特别行政区"
	},
	"820000": {
		"areaId": 8,
		"children": {
			"820100": {
				"areaId": 0,
				"children": {
					"820101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 820101,
						"regionName": "澳门"
					}
				},
				"regionGrade": 2,
				"regionId": 820100,
				"regionName": "澳门"
			}
		},
		"regionGrade": 1,
		"regionId": 820000,
		"regionName": "澳门特别行政区"
	},
	"910000": {
		"areaId": 9,
		"children": {
			"910100": {
				"areaId": 0,
				"children": {
					"910101": {
						"areaId": 0,
						"children": {},
						"regionGrade": 3,
						"regionId": 910101,
						"regionName": "海外"
					}
				},
				"regionGrade": 2,
				"regionId": 910100,
				"regionName": "海外"
			}
		},
		"regionGrade": 1,
		"regionId": 910000,
		"regionName": "海外"
	}
}


export default obj

