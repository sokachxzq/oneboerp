import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
};

const staticRouter = [
    {
        path: '*',
        redirect: '/new/indexAdmin'
    },
    {
        path: '/indexAdmin',
        name: 'indexAdmin',
        component: resolve => require(['views/homePage/index/indexAdmin'], resolve)
    },
    {
        path: '/loginAdmin',
        name: 'loginAdmin',
        component: resolve => require(['views/loginAdmin/loginAdmin'], resolve)
    },
    {
        path: '/loginAgree',
        name: 'loginAgree',
        component: resolve => require(['views/loginAdmin/loginAgree'], resolve)
    },
    {
        path: '/helpAdmin',
        name: 'helpAdmin',
        component: resolve => require(['views/homePage/help/helpAdmin'], resolve)
    },
    {
        path: '/agreeAdmin',
        name: 'agreeAdmin',
        component: resolve => require(['views/homePage/agree/agreeAdmin'], resolve)
    },
    {
        path: '/VIPServerAdmin',
        name: 'VIPServerAdmin',
        component: resolve => require(['views/homePage/VIP/VIPServerAdmin'], resolve)
    },
    {
        path: '/new/indexAdmin',
        name: 'new_indexAdmin',
        component: () => import('views/homePageNew/index/indexAdmin')
    },
    {
        path: '/new/productAdmin',
        name: 'new_productAdmin',
        component: () => import('views/homePageNew/index/productAdmin')
    },
    {
        path: '/new/supplierAdmin',
        name: 'new_supplierAdmin',
        component: () => import('views/homePageNew/index/supplierAdmin')
    },
    {
        path: '/new/VIPServerAdmin',
        name: 'new_VIPServerAdmin',
        component: () => import('views/homePageNew/index/VIPServerAdmin')
    },
    {
        path: '/new/ERPShopAdmin',
        name: 'new_ERPShopAdmin',
        component: () => import('views/homePageNew/index/ERPShopAdmin')
    },
    {
        path: '/new/serverAdmin1',
        name: 'new_serverAdmin1',
        component: () => import('views/homePageNew/index/serverAdmin1')
    },
    {
        path: '/new/serverAdmin2',
        name: 'new_serverAdmin2',
        component: () => import('views/homePageNew/index/serverAdmin2')
    },
    {
        path: '/new/serverAdmin3',
        name: 'new_serverAdmin3',
        component: () => import('views/homePageNew/index/serverAdmin3')
    },
    {
        path: '/new/commertAdmin/:name',
        name: 'new_commertAdmin',
        component: () => import('views/homePageNew/index/commertAdmin')
    },
    {
        path: '/new/loginAdmin/:inviteCode?',
        name: 'new_loginAdmin',
        component: () => import('views/homePageNew/index/loginAdmin')
    },
]

const routers = new Router({
    mode: 'history',
    // base: '/weberp',
    routes: staticRouter
})

routers.beforeEach((to, from, next) => {
   // next();
    window.scrollTo({
        top: 0,
        behavior: 'smooth',
    })
    next();
})

export default routers