import Http from '../../http'

export default {
    // TikTok ================================
    // 授权链接
    AuthUrl(params) {
        return Http.get('/Tiktok/Auth/Url', { params })
    },
    // 授权回调
    AuthCallback(data) {
        return Http.post('/Tiktok/Auth/Callback', data)
    },
    // 授权列表
    AuthList(params) {
        return Http.get('/Tiktok/Auth/List', { params })
    },
    // 授权账号编辑
    AuthEdit(data) {
        return Http.post('/Tiktok/Auth/Edit', data)
    },


    // 获取平台列表
    PlatformList(params) {
        return Http.get('/Platform/List', { params })
    },


    // 授权店铺列表
    ShopList(params) {
        return Http.get('/Tiktok/Shop/List', { params })
    },


    // 分类列表
    GlobalCategoryList(params) {
        return Http.get('/Tiktok/GlobalCategory/List', { params })
    },
    // 分类规则列表
    GlobalCategoryRuleList(params) {
        return Http.get('/Tiktok/GlobalCategory/RuleList', { params })
    },
    // 分类属性列表
    GlobalCategoryAttributeList(params) {
        return Http.get('/Tiktok/GlobalCategory/AttributeList', { params })
    },
    // 分类品牌列表
    CategoryBrandPage(params) {
        return Http.get('/Tiktok/Category/BrandPage', { params })
    },


    // 产品列表
    GlobalProductPage(data, params) {
        return Http.post('/Tiktok/GlobalProduct/Page', data, { params })
    },
    // 创建
    GlobalProductCreate(data, params) {
        return Http.post('/Tiktok/GlobalProduct/Create', data, { params })
    },
    // 编辑
    GlobalProductEdit(data, params) {
        return Http.post('/Tiktok/GlobalProduct/Edit', data, { params })
    },
    // 导入
    GlobalProductImport(data, params) {
        return Http.post('/Tiktok/GlobalProduct/Import', data, { params })
    },
    // 发布
    GlobalProductPublish(data, params) {
        return Http.post('/Tiktok/GlobalProduct/Publish', data, { params })
    },
    // 进度
    ProductBatchRecordQuery(params) {
        return Http.get('/Product/BatchRecord/Query', { params })
    },
    // 批量修改产品
    GlobalProductEdit(data) {
        return Http.post('/Tiktok/GlobalProduct/Edit', data)
    },


    // 获取未完成的产品批量处理任务
    ProductBatchRecordPage(params) {
        return Http.get('/Product/BatchRecord/Page', { params })
    },
    // 获取产品批处理明细列表
    ProductBatchRecordDetailList(params) {
        return Http.get('/Product/BatchRecord/DetailList', { params })
    },
    // 继续未完成的产品批量处理任务
    ProductBatchRecordContinue(data) {
        return Http.post('/Product/BatchRecord/Continue', data)
    },
    // 取消未完成的产品批量处理任务
    ProductBatchRecordCancel(data) {
        return Http.post('/Product/BatchRecord/Cancel', data)
    },


    // 分页获取采集箱产品列表
    ProductCollectPage(params) {
        return Http.get('/Product/Collect/Page', { params })
    },
    // 添加采集箱产品
    ProductCollectAdd(data) {
        return Http.post('/Product/Collect/Add', data)
    },
    // 删除采集箱产品
    ProductCollectDelete(data) {
        return Http.post('/Product/Collect/Delete', data)
    },


    // 定时任务
    // 分页获取定时任务列表
    ProductCrontabPage(params) {
        return Http.get('/Product/Crontab/Page', { params })
    },
    // 添加定时任务
    ProductCrontabAdd(data) {
        return Http.post('/Product/Crontab/Add', data)
    },
    // 删除定时任务
    ProductCrontabCancel(data) {
        return Http.post('/Product/Crontab/Cancel', data)
    },

    
    // 订单列表
    OrderPage(data, params) {
        return Http.post('/Tiktok/Order/Page', data, { params })
    },
    // 详情
    OrderDetail(data, params) {
        return Http.post('/Tiktok/Order/Detail', data, { params })
    },
    // 物流
    ShippingInfo(params) {
        return Http.get('/Tiktok/Shipping/Info', { params })
    },
    // 快递单文件
    ShippingDocument(params) {
        return Http.get('/Tiktok/Shipping/Document', { params })
    },
    // 发货
    ShippingPackage(data, params) {
        return Http.post('/Tiktok/Shipping/Package', data, { params })
    },
    

    // 上传图片
    UploadImage(data, params) {
        return Http.post('/Tiktok/Upload/Image', data, { params })
    },


}
