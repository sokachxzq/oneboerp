
import Http from '../../http'

export default {
    // onebo ================================
    // 获取员工列表
    EmployeePage(params) {
        return Http.get('/Employee/Page', { params })
    },
    // 添加员工
    EmployeeAdd(data) {
        return Http.post('/Employee/Add', data)
    },
    // 编辑员工
    EmployeeEdit(data) {
        return Http.post('/Employee/Edit', data)
    },


    // 分页获取部门列表
    EmployeeDepartmentPage(params) {
        return Http.get('/Employee/Department/Page', { params })
    },
    // 获取部门下拉列表
    EmployeeDepartmentDropList(params) {
        return Http.get('/Employee/Department/DropList', { params })
    },
    // 添加部门
    EmployeeDepartmentAdd(data) {
        return Http.post('/Employee/Department/Add', data)
    },
    // 编辑部门
    EmployeeDepartmentEdit(data) {
        return Http.post('/Employee/Department/Edit', data)
    },


    // 分页获取职位列表
    EmployeePostPage(params) {
        return Http.get('/Employee/Post/Page', { params })
    },
    // 获取职位下拉列表
    EmployeePostDropList(params) {
        return Http.get('/Employee/Post/DropList', { params })
    },
    // 添加职位
    EmployeePostAdd(data) {
        return Http.post('/Employee/Post/Add', data)
    },
    // 编辑职位
    EmployeePostEdit(data) {
        return Http.post('/Employee/Post/Edit', data)
    },

}
