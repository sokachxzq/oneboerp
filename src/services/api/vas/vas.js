import Http from "../../http";

export default {
  List(params) {
    return Http.get("/Vas/List", { params });
  },
  Create(data) {
    return Http.post("/Vas/Order/Create", data);
  }
};
