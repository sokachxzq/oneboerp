import Http from '../../http'

export default {
    // 帮助中心 ================================
    HelpMenuList(params) {
        return Http.get('/Help/Menu/List', { params })
    },
    // 分页获取帮助文章列表
    HelpArticlePage(params) {
        return Http.get('/Help/Article/Page', { params })
    },
    // 获取帮助文章内容
    HelpArticleContent(params) {
        return Http.get('/Help/Article/Content', { params })
    },
    // 添加投诉建议
    HelpComplaintAdviceAdd(data) {
        return Http.post('/Help/ComplaintAdvice/Add', data)
    },


}
