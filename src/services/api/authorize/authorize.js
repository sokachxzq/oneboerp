import Http from '../../http'

export default {
    // 公司权限菜单 ===============================
    getCompanyPowerMenu(params) {
        return Http.get('/Authorize/GetCompanyPowerMenu', { params })
    },
    // 公司角色 ===============================
    getCompanyRolePage(params) {
        return Http.get('/Authorize/GetCompanyRolePage', { params })
    },
    addCompanyRole(data) {
        return Http.post('/Authorize/AddCompanyRole', data)
    },
    editCompanyRole(data) {
        return Http.post('/Authorize/EditCompanyRole', data)
    },
    enableCompanyRole(data) {
        return Http.post('/Authorize/EnableCompanyRole', data)
    },
    disableCompanyRole(data) {
        return Http.post('/Authorize/DisableCompanyRole', data)
    },
    deleteCompanyRole(data) {
        return Http.post('/Authorize/DeleteCompanyRole', data)
    },
    // 公司角色权限 ===============================
    getCompanyRolePowerMenu(params) {
        return Http.get('/Authorize/GetCompanyRolePowerMenu', { params })
    },
    enableCompanyRolePower(data) {
        return Http.post('/Authorize/EnableCompanyRolePower', data)
    },
    disableCompanyRolePower(data) {
        return Http.post('/Authorize/DisableCompanyRolePower', data)
    },
    deleteCompanyRolePower(data) {
        return Http.post('/Authorize/DeleteCompanyRolePower', data)
    },
    // 公司账号 ===============================
    getCompanyAccountPage(params) {
        return Http.get('/Authorize/GetCompanyAccountPage', { params })
    },
    addCompanyAccount(data) {
        return Http.post('/Authorize/AddCompanyAccount', data)
    },
    editCompanyAccount(data) {
        return Http.post('/Authorize/EditCompanyAccount', data)
    },
    enableCompanyAccount(data) {
        return Http.post('/Authorize/EnableCompanyAccount', data)
    },
    disableCompanyAccount(data) {
        return Http.post('/Authorize/DisableCompanyAccount', data)
    },
    deleteCompanyAccount(data) {
        return Http.post('/Authorize/DeleteCompanyAccount', data)
    },
    // 公司账号角色 ===============================
    getCompanyAccountRoleList(params) {
        return Http.get('/Authorize/GetCompanyAccountRoleList', { params })
    },
    editCompanyAccountRole(data) {
        return Http.post('/Authorize/EditCompanyAccountRole', data)
    },
    enableCompanyAccountRole(data) {
        return Http.post('/Authorize/EnableCompanyAccountRole', data)
    },
    disableCompanyAccountRole(data) {
        return Http.post('/Authorize/DisableCompanyAccountRole', data)
    },
    deleteCompanyAccountRole(data) {
        return Http.post('/Authorize/DeleteCompanyAccountRole', data)
    },
    // 公司账号权限 ===============================
    getCompanyAccountPowerMenu(params) {
        return Http.get('/Authorize/GetCompanyAccountPowerMenu', { params })
    },
    editCompanyAccountPower(data) {
        return Http.post('/Authorize/EditCompanyAccountPower', data)
    },
    enableCompanyAccountPower(data) {
        return Http.post('/Authorize/EnableCompanyAccountPower', data)
    },
    disableCompanyAccountPower(data) {
        return Http.post('/Authorize/DisableCompanyAccountPower', data)
    },
    deleteCompanyAccountPower(data) {
        return Http.post('/Authorize/DeleteCompanyAccountPower', data)
    },
    // 公司帐号登录日志 ===============================
    getCompanyAccountLogPage(params) {
        return Http.get('/Authorize/GetCompanyAccountLogPage', { params })
    },
}