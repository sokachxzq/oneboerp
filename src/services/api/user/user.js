import Http from '../../http'

export default {
    // user ================================
    // 登录
    UserLogin(data) {
        return Http.post('/User/Login', data)
    },
    // 注册
    UserRegister(data) {
        return Http.post('/User/Register', data)
    },
    // 发送验证码
    UserSendCode(data) {
        return Http.post('/User/SendCode', data)
    },
    // 忘记密码
    UserForgetPwd(data) {
        return Http.post('/User/ForgetPwd', data)
    },
    // 修改密码
    UserEditPwd(data) {
        return Http.post('/User/EditPwd', data)
    },
    // 获取帐号信息
    UserInfo(params) {
        return Http.get('/User/Info', { params })
    },

}
