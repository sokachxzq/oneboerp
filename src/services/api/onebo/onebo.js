import Http from '../../http'

export default {
    // onebo ================================
    // 获取分类列表
    CategoryList(params) {
        return Http.get('/Onebo/Category/List', { params })
    },
    // 获取分类子集
    CategorySubList(params) {
        return Http.get('/Onebo/Category/SubList', { params })
    },
    // 分页获取产品列表
    ProductPage(params) {
        return Http.get('/Onebo/Product/Page', { params })
    },
    // 订单预览
    OneboOrderPreview(data) {
        return Http.post('/Onebo/Order/Preview', data)
    },
    // ONEBO订单创建
    OneboOrderCreate(data) {
        return Http.post('/Onebo/Order/Create', data)
    },
    // 汇率
    OneboExchange(params) {
        return Http.get('/Onebo/Exchange', { params })
    },

}
