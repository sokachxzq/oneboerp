import Http from '../../http'

export default {
    // 客户订单 ================================
    // 订单支付查询
    OrderPayQuery(params) {
        return Http.get('/Order/PayQuery', { params })
    },
    // 分页客户订单列表
    OrderCustomerPage(params) {
        return Http.get('/Order/Customer/Page', { params })
    },

    // 财务订单 ================================
    // 分页获取订单列表
    OrderPage(params) {
        return Http.get('/Order/Page', { params })
    },
    // 获取订单信息列表
    OrderInfoList(params) {
        return Http.get('/Order/Info/List', { params })
    },
    // 获取订单明细列表
    OrderDetailList(params) {
        return Http.get('/Order/Detail/List', { params })
    },

}



