import user from './user/user'
import tikTok from './tikTok/tikTok'
import onebo from './onebo/onebo'
import order from './order/order'
import employee from './employee/employee'
import help from './help/help'
import vas from './vas/vas'

export default {
    
    user,
    tikTok,
    onebo,
    order,
    employee,
    help,
    vas

}