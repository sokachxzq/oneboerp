import Http from '../../http'

export default {
    // 同步数据 ================================
    // 全部数据
    syncData(data) {
        return Http.post('/Daoduo/SyncData', data)
    },
    // 品牌数据
    syncBrand(data) {
        return Http.post('/Daoduo/SyncBrand', data)
    },
    // 货品数据
    syncGoods(data) {
        return Http.post('/Daoduo/SyncGoods', data)
    },
    // 订单数据 
    syncOrder(data) {
        return Http.post('/Daoduo/SyncOrder', data)
    },

}
