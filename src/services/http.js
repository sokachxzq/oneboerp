import axios from "axios"
import vm from "../main"
import { Message, Loading } from 'element-ui'

let loading
const Http = axios

// function startLoading() {
//     loading = Loading.service({
//         lock: true,
//         text: '加载中...',
//         background: 'rgba(0, 0, 0, 0.7)'
//     })
// }
// function endLoading() {
//     loading.close()
// }
// let needLoadingRequestCount = 0
// export function showFullScreenLoading() {
//     if (needLoadingRequestCount === 0) {
//         startLoading()
//     }
//     needLoadingRequestCount++
// }

// export function tryHideFullScreenLoading() {
//     if (needLoadingRequestCount <= 0) return
//     needLoadingRequestCount--
//     if (needLoadingRequestCount === 0) {
//         endLoading()
//     }
// }

// 接口请求base地址
Http.defaults.baseURL = process.env.API_DOMAIN

// 接口超时时间设置
Http.defaults.timeout = 1000 * 30

// 请求拦截
Http.interceptors.request.use(
    config => {
        // 如果非登录接口
        if (config.url.indexOf("/Account/Login") === -1) {
            config.headers['authorization'] = 'Bearer' + ' ' + localStorage.getItem('token')
        } 
        // config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
        config.headers["Content-Type"] = "application/json"
        // loading = Loading.service({
        //     lock: true,
        //     text: '加载中...',
        //     background: 'rgba(0, 0, 0, 0.7)'
        // })
        // showFullScreenLoading()
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

// 响应拦截
Http.interceptors.response.use(res => {

    // tryHideFullScreenLoading()
    if (res.data.code === 0) {
        // vm.eleui.message(res.data.msg)

    } else {
        vm.eleui.message(res.data.msg)
    }
    return res.data
})
export default Http