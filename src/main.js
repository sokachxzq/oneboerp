// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";

Vue.config.productionTip = false;

// 引入element-ui
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

// 引入element-ui重新封装的
import eleui from "./assets/js/eleui.js";
Vue.prototype.eleui = eleui;

// 引入element-ui重新封装的全局组件
import eleuiIndex from "@/components/eleuiComponents/index";
Vue.use(eleuiIndex);

// 引入公共样式文件
import "./assets/styles/iconfont.js";
import "./assets/styles/index.scss";
import animate from "animate.css";
Vue.use(animate);

// 状态管理
import store from "./store";

// 接口服务
import api from "./services/api/index";
Vue.prototype.$api = api;

// utils
import utils from "./assets/js/utils";
Vue.prototype.utils = utils;

// math
import math from "./assets/js/math";
Vue.prototype.math = math;

// math
import math_ from "./assets/js/math_";
Vue.prototype.math_ = math_;

// 打印
import print from "vue-print-nb";
Vue.prototype.print = print;
Vue.use(print);

// import Print from './assets/js/print' // 引入js文件
// Vue.use(Print);

/* eslint-disable no-new */
let vm = new Vue({
  el: "#app",
  router,
  store,
  components: { App },
  template: "<App/>"
});
export default vm;
