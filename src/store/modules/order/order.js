

import mutations from './mutations'

const state = {
    orderList: []
}

const getters = {
    getOrderList: state => state.orderList
}

export default {
    state,
    getters,
    mutations
}