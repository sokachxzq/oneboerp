const mutations = {
    // 设置导航
    setNavCtrl(state, data) {
        var arr = []
        state.navCtrl.forEach((item, index) => {
            arr.push(item.name)
        })
        if (arr.indexOf(data.name) == -1) {
            state.navCtrl.push(data)
        } else {
            state.navPath = data.path;
            sessionStorage.setItem('navPath', state.navPath);
        }
        var navCtrl = JSON.stringify(state.navCtrl);
        sessionStorage.setItem('navCtrl', navCtrl)
    },
    setNavPath(state, data) {
        state.navPath = data
        sessionStorage.setItem('navPath', state.navPath);
    },

}

export default mutations