import mutations from './mutations';

const state = {
    navCtrl: [{
        meta: { level: '首页' },
        name: 'indexAdmin',
        path: "/indexAdmin"
    }],
    navPath: '/indexAdmin'
};

const getters = {
    getNavCtrl: state => state.navCtrl,
    getNavPath: state => state.navPath
};

export default {
    state,
    getters,
    mutations
};