import mutations from './mutations'

const state = {
    tiktokAuthId: null
}

const getters = {
    getTiktokAuthId: state => state.tiktokAuthId
}

export default {
    state,
    getters,
    mutations
}