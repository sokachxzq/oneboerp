import router from '@/router/index';

const mutations = {
    setMenuList: (state, menuList) => {
        // console.log('menuList === ', menuList)
        for (var i in menuList) {
            menuList[i].component = resetComponent(menuList[i].component_)
            for (var j in menuList[i].children) {
                menuList[i].children[j].component = resetComponent(menuList[i].children[j].component_)
            }
        }
        // var menuList_ = []
        // for (var i in menuList) {
        //     menuList_.push(menuList[i])
        //     menuList[i].component = resetComponent(menuList[i].component_)
        //     for (var j in menuList[i].children) {
        //         for (var k in menuList[i].children[j].children) {
        //             console.log('menuList[i].children[j].children[k].component_', menuList[i].children[j].children[k].component_)
        //             menuList[i].children[j].children[k].component = resetComponent(menuList[i].children[j].children[k].component_)
        //             menuList_[i].children.push(menuList[i].children[j].children[k])
        //         }
        //     }
        // }
        // console.log('menuList end === ', menuList)

        router.options.routes = menuList;
        router.addRoutes(menuList);
        // console.log('vuwx router ===', router)
        state.menuList = menuList;
    }
}

export default mutations

export const resetComponent = (path) => {
    return () => import('@/' + path + '.vue');
};