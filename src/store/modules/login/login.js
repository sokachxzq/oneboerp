import mutations from './mutations';

const state = {
    menuList: []
};

const getters = {
    // getMenuList: (state) => {
    //     console.log('执行 ==> ')
    //     if (state.menuList.length == 0){
    //         state.menuList = JSON.parse(sessionStorage.getItem('menuList'))
    //     }
    //     return state.menuList

    // }
    getMenuList: state => state.menuList
};

export default {
    state,
    getters,
    mutations
};