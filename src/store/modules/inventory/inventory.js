import mutations from './mutations';

const state = {
    inventoryOrderList: [],
};

const getters = {
    getInventoryOrderList: state => state.inventoryOrderList
};

export default {
    state,
    getters,
    mutations
};