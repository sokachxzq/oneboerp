
const mutations = {
    setInventoryOrderList: (state, inventoryOrderList) => {
        state.inventoryOrderList = inventoryOrderList;
    },
}

export default mutations
