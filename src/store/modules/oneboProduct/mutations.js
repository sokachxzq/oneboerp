
const mutations = {
    setBatchRecordId: (state, batchRecordId) => {
        state.batchRecordId = batchRecordId
    },
    setBatchRecordList: (state, batchRecordList) => {
        state.batchRecordList = batchRecordList
    }

}

export default mutations
