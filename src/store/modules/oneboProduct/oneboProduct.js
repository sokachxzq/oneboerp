import mutations from './mutations'

const state = {
    batchRecordId: 0,
    batchRecordList: []
}

const getters = {
    getBatchRecordId: state => state.batchRecordId,
    getBatchRecordList: state => state.batchRecordList
}

export default {
    state,
    getters,
    mutations
}