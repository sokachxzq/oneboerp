import mutations from './mutations';

const state = {
    purchaseOrderList: []
}

const getters = {
    getPurchaseOrderList: state => state.purchaseOrderList
}

export default {
    state,
    getters,
    mutations
}