
const mutations = {
    setPurchaseOrderList: (state, purchaseOrderList) => {
        state.purchaseOrderList = purchaseOrderList
    },
    setProductList: (state, productList) => {
        state.productList = productList
    }

}

export default mutations
