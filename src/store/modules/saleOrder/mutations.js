
const mutations = {
    setSaleOrderList: (state, saleOrderList) => {
        state.saleOrderList = saleOrderList;
    },
    setSaleOrderInfo: (state, saleOrderInfo) => {
        state.saleOrderInfo = saleOrderInfo;
    },

    setSaleOrderReturnApplyList: (state, saleOrderReturnApplyList) => {
        state.saleOrderReturnApplyList = saleOrderReturnApplyList;
    },
    setSaleOrderReturnApplyInfo: (state, saleOrderReturnApplyInfo) => {
        state.saleOrderReturnApplyInfo = saleOrderReturnApplyInfo;
    },
    
}

export default mutations
