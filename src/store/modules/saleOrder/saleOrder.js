import mutations from './mutations';

const state = {
    saleOrderList: [],
    saleOrderInfo: {},

    saleOrderReturnApplyList: [],
    saleOrderReturnApplyInfo: {},
};

const getters = {
    getSaleOrderList: state => state.saleOrderList,
    getSaleOrderInfo: state => state.saleOrderInfo,
    
    getSaleOrderReturnApplyList: state => state.saleOrderReturnApplyList,
    getSaleOrderReturnApplyInfo: state => state.saleOrderReturnApplyInfo,

};

export default {
    state,
    getters,
    mutations
};