

import mutations from './mutations'

const state = {
    productList: []
}

const getters = {
    getProductList: state => state.productList
}


export default {
    state,
    getters,
    mutations
}