import Vue from 'vue'
import Vuex from 'vuex'
import navCtrl from './modules/navCtrl/navCtrl'
import login from './modules/login/login'
import product from './modules/product/product'
import purchase from './modules/purchase/purchase'
import inventory from './modules/inventory/inventory'
import saleOrder from './modules/saleOrder/saleOrder'
import order from './modules/order/order'
import oneboProduct from './modules/oneboProduct/oneboProduct'


Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        navCtrl,
        login,
        product,
        purchase,
        inventory,
        saleOrder,
        order,
        oneboProduct,

    }
})