
import eleuiSelect from './eleuiSelect.vue'
import eleuiSelectCom from './eleuiSelectCom.vue'
import myUploadImage from '../myUploadImage.vue'
import myPriviewImage from '../myPriviewImage.vue'
import myRadioCheckbox from '../myRadioCheckbox.vue'
import myAuthListShop from '../myAuthListShop.vue'
import myAffix from '../myAffix.vue'

import myIconfont from '../myIconfont.vue'



const obj = {
    install(Vue) {
        Vue.component("eleuiSelect", eleuiSelect)
        Vue.component("eleuiSelectCom", eleuiSelectCom)
        Vue.component("myUploadImage", myUploadImage)
        Vue.component("myPriviewImage", myPriviewImage)
        Vue.component("myRadioCheckbox", myRadioCheckbox)
        Vue.component("myAuthListShop", myAuthListShop)
        Vue.component("myAffix", myAffix)

        Vue.component("myIconfont", myIconfont)


    }
}

export default obj